<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class EZDocmanControllerSendEmails extends JControllerAdmin
{

    public function sendemails()
    {
        $db = JFactory::getDbo();
        $config = JFactory::getConfig();
        $app = JFactory::getApplication();
        $mailer = JFactory::getMailer();
        $root = JURI::root();

        $counts = [
            'success' => 0,
            'fail'    => 0
        ];
        $errors = [];

        // configure mailer
        $sender = array(
            $config->get('mailfrom'),
            $config->get('fromname'));

//        $mailer->isHTML(true);
        $mailer->Encoding = 'base64';


        $docs = $this->generateList();
        if (empty($docs))
        {
            $this->setRedirect('index.php?option=com_ezdocman&view=sendemails', 'No emails to send');
            return;
        }


        foreach ($docs as $doc)
        {
            $mailer->setSender($sender)
                    ->setSubject("New Invoice {$doc['title']} from Oxford HQ")
                    ->setBody($this->getBody($root, $doc['cat_id']));

            foreach ($doc['users'] as $email)
            {
                if($email)
                {
                    $mailer->ClearAllRecipients();

                    $result = $mailer->addRecipient($email)->Send();

                    if ($result !== true)
                    {
                        $counts['fail'] ++;
                        $errors[] = $result->getError();
                    }
                    else
                        $counts['success'] ++;
                }
            }

            if($doc['id']){
                $sql = "DELETE FROM #__edocman_categories_updated WHERE id = " . $doc['id'];
                $db->setQuery($sql);
                $db->execute();
            }
        }

        if ($counts['fail'])
        {
            $app->enqueueMessage('Errors occurred.', 'error');
            foreach ($errors as $error)
            {
                $app->enqueueMessage($error, 'error');
            }
            $this->setRedirect('index.php?option=com_ezdocman&view=sendemails',
                    "Email(s) sent: {$counts['success']} successful, {$counts['fail']} failed", 'warning');
        }
        else
            $this->setRedirect('index.php?option=com_ezdocman&view=sendemails',
                    "{$counts['success']} email(s) sent successfully.");
    }

    private function generateList()
    {
        $sql = <<<QUERY
SELECT 
    d.id AS doc_id,
    c.id AS cat_id,
    c.user_ids,
    d.title AS doc_title, 
    d.published
FROM 
    #__edocman_categories_updated AS u
LEFT JOIN 
    #__edocman_categories c on c.id = u.category_id
LEFT JOIN
    #__edocman_documents d ON d.id = u.id
WHERE
    d.title RLIKE '^[[:digit:]]+\-[[:digit:]]{6}$'
QUERY;
        $rows = JFactory::getDbo()->setQuery($sql)->loadAssocList();

        $docs = [];
        foreach ($rows as $row)
        {
            if ($row['published'])
            {
                $doc = [
                    'id'     => $row['doc_id'],
                    'title'  => $row['doc_title'],
                    'cat_id' => (int) $row['cat_id'],
                    'users'  => []
                ];
                $userIDs = explode(',', $row['user_ids']);
                foreach ($userIDs as $id)
                {
                    if (!$id)
                        continue;

                    $doc['users'][$id] = $this->getUserMail($id);
                }
                $docs[] = $doc;
            }
        }
        return $docs;
    }

    function getUserMail($id)
    {
        $db = JFactory::getDbo();
        $sql = 'SELECT `email` FROM #__users WHERE id=' . $id;
        $db->setQuery($sql);
        return $db->loadResult();
    }

    private function getBody($root, $cat_id)
    {
        return <<<EOM
Month end invoices have been uploaded to Oxford HQ.

Please view your invoice at the following location:
{$root}index.php/component/edocman/?view=jobsite&id=$cat_id

For billing related questions, please contact accounting@oxfordbuilders.ca.

If you have any other questions or problems, please contact us at oxfordhq@oxfordbuilders.ca.

Thank you,
Oxford Builders
EOM;
    }

}
