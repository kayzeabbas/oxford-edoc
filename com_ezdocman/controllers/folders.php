<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class EZDocmanControllerFolders extends JControllerAdmin
{

    /**
     * Proxy for getModel.
     * @since	2.5
     */
    public function getModel($name = 'folders', $prefix = 'EZDocmanModel', $array = array('ignore_request' => true))
    {
        $model = parent::getModel($name, $prefix, $array);
        return $model;
    }

    public function save()
    {
        $url = 'index.php?option=com_ezdocman&view=folders';
        $msg = "Saved succesfully";
        $type = null;
        $jInput = JFactory::getApplication()->input;
        $post =$jInput->post->getArray();
        $model = $this->getModel();
        if (!$model->store($post["jform"]))
        {
            $msg = "Failed to save";
            $type = 'error';
        }

        $this->setRedirect($url, $msg, $type);
    }

    public function delete()
    {
        $url = 'index.php?option=com_ezdocman&view=folders';
        $msg = "Deleted";
        $type = null;
        $jInput = JFactory::getApplication()->input;
        $cid = $jInput->getArray("cid", array());

        $model = $this->getModel();
        $model->delete($cid);

        $this->setRedirect($url, $msg, $type);
    }

}
