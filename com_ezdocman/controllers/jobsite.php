<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class EZDocmanControllerJobsite extends JControllerAdmin
{

    /**
     * Proxy for getModel.
     * @since	2.5
     */
    public function getModel($name = 'Jobsite', $prefix = 'EZDocmanModel', $config = array())
    {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));
        return $model;
    }

    public function save()
    {
        $url = 'index.php?option=com_ezdocman&view=folders';
        $msg = "Saved succesfully";

        $type = null;
        $jInput = JFactory::getApplication()->input;
        $data = $jInput->post->get('jform', array(), 'array');
        $id = $data["id"];
        $model = $this->getModel();
        if (!$model->save($data))
        {
            $msg = "Failed to save";
            $type = 'error';
            $url = 'index.php?option=com_ezdocman&view=jobsite&id=' . $id;
        }

        $this->setRedirect($url, $msg, $type);
    }

    public function finish()
    {
        $url = 'index.php?option=com_ezdocman&view=folders';
        $msg = "Unpublished succesfully";

        $type = null;
        $jInput = JFactory::getApplication()->input;
        $data = $jInput->post->get('jform', array(), 'array');;
        $id = $data["id"];
        $model = $this->getModel();
        if (!$model->unpublish($data))
        {
            $msg = "Failed to unpublish";
            $type = 'error';
            $url = 'index.php?option=com_ezdocman&view=jobsite&id=' . $id;
        }

        $this->setRedirect($url, $msg, $type);
    }

    public function cancel()
    {
        $this->setRedirect("index.php?option=com_ezdocman&view=folders");
    }

}
