<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * General Controller of HelloWorld component
 */
class EZDocmanController extends JControllerLegacy
{

    /**
     * display task
     *
     * @return void
     */
    function display($cachable = false, $urlparams = false)
    {
        // set default view if not set
        $input = JFactory::getApplication()->input;
        $input->set('view', $input->getCmd('view', 'EZDocman'));

        // call parent behavior
        parent::display($cachable);
    }

    function upload_documents()
    {
        $jInput = JFactory::getApplication()->input;
        $post = $jInput->post->getArray();
        $files = $this->switchFilePostFormat($jInput->files->get('invoices'));

        $model = $this->getModel('bulkupload');
        $model->uploadFiles($post, $files);

        $this->setRedirect('index.php?option=com_ezdocman');
    }

    function newfolder()
    {
        $jInput = JFactory::getApplication()->input;
        $url = 'index.php?option=com_ezdocman';
        $msg = "Folder sucessfully created.";
        $model = $this->getModel('folder');
        if (!$model->newfolder($jInput->post->getArray()))
        {
            $msg = "Failed to create folder";
        }

        $this->setRedirect($url, $msg);
    }

    function cancel()
    {
        $url = 'index.php?option=com_ezdocman';
        $msg = null;
        $this->setRedirect($url, $msg);
    }

    private function switchFilePostFormat($posted_files)
    {
        $files = [];
        for ($i = 0, $n = count($posted_files); $i < $n; $i++)
        {
            if($posted_files[$i]['name'])
            {
                $files[] = [
                    'name'     => $posted_files[$i]['name'],
                    'tmp_name' => $posted_files[$i]['tmp_name'],
                    'type'     => $posted_files[$i]['type'],
                    'size'     => $posted_files[$i]['size'],
                    'error'    => $posted_files[$i]['error']
                ];
            }
        }
        return $files;
    }

}
