<?php
// no direct access
defined('_JEXEC') or die;
?>

<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div style="float:left; width: 100%;">
        <fieldset class="adminform">
            <legend><?php echo JText::_('Send Emails'); ?></legend>
            <table>
                <tr><td><input type="submit" value="Sends emails"/> <small> Send emails to all users who have access to categories that have been updated since the last send.</small></td></tr>
                <tr><td><input type="button" onclick="Joomla.submitbutton('cancel')" value="Cancel" /></td></tr>
            </table>



        </fieldset>				
    </div>		
    <div class="clr"></div>
    <input type="hidden" name="documentsToBeCleared" value="<?php echo implode(",", $this->documentIDs); ?>" />
    <?php echo JHTML::_('form.token'); ?>	
    <input type="hidden" name="option" value="com_ezdocman" />
    <input type="hidden" name="task" value="sendemails.sendemails" />
</form>