<?php

/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	EDocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2018-2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * HTML View class for EDocman component
 *
 * @static
 * @package		Joomla
 * @subpackage	EDocman
 * @since 1.5
 */
class EZDocmanViewSendEmails extends JViewLegacy
{

    function display($tpl = null)
    {
        JHTML::_('behavior.tooltip');
        JToolBarHelper::title(JText::_('Send Emails to Users'), 'massmail.png');
        parent::display($tpl);
    }

}
