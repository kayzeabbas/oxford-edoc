<?php
/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die('');
JHTML::_('behavior.tooltip');

$user = JFactory::getUser();
$userId = $user->id;
$listOrder = $this->lists['order'];
$listDirn = $this->lists['order_Dir'];
$saveOrder = $listOrder == 'a.ordering';

$db = JFactory::getDbo();

$db->setQuery("
    UPDATE #__edocman_document_category dc
    JOIN #__edocman_categories c ON c.`id` = dc.`category_id`
    JOIN `#__edocman_documents` d ON d.`id` = dc.`document_id`
    SET d.`user_ids` = c.`user_ids`
")->execute();

?>
<form action="<?php echo JRoute::_('index.php?option=com_ezdocman&view=folders') ?>" method="post" name="adminForm" id="adminForm">
    <fieldset id="filter-bar">
        <div class="filter-search fltlft">
            <label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
            <input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('Search'); ?>" />
            <button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
            <button type="button" onclick="document.id('filter_search').value = '';
                    this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
        </div>
        <div class="filter-select fltrt">
            <?php echo $this->lists['filter_category_id']; ?>

            <?php echo $this->lists['state']; ?>
        </div>
    </fieldset>
    <div id="editcell">
        <table class="adminlist">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->items); ?>);" />
                    </th>
                    <th class="title" style="text-align: left;">
                        <?php echo JHTML::_('grid.sort', JText::_('Title'), 'a.title',
                                $listDirn, $listOrder);
                        ?>
                    </th>
                    <th>
<?php echo JText::_('Jobsite Info'); ?>
                    </th>
                    <th>
<?php echo JText::_('# Documents'); ?>
                    </th>

                    <th width="10%">
<?php echo JText::_("Users"); ?>
                    </th>

                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="8">
<?php echo $this->pagination->getListFooter(); ?>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <?php
                $k = 0;
                $count = count($this->items);
                for ($i = $this->pagination->limitstart, $n = $this->pagination->limitstart + $this->pagination->limit; $i < $n && $i < $count; $i++)
                {
                    $item = &$this->items[$i];
                    $canEdit = $user->authorise('core.edit', 'com_edocman.category.' . $item->id);
                    $canCheckin = $user->authorise('core.admin', 'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
                    $canEditOwn = $user->authorise('core.edit.own', 'com_edocman.category.' . $item->id) && $item->created_user_id == $userId;
                    $canChange = $user->authorise('core.edit.state', 'com_edocman.category.' . $item->id) && $canCheckin;
                    ?>
                    <tr class="<?php echo "row$k"; ?>">
                        <td style="text-align:center;">
    <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                        </td>
                        <td>
                            <?php if ($item->checked_out) : ?>
                                <?php echo JHtml::_('jgrid.checkedout', $i,
                                        $item->editor, $item->checked_out_time, 'categories.', $canCheckin);
                                ?>
                            <?php endif; ?>
                            <?php
                            if (($canEdit || $canEditOwn) && $item->job_id != 0)
                            {
                                if ($item->js_id != 0)
                                {
                                    ?>
                                    <a href="<?php echo JRoute::_('index.php?option=com_ezdocman&view=jobsite&id=' . $item->js_id); ?>">
                                    <?php echo $item->title; ?>
                                    </a>
                                <?php
                                }
                                else
                                {
                                    ?>
                                    <a href="<?php echo JRoute::_('index.php?option=com_ezdocman&view=jobsite&jobsite_id=' . $item->job_id); ?>">
                                    <?php echo $item->title; ?>
                                    </a>
                                    <?php
                                }
                            }
                            else
                            {
                                echo $item->title;
                            }
                            ?>
                            <p class="smallsub">

                            <?php echo JText::sprintf('JGLOBAL_LIST_ALIAS',
                                    $this->escape($item->alias));
                            ?></p>
                        </td>

                        <td>
                            <?php
                            if (isset($item->job_id) && $item->job_id != 0)
                            {
                                echo '<div class="jobsite_info">
						<span>' . $item->job_id . '</span>
						<span>' . $item->name . '</span>
						<span>' . $item->contact . '</span>
						<span>' . $item->address . '</span>
						<span>' . $item->city . '</span>
						<span>' . $item->postal_code . '</span>
						<span>' . $item->telephone . '</span>
						<span>' . $item->cellphone . '</span>
						<span>' . $item->email . '</span>
					</div>';
                            }
                            ?>
                        </td>

                        <td style="text-align: center;">
    <?php echo $item->total_documents; ?>
                        </td>

                        <td class="center">
                    <?php echo EZDocmanHelper::getUserInputPerCat($item->user_ids, $item->id) ?>
                        </td>

                    </tr>
    <?php
    $k = 1 - $k;
}
?>
            </tbody>
        </table>
    </div>
    <input type="hidden" name="option" value="com_ezdocman" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
<?php echo JHTML::_('form.token'); ?>
</form>