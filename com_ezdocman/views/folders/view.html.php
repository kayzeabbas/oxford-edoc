<?php

/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');


/**
 * View class for a list of Edocman.
 */
class EZDocmanViewFolders extends JViewLegacy
{

    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null)
    {
        $mainframe = & JFactory::getApplication('admin');
        $option = 'com_ezdocman';
        $filter_state = $mainframe->getUserStateFromRequest($option . 'category_filter_state', 'filter_state', '',
                'word');
        //$mainframe->setUserState($option . 'category_filter_state', $filter_state);
        $filter_order = $mainframe->getUserStateFromRequest($option . 'category_filter_order', 'filter_order',
                'a.ordering', 'cmd');
        $filter_order_Dir = $mainframe->getUserStateFromRequest($option . 'category_filter_order_Dir',
                'filter_order_Dir', '', 'word');
        $search = $mainframe->getUserStateFromRequest($option . 'filter_search', 'filter_search', '', 'string');


        $search = utf8_strtolower($search);
        $config = EdocmanHelper::getConfig();

        $lists['search'] = $search;
        $items = & $this->get('Data');
        $pagination = & $this->get('Pagination');
        $lists['order_Dir'] = $filter_order_Dir;
        $lists['order'] = $filter_order;
        $lists['state'] = JHTML::_('grid.state', $filter_state);

        $this->lists = $lists;
        $this->items = $items;
        $this->pagination = $pagination;
        $this->config = $config;
        $this->language = JFactory::getLanguage();

        $this->state = $this->get('State');


        $this->addToolbar();
        parent::display($tpl);

    }

    /**
     * Add the page title and toolbar.
     *
     * @since	1.6
     */
    protected function addToolbar()
    {
        require_once str_replace(array('\administrator','/administrator'),'',JPATH_ADMINISTRATOR) . DS . 'components' . DS . 'com_edocman' . DS . 'helper' . DS . 'helper.php';

        $state = $this->get('State');

        $canDo = EdocmanHelper::getActions();

        JToolBarHelper::title(JText::_('Edit Jobsites/Folder Permissions'), 'categories.png');

        //Check if the form exists before showing the add/edit buttons

        if ($canDo->get('core.edit'))
        {
            JToolBarHelper::save('folders.save', 'JTOOLBAR_SAVE');
            JToolBarHelper::deleteList('Delete checked jobsites/folders?', 'folders.delete', 'JTOOLBAR_DELETE');
        }
        JToolBarHelper::custom('cancel', 'cancel.png', 'cancel.png', 'Cancel', false);
    }

}
