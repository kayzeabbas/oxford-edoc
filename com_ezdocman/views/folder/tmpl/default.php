<?php
/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

// Include the component HTML helpers.
//JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
?>
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <input type="hidden" name="id" value>
    <table>
        <tbody class="folder-info">

            <tr>
                <td>Title</td>
                <td><input type="text" name="title"></td>
            </tr>
            <tr>
                <td>
                    Parent:
                </td>
                <td>
                    <?php echo $this->lists['category_id']; ?>
                </td>
            </tr>


            <tr>
                <td>User IDs</td>
                <td><?php echo EdocmanHelper::getUserInput('') ?></td>
            </tr>
            <tr>
                <td>
                    Is Jobsite
                </td>
                <td><input type="checkbox" name="isJobSite" checked="true" onchange="$$('.job-site-info').toggle();"></td>
            </tr>
        </tbody>


        <tbody class="job-site-info">
            <tr>
                <td>Job Id</td>
                <td><input type="text" name="jobsite_info[jobsite_id]"></td>
            </tr>
            <tr>
                <td>Job Name</td>
                <td><input type="text" name="jobsite_info[name]"></td>
            </tr>
            <tr>
                <td>Job Contact</td>
                <td><input type="text" name="jobsite_info[contact]"></td>
            </tr>
            <tr>
                <td>Job Address</td>
                <td><input type="text" name="jobsite_info[address]"></td>
            </tr>
            <tr>
                <td>Job City</td>
                <td><input type="text" name="jobsite_info[city]"></td>
            </tr>
            <tr>
                <td>Job Province</td>
                <td><input type="text" name="jobsite_info[province]"></td>
            </tr>
            <tr>
                <td>Job Postal Code</td>
                <td><input type="text" name="jobsite_info[postal_code]"></td>
            </tr>
            <tr>
                <td>Job Telephone</td>
                <td><input type="text" name="jobsite_info[telephone]"></td>
            </tr>
            <tr>
                <td>Job Cellphone</td>
                <td><input type="text" name="jobsite_info[cellphone]"></td>
            </tr>
            <tr>
                <td>Job Email</td>
                <td><input type="text" name="jobsite_info[email]"></td>
            </tr>

        </tbody>



    </table>
    <?php echo JHTML::_('form.token'); ?>
    <input type="hidden" name="option" value="com_ezdocman" />
    <input type="hidden" name="task" value="newfolder" />
</form>

<script type="text/javascript">
    Joomla.submitbutton = function (pressbutton) {
        var form = document.forms["adminForm"];
        if (pressbutton === "newfolder")
            validateAndSubmit();
        else {
            form.task.value = pressbutton;
            form.submit();
        }
    }

    function validateAndSubmit() {
        var form = document.forms["adminForm"];
        if (form["title"].value === "") {
            alert("Title must be filled out");
            return;
        }
        if (form["isJobSite"].checked) {

            if (form["jobsite_info[jobsite_id]"].value === "" || isNaN(form['jobsite_info[jobsite_id]'].value)) {
                alert("Job ID must be filled out with numbers");
                return;
            }
        }
        form.submit();
    }
</script>
