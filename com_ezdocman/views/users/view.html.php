<?php

/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View class for a list of users.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_users
 * @since		1.6
 */
class EZDocmanViewUsers extends JViewLegacy
{

    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null)
    {
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');
        $jinput = JFactory::getApplication()->input;
        $this->usersstring = $jinput->getString("users");
        $this->usersarray = explode(',', $this->usersstring);
        $this->checkboxes = $jinput->getString("checkboxes");
        if (isset($this->checkboxes))
        {
            $this->usersarray = $this->checkboxes;
        }

        $this->category = $jinput->getString('category');


        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            JError::raiseError(500, implode("\n", $errors));
            return false;
        }

        // Include the component HTML helpers.
        JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');


        parent::display($tpl);
    }

}
