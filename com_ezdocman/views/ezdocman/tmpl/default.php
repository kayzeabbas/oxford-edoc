<?php
defined('_JEXEC') or die('');
?>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
        <td valign="top">
            <table class="adminlist">
                <tr>
                    <td>
                        <div id="cpanel">
                            <?php
                            $this->quickiconButton('index.php?option=com_ezdocman&amp;view=bulkupload',
                                    'icon-48-document-add.png', JText::_('Bulk Upload Invoices'));
                            $this->quickiconButton('index.php?option=com_ezdocman&amp;view=folder',
                                    'icon-48-category-add.png', JText::_('New Folder/Jobsite'));
                            $this->quickiconButton('index.php?option=com_ezdocman&amp;view=folders',
                                    'icon-48-config.png', JText::_('Edit Jobsites/Folders Permissions'));
                            $this->quickiconButton('index.php?option=com_ezdocman&amp;view=sendemails',
                                    'icon-48-empty.png', JText::_('Send Emails'));
                            $this->quickiconButton('index.php?option=com_edocman&amp;view=documents',
                                    'icon-48-documents.png', JText::_('Documents'));
                            ?>
                        </div>
                    </td>
                </tr>
            </table>
        </td>

    </tr>
</table>
