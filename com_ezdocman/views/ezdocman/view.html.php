<?php
/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * Edocman Component - Dashboard View
 * @package		Edocman
 * @subpackage	View
 */
class EZDocmanViewEZDocman extends JViewLegacy
{

    /**
     * Display.
     */
    function display($tpl = null)
    {

        jimport('joomla.html.pane');
        JHTML::_('behavior.tooltip');

        // create the toolbar
        JToolBarHelper::title(JText::_('Monthly Invoices'), 'frontpage.png');


        //$this->pane = $pane;
        $this->user = JFactory::getUser();

        //Add the preperence option

        parent::display($tpl);
    }

    /**
     * Creates the buttons view.
     * @param string $link targeturl
     * @param string $image path to image
     * @param string $text image description
     * @param boolean $modal 1 for loading in modal
     */
    function quickiconButton($link, $image, $text, $modal = 0)
    {
        //initialise variables
        $lang = &JFactory::getLanguage();
        ?>

        <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
            <div class="icon">
                <?php
                if ($modal == 1)
                {
                    JHTML::_('behavior.modal');
                    ?>
                    <a href="<?php echo $link . '&amp;tmpl=component'; ?>" style="cursor:pointer" class="modal" rel="{handler: 'iframe', size: {x: 650, y: 400}}">
                        <?php
                    }
                    else
                    {
                        ?>
                        <a href="<?php echo $link; ?>">
                            <?php
                        }
                        echo JHTML::_('image', 'administrator/components/com_edocman/assets/images/' . $image, $text);
                        ?>
                        <span><?php echo $text; ?></span>
                    </a>
            </div>
        </div>
        <?php
    }

}
