<?php
/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

// Include the component HTML helpers.
//JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JToolBarHelper::title(JText::_('Edit Jobsite Info'), 'categories.png');
?>
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" class="jobsite adminform">
    <ul class="adminformlist">
        <?php foreach ($this->form->getFieldset() as $field): ?>
            <li<?php
            if($field->fieldname == "id")
            {
                echo ' class="hidden"';
            }
            ?>><?php
                echo $field->label;
                if ($field->fieldname == "jobsite_id" && isset($this->placeholder_jobsite) && $field->value == '')
                {
                    echo str_replace('value=""', 'value="' . $this->placeholder_jobsite . '"', $field->input);
                }
                else
                {
                    echo $field->input;
                }
                ?></li>
        <?php endforeach; ?>
    </ul>
    <?php echo JHTML::_('form.token'); ?>
    <input type="hidden" name="option" value="com_ezdocman" />
    <input type="hidden" name="task" value="newfolder" />
</form>

<script type="text/javascript">

</script>
