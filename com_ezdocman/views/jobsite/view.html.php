<?php

/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	EDocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2018-2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * HTML View class for EDocman component
 *
 * @static
 * @package		Joomla
 * @subpackage	EDocman
 * @since 1.5
 */
class EZDocmanViewJobsite extends JViewLegacy
{

    public function display($tpl = null)
    {
        jimport('joomla.filesystem.folder');
        JHTML::_('behavior.tooltip');

        $db = JFactory::getDbo();
        $config = EdocmanHelper::getConfig();
        $form = $this->get('Form');
        $item = $this->get('Item');
        $jInput = JFactory::getApplication()->input;
        $jobsite = $jInput->getString('jobsite_id', -1);

        if ($jobsite != -1)
            $this->placeholder_jobsite = $jobsite;
        $this->form = $form;
        $this->item = $item;
        $this->config = $config;
        $this->addToolbar();
        parent::display($tpl);
    }

    protected function addToolbar()
    {

        JToolBarHelper::save('jobsite.save');
        JToolBarHelper::unpublish('jobsite.finish');
        JToolBarHelper::custom('jobsite.cancel', 'cancel.png', 'cancel.png', 'Cancel', false);
    }

}
