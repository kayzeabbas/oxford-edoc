<?php

/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	EDocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2018-2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * HTML View class for EDocman component
 *
 * @static
 * @package		Joomla
 * @subpackage	EDocman
 * @since 1.5
 */
class EZDocmanViewBulkUpload extends JViewLegacy
{

    function display($tpl = null)
    {

        jimport('joomla.filesystem.folder');
        JHTML::_('behavior.tooltip');
        JToolBarHelper::title(JText::_('Bulk Invoice Upload'), 'generic.png');
        $db = JFactory::getDbo();
        $config = EdocmanHelper::getConfig();

        $options = self::getCategoryOptions();
        array_shift($options); // remove 'Select Option' option
        array_unshift($options, JHTML::_('select.option', -1, 'Auto'));

        $lists['select_category'] = JHTML::_('select.genericlist', $options, 'category_id', 'class="inputbox" ',
                        'value', 'text', -1);
        $this->assignRef('lists', $lists);
        $this->assignRef('config', $config);

        parent::display($tpl);
    }

    public static function getCategoryOptions($published = '*')
    {
        $db = & JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id, title, parent_id, job_id');
        $query->from('#__edocman_categories');
        if ($published != '*')
            $query->where('published=1'); // want to be able to filter by review, so we don't want this where clause
        $query->order('title');
        $db->setQuery($query);
        $rows = $db->loadObjectList();

        $children = array();
        // first pass - collect children
        if (count($rows))
        {
            foreach ($rows as $v)
            {
                $v->title = $v->job_id == 0 ? $v->title : "$v->title ($v->job_id)";
                $pt = $v->parent_id;
                $list = $children[$pt] ? $children[$pt] : array();
                array_push($list, $v);
                $children[$pt] = $list;
            }
        }
        $list = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, false);
        $options = array();
        $options[] = JHtml::_('select.option', 0, JText::_('Select Category'));
        if (count($list))
        {
            foreach ($list as $row)
            {
                $options[] = JHtml::_('select.option', $row->id, $row->treename, ['disable' => $row->parent_id == 0]);
            }
        }
        return $options;
    }

}
