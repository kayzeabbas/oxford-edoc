<?php
// no direct access
defined('_JEXEC') or die;
?>

<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div style="float:left; width: 100%;">
        <fieldset class="adminform">
            <legend><?php echo JText::_('Bulk Upload Options'); ?></legend>
            <table class="admintable">				
                <tr>
                    <td>Upload Files</td>
                    <td width="100" align="right" class="key">
                        <input type="file" accept="application/pdf" name="invoices[]" id="jform_filenames" multiple="true" class="inputbox" />
                    </td>
                    <td>
                        <small>Maximum cumulative file size is <?php
                            echo min(ini_get('post_max_size'), ini_get('upload_max_filesize'));
                            ?></small>.
                    </td>
                </tr>
                <tr>
                    <td>
                        Category
                    </td>
                    <td>
                        <?php echo $this->lists['select_category']; ?>
                    </td>
                    <td>
                        <small>Choose the category to which the imported documents will be assigned.</small>.
                    </td>
                </tr>
                <tr>
                    <td>
                        Distribution Date
                    </td>
                    <td colspan="2">
                        <input name='distribution_time' type="date"/>
                    </td>

                </tr>
                <tr>
                    <td colspan="2">
                        <input type="button" onclick="Joomla.submitbutton('upload_documents')" value="Submit"/>
                        <input type="button" onclick="Joomla.submitbutton('cancel')" value="Cancel" />
                    </td>
                </tr>

            </table>						
        </fieldset>				
    </div>		
    <div class="clr"></div>	
    <?php echo JHTML::_('form.token'); ?>	
    <input type="hidden" name="option" value="com_ezdocman" />
    <input type="hidden" name="task" value="" />
</form>