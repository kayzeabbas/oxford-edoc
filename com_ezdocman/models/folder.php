<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class EZDocmanModelFolder extends JModelLegacy
{

    function newfolder($data)
    {
        $db = JFactory::getDbo();

        $catID = $data['category_id'];
        if (ctype_digit($catID) && $catID != 0)
        {
            $catID = (int) $catID;
            $catAccess = $db
                    ->setQuery("SELECT `access` FROM #__edocman_categories WHERE `id` = $catID")
                    ->loadResult();
        }
        else
        {
            $catID = 0;
            $catAccess = 1;
        }

        $jobID = null;
        if (isset($data['isJobSite']))
        {
            $jobInfoRecord = $this->getTable('Jobsite', 'EZDocmanTable');
            $jobsitedata = $data['jobsite_info'];
            $jobInfoRecord->bind($jobsitedata);
            $jobInfoRecord->check();
            if (!$jobInfoRecord->store())
            {
                ob_start();
                var_dump($jobInfoRecord);
                JFactory::getApplication()->enqueueMessage('creating jobsite failed');
                JFactory::getApplication()->enqueueMessage(ob_get_clean());
                return false;
            }
            $jobID = $jobInfoRecord->jobsite_id;
        }

        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_edocman/table');
        $table = JTable::getInstance('Category', 'EDocmanTable');
        $table->bind([
            'id'        => $jobInfoRecord->id,
            'parent_id' => $catID,
            'access'    => $catAccess,
            'title'     => $data['title'],
            'job_id'    => $jobID,
            'user_ids'  => $data['jform']['user_ids'],
            'published' => 1
        ]);
        $table->check();

        if ($table->store())
            return true;

        if ($jobInfoRecord)
            $jobInfoRecord->delete();

        return false;
    }

}
