<?php

/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.model');

/**
 * Methods supporting a list of Edocman records.
 */
class EZDocmanModelFolders extends JModelLegacy
{

    /**
     * Categories data array
     *
     * @var array
     */
    var $_data = null;

    /**
     * Pagination object
     *
     * @var object
     */
    var $_pagination = null;

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct()
    {
        parent::__construct();

        global $mainframe, $option;

        $mainframe = & JFactory::getApplication('admin');
        $jInput = JFactory::getApplication()->input;
        $option = $jInput->getCmd('option', 'com_ezdocman');


        // Get the pagination request variables
        $config = JFactory::getConfig();
        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $config->get('list_limit'),
                'int');
        $limitstart = $mainframe->getUserStateFromRequest($option . '.limitstart', 'limitstart', 0, 'int');

        // In case limit has been changed, adjust limitstart accordingly
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
        $this->setState('filter.search', $mainframe->getUserStateFromRequest($option . 'filter_search', 'filter_search', '', 'string'));
    }

    function store($data)
    {
        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_edocman/table');

        $db = JFactory::getDbo();
        $tablesToChange = array();
        $dataToInsert = array();
        $ogData = array();
        $i = 0;
        foreach ($data as $key => $value)
        {
            $temp = explode("_", $key);
            $tablesToChange[] = $temp[2];
            $dataToInsert[] = $value;

            $query = "Select user_ids from #__edocman_categories where id = " . $temp[2];
            $db->setQuery($query);
            $ogData[] = $db->loadResult();
            if ($ogData[$i] == $dataToInsert[$i])
            {
                array_splice($ogData, $i, 1);
                array_splice($dataToInsert, $i, 1);
                array_splice($tablesToChange, $i, 1);
                $i--;
            }
            $i++;
        }
        for ($i = 0; $i < count($tablesToChange); $i++)
        {
            $row = JTable::getInstance("category", "EDocmanTable");
            $row->load($tablesToChange[$i]);
            $toBeBound = array();
            $toBeBound['user_ids'] = $dataToInsert[$i];
            if (!$row->bind($toBeBound) || !$row->check() || !$row->store())
            {
                break;
                return false;
            }
        }

        return true;
    }

    /**
     * Method to get categories data
     *
     * @access public
     * @return array
     */
    function getData()
    {
        // Lets load the content if it doesn't already exist
        if (empty($this->_data))
        {
            $query = $this->_buildQuery();
            //We will build the data here
            $this->_db->setQuery($query);
            $rows = $this->_db->loadObjectList();
            $this->_data = $rows;
        }
        return $this->_data;
    }

    /**
     * Get total documents 
     *
     * @return int
     */
    function getTotal()
    {
        // Lets load the content if it doesn't already exist
        if (empty($this->_total))
        {
            $where = $this->_buildContentWhere();

            //Must remove portion of where statement that includes js.name (_buildQuery uses js table)
            $mainframe = & JFactory::getApplication('admin');
            $option = 'com_ezdocman';
            $db = & JFactory::getDBO();
            $search = $mainframe->getUserStateFromRequest($option . 'filter_search', 'filter_search', '', 'string');
            $search = utf8_strtolower($search);

            $where = str_replace(' OR    LOWER(js.name) LIKE ' . $db->Quote('%' . $search . '%', false), '', $where);

            $sql = 'SELECT COUNT(*) FROM #__edocman_categories AS a ' . $where;
            $this->_db->setQuery($sql);
            $this->_total = $this->_db->loadResult();
        }
        return $this->_total;
    }

    /**
     * Method to get a pagination object for the donors
     *
     * @access public
     * @return integer
     */
    function getPagination()
    {
        // Lets load the content if it doesn't already exist
        if (empty($this->_pagination))
        {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'),
                    $this->getState('limit'));
        }
        return $this->_pagination;
    }

    /**
     * Build the select clause
     *
     * @return string
     */
    function _buildQuery()
    {
        // Get the WHERE and ORDER BY clauses for the query
        $where = $this->_buildContentWhere();
        $orderby = $this->_buildContentOrderBy();
        $query = 'SELECT a.id, a.title, a.alias , a.parent_id, a.ordering, a.created_user_id , a.published, '
                . ' ag.title AS access_level, COUNT(b.id) AS total_documents, a.user_ids, js.id as js_id, a.job_id, js.name, js.contact, js.address, js.city, js.postal_code, js.telephone, '
                . ' js.cellphone, js.email '
                . ' FROM #__edocman_categories AS a '
                . ' LEFT JOIN #__jobsite_info AS js ON js.jobsite_id=a.job_id '
                . ' LEFT JOIN #__users AS uc ON uc.id=a.checked_out '
                . ' LEFT JOIN #__viewlevels AS ag ON ag.id = a.access '
                . ' LEFT JOIN #__edocman_document_category AS b '
                . ' ON a.id = b.category_id '
                . $where
                . ' GROUP BY a.id '
                . $orderby
        ;
        return $query;
    }

    /**
     * Build order by clause for the select command
     *
     * @return string order by clause
     */
    function _buildContentOrderBy()
    {
        $mainframe = & JFactory::getApplication('admin');
        $option = 'com_ezdocman';
        $filter_order = $mainframe->getUserStateFromRequest($option . 'category_filter_order', 'filter_order',
                'a.ordering', 'cmd');
        $filter_order_Dir = $mainframe->getUserStateFromRequest($option . 'category_filter_order_Dir',
                'filter_order_Dir', '', 'word');
        $orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir;
        return $orderby;
    }

    /**
     * Build the where clause
     *
     * @return string
     */
    function _buildContentWhere()
    {
        $mainframe = & JFactory::getApplication('admin');
        $option = 'com_ezdocman';
        $db = & JFactory::getDBO();
        $search = $mainframe->getUserStateFromRequest($option . 'filter_search', 'search', '', 'string');
        $search = utf8_strtolower($search);
        $language = $mainframe->getUserStateFromRequest($option . 'category_language', 'filter_language', '', 'string');
        $where = array();
        $filter_state = $mainframe->getUserStateFromRequest($option . 'filter_state', 'filter_state', '', 'word');

        if ($filter_state == 'P')
        {
            $where[] = ' a.published=1 ';
        }
        elseif ($filter_state == 'U')
            $where[] = ' a.published = 0';
        if ($search)
        {
            $where[] = '( LOWER(a.title) LIKE ' . $db->Quote('%' . $search . '%', false) . ' OR  '
                    . '  LOWER(a.alias) LIKE ' . $db->Quote('%' . $search . '%', false) . ' OR  '
                    . '  LOWER(a.job_id) LIKE ' . $db->Quote('%' . $search . '%', false) . ' OR  '
                    . '  LOWER(js.name) LIKE ' . $db->Quote('%' . $search . '%', false) . ')';
        }
        if ($language)
        {
            $where[] = 'a.language IN (' . $db->Quote($language) . ',' . $db->Quote('*') . ', "")';
        }
        $where = ( count($where) ? ' WHERE ' . implode(' AND ', $where) : '' );
        return $where;
    }

    function delete($data)
    {
        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_edocman/table');
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        foreach ($data as $cid)
        {
            $cat = JTable::getInstance("category", "EDocmanTable");
            $cat->load($cid);
            $jid = $cat->job_id;
            $result = $cat->delete($cat->id);
            if ($jid && $result)
            {
                $sql = "DELETE FROM #__jobsite_info where jobsite_id  = " . $jid;
                $db->setQuery($sql)->execute();
            }
        }

        return true;
    }

}
