<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modeladmin');

class EZDocmanModelJobsite extends JModelAdmin
{

    public function getTable($type = 'Jobsite', $prefix = 'EZDocmanTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getForm($data = array(), $loadData = true)
    {
        // Get the form.
        $form = $this->loadForm('com_ezdocman.jobsite', 'jobsite', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form))
        {
            return false;
        }
        if (empty($data))
            $data = $this->loadFormData();

        return $form;
    }

    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_ezdocman.edit.jobsite.data', array());
        if (empty($data))
        {
            $data = $this->getItem();
        }
        return $data;
    }

    public function getItem($pk = null)
    {
        $item = parent::getItem($pk);
        if ($item->id == null)
        {
            
        }
        return $item;
    }

    public function save($data)
    {
        $row = $this->getTable();
        $id = $data["id"];

        if ($data["id"])
        {
            $row->load($id);
            $oldrow = clone($row);
            $old_jobsite_id = $row->jobsite_id;
        }
        else
        {
            $old_jobsite_id = $data["jobsite_id"];
        }

        $row->bind($data);
        $row->check();
        if (!$row->store())
        {
            return false;
        }

        if ($row->jobsite_id != $old_jobsite_id)
        {
            $db = JFactory::getDBO();
            $sql = 'UPDATE `#__edocman_categories` SET job_id = ' . $row->jobsite_id . ' WHERE job_id = ' . $old_jobsite_id;
            $result = $db->setQuery($sql)->execute();

            if (!$result)
            {
                if ($id)
                {
                    $oldrow->store();
                }
                return false;
            }
        }
        return true;
    }

    public function unpublish($data)
    {
        $row = $this->getTable();

        $id = $data["id"];
        if ($id)
        {
            $row->load($id);
            $jobsite_id = $row->jobsite_id;
        }
        else
        {
            $jobsite_id = $data["jobsite_id"];
        }

        $sql = 'UPDATE `#__edocman_categories` SET published = 0 WHERE job_id = ' . $jobsite_id;
        $db = JFactory::getDBO();
        return $db->setQuery($sql)->execute();
    }

}
