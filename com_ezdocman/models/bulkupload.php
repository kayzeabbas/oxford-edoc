<?php

/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');
jimport('joomla.filesystem.folder');

/**
 * Edocman Component Import Model
 *
 * @package		Joomla
 * @subpackage	Edocman
 * @since 1.5
 */
class EZDocmanModelBulkUpload extends JModelLegacy
{

    static $PHP_UPLOAD_ERRORS = [
        1 => 'File too large',
        2 => 'File too large',
        3 => 'The file was only partially uploaded',
        4 => 'No file was uploaded',
        6 => 'Error (6)',
        7 => 'Error (7)',
        8 => 'Error (8)',
    ];
    var $_db;
    var $counts = [
        'total'   => 0,
        'new'     => 0,
        'updated' => 0,
        'fail'    => 0,
        'review'  => 0,
        'perCat'  => []
    ];
    var $review_catID;

    public function __construct($config = array())
    {
        parent::__construct($config);
        $this->_db = &JFactory::getDbo();

        // setup review category
        $sql = "SELECT `id` FROM #__edocman_categories WHERE title = 'review' ";
        $this->_db->setQuery($sql);
        $this->review_catID = $this->_db->loadResult();

        if (!$this->review_catID)
        {
            $row = JTable::getInstance("category", "EDocmanTable");
            $row->bind([
                'title'  => 'review',
                'access' => 1
            ]);
            $row->check();
            $row->store();
            $this->review_catID = $row->id;
        }
    }

    /**
     * Store the configuration data
     *
     * @param array $post
     */
    function uploadFiles($post, $files)
    {
        $this->counts['total'] = count($files);
        if ($this->counts['total'] <= 0)
            return false;

        $config = EdocmanHelper::getConfig();
        $rootDir = $config->documents_path;
        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_edocman/table');

        if (isset($post['distribution_time']))
            $distribution_time = $post['distribution_time'];
        else
            $distribution_time = null;

        $catID = is_int((int)$post['category_id']) ? (int)$post['category_id'] : -1;
        $auto = $catID == -1;

        if (!$auto)
            list($path, $categoryAccess, $catName) = $this->getCategoryDetails($catID);

        $errors = [];
        $warnings = [];
        for ($i = 0; $i < $this->counts['total']; $i++)
        {
            $file = $files[$i];

            $filename_raw = $file['name'];
            $filename_clean = JFile::makeSafe($filename_raw);

            // handle PHP upload errors
            if ($file['error'] != UPLOAD_ERR_OK)
            {
                $errors[] = $filename_raw . ': ' . self::$PHP_UPLOAD_ERRORS[$file['error']];
                continue;
            }

            // determine category
            if ($auto)
            {
                $parsed = $this->parseFilename($filename_raw);
                if (!$parsed)
                {
                    $catID = $this->review_catID;
                    $warnings[] = "$filename_raw: invalid filename format, invoice moved to review category";
                }
                else
                {
                    list($jobID, $invoiceDate) = $parsed;

                    $catID = $this->getJobCatId($jobID);
                    if (!$catID)
                    {
                        $catID = $this->review_catID;
                        $warnings[] = "$filename_raw: jobsite ID not found, invoice moved to review category";
                    }
                }

                list($path, $categoryAccess, $catName) = $this->getCategoryDetails($catID);
            }

            // UPLOAD FILE
            $savedFilePath = $path ? "$rootDir/$path/$filename_clean" : "$rootDir/$filename_clean";

            //      add unique id to end of name (before extension): test.pdf => test.{uid}.pdf
//            if (JFile::exists($savedFilePath))
//            {
//                $uid = uniqid();
//                $savedFilePath = preg_replace('#\.[^\.]*$#', ".$uid$0", $savedFilePath, 1);
//            }

            if (!JFile::upload($file['tmp_name'], $savedFilePath))
            {
                $errors[] = "$filename_raw: upload failed";
                $this->counts['fail'] ++;
                continue;
            }

            // ADD DB RECORD
            $docID = $this->getDocId($filename_clean);
            $documentRecord = JTable::getInstance('document', 'EDocmanTable');
            $documentRecord->bind([
                'id'                => $docID,
                'title'             => preg_replace('#\.[a-z0-9]+$#i', '', $filename_raw), // remove extension for title
                'original_filename' => $filename_raw,
                'filename'          => $filename_clean,
                'access'            => $categoryAccess,
                'published'         => 1,
                'distribution_time' => $distribution_time,
                'invoice_time'      => $invoiceDate,
                'type'              => 'invoice',
            ]);
            $documentRecord->check();

            //      unfortunately store() reads category ID directly from POST, so set it here
            $_POST['jform']['category_id'] = $catID;
            if (!$documentRecord->store())
            {
                $errors[] = "$filename_clean: document record failed";
                $errors[] = $documentRecord->getError();
                JFile::delete($savedFilePath);
                $this->counts['fail'] ++;
                continue;
            }

            // UPDATE COUNTS
            $this->counts[$docID ? 'updated' : 'new'] ++;

            if ($auto)
            {
                if ($catID == $this->review_catID)
                    $this->counts['review'] ++;
                else
                {
                    if (!isset($this->counts['perCat'][$catName]))
                        $this->counts['perCat'][$catName] = 0;
                    $this->counts['perCat'][$catName] ++;
                }
            }
            else
            {
                if (!isset($this->counts['perCat'][$catName]))
                    $this->counts['perCat'][$catName] = 0;
                $this->counts['perCat'][$catName] ++;
            }

            // UPDATE DB FOR NOTIFICATION EMAILS
            $sql = "REPLACE INTO #__edocman_categories_updated VALUES ( $documentRecord->id , NOW(), $catID )";
            $this->_db->setQuery($sql)->execute();

            //UPDATE DB FOR CATEGORIES
            $sql = "REPLACE INTO #__edocman_document_category VALUES (NULL, $documentRecord->id , $catID, 1 )";
            $this->_db->setQuery($sql)->execute();
        }

        $this->setMessages($auto, $errors, $warnings);

        return true;
    }

    private function getCategoryDetails($catID)
    {
        $sql = 'SELECT `path`, `access`, `title` FROM #__edocman_categories WHERE id = ' . $catID;
        $cat = $this->_db->setQuery($sql)->loadAssoc();

        return [$cat['path'], (int) $cat['access'], $cat['title']];
    }

    private function parseFilename($filename)
    {
        $matches = [];
        if (!preg_match('#^([a-z\d]+)-(\d{6}).*\.[^.]+#i', $filename, $matches))
            return false;

        list(, $jobID, $date, $ext) = $matches;

        $invoiceDate = DateTime::createFromFormat('mdy', $date);
        if (!$invoiceDate)
            return false;

        return [$jobID, $invoiceDate, $ext];
    }

    private function getJobCatId($jobID)
    {
        $sql = "SELECT `id` FROM #__edocman_categories WHERE job_id = '$jobID'";

        $this->_db->setQuery($sql);
        return $this->_db->loadResult();
    }

    private function getDocId($filename)
    {
        $sql = "SELECT `id` FROM #__edocman_documents WHERE filename = '$filename'";

        $this->_db->setQuery($sql);
        $result = $this->_db->loadResult();
        return (int) $result;
    }

    private function setMessages($auto, $errors, $warnings)
    {
        $app = JFactory::getApplication();

        $counts = $this->counts;
        $fail = $counts['fail'] || count($errors);

        if ($fail)
        {
            $app->enqueueMessage('Errors occurred. Summary:', 'error');
            $app->enqueueMessage('Total: ' . $counts['total'], 'error');
            $app->enqueueMessage('Successful: ' . $counts['new'] . ' New, ' . $counts['updated'] . ' Updated', 'error');
            $app->enqueueMessage('Failed: ' . $counts['fail'], 'error');
        }
        else
        {
            $s = $counts['total'] == 1 ? '' : 's';
            $app->enqueueMessage($counts['total'] . " file$s uploaded successfully");
            $app->enqueueMessage($counts['new'] . ' New, ' . $counts['updated'] . ' Updated');
        }

        if ($auto && $counts['review'])
            $app->enqueueMessage('For review: ' . $counts['review'], 'warning');

        $app->enqueueMessage('Category Details:');
        foreach ($counts['perCat'] as $cat => $count)
        {
            $s = $count == 1 ? '' : 's';
            $app->enqueueMessage("$cat: $count");
        }

        foreach ($errors as $msg)
        {
            $app->enqueueMessage($msg, 'error');
        }

        foreach ($warnings as $msg)
        {
            $app->enqueueMessage($msg, 'warning');
        }
    }

}
