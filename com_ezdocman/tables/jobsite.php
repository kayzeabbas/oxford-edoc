<?php

// No direct access
defined('_JEXEC') or die;

// import Joomla table library
jimport('joomla.database.table');

class EZDocmanTableJobsite extends JTable
{

    /**
     * Constructor
     *
     * @param JDatabase A database connector object
     */
    public function __construct(&$db)
    {
        parent::__construct('#__jobsite_info', 'id', $db);
    }

}
