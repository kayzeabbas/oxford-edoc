<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
//error_reporting(0);
// import joomla controller library
jimport('joomla.application.component.controller');
//error_reporting(0);
require_once str_replace(array('\administrator','/administrator'),'',JPATH_BASE) . '/components/com_edocman/helper/helper.php';

//DS is deprecated
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

require_once JPATH_COMPONENT . DS . 'helpers' . DS . 'ezdocman.php';
// Get an instance of the controller prefixed by HelloWorld
$controller = JControllerLegacy::getInstance('EZDocman');
error_reporting(0);

// Get the task
$jinput = JFactory::getApplication()->input;
$task = $jinput->get('task', "", 'STR');

// Perform the Request task
$controller->execute($task);

// Redirect if set by the controller
$controller->redirect();
