<?php

/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Edocman helper.
 */
class EZDocmanHelper
{

    static private $scriptAdded = false;

    /**
     * Generate User Input Select
     * @param int $userId
     * @param int $catId
     */
    public static function getUserInputPerCat($userIds, $catId)
    {
        $tempIds = $userIds;
        $Ids = explode(',', $userIds);
        sort($Ids);
        $sortIds = implode(',', $Ids);
        // Initialize variables.
        $html = array();
        $link = 'index.php?option=com_ezdocman&view=users&layout=modal&tmpl=component&field=user_id&users=';
        // Initialize some field attributes.
        $attr = ' class="inputbox"';
        // Load the modal behavior script.
        JHtml::_('behavior.modal', 'a.modal_user_id');
        self::addScripts();

        $html[] = '<div class="fltlft">';
        $html[] = '	<input type="text" name="jform[user_ids_' . $catId . ']" id="jform_user_ids_' . $catId . '"' . ' value="' . $sortIds . '"'
                . $attr . ' />';
        $html[] = '</div>';
        // Create the user select button.
        $html[] = '<div class="button2-left">';
        $html[] = '<div class="blank">';
        $html[] = '<a class="modal_user_id" title="' . JText::_('JLIB_FORM_CHANGE_USER') . '"' . ' onclick="openModal(' . $catId . ')"'
                . ' rel="{handler: \'iframe\', size: {x: 800, y: 500}}">';
        $html[] = '	' . JText::_('JLIB_FORM_CHANGE_USER') . '</a>';
        $html[] = '</div>';
        $html[] = '</div>';

        return implode("\n", $html);
    }

    public static function addScripts()
    {
        if (self::$scriptAdded)
        {
            return;
        }
        $link = 'index.php?option=com_ezdocman&view=users&layout=modal&tmpl=component&field=user_id&users=';
        $script = array();
        $script[] = '	function jSelectUser_user_id(ids,cat) {';
        $script[] = '		document.getElementById("jform_user_ids_"+ cat).value =ids;';
        $script[] = '		SqueezeBox.close();';
        $script[] = '	}';
        $script[] = '	function openModal(cat){';
        $script[] = '	    box = SqueezeBox.initialize({});';
        $script[] = '	    var options = {handler:"iframe",size:{x:800,y:500}}; ';
        $script[] = '	    box.setOptions(box.presets, options); ';
        $script[] = '	    box.assignOptions()  ';
        $script[] = '	    box.setContent("iframe","' . $link . '" + $("jform_user_ids_" + cat).value + "&category=" + cat )';
        $script[] = '	}';
        JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

        self::$scriptAdded = true;
    }

}
