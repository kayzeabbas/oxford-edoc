<?php
/**
 * @version        1.7.5
 * @package        Joomla
 * @subpackage     EDocman
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2011 - 2016 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->filter_order;
$listDirn	= $this->state->filter_order_Dir;
$canOrder	= $user->authorise('core.edit.state', 'com_edocman');
$saveOrder	= $listOrder == 'tbl.ordering';
EdocmanHelper::chosen();
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{		
		if (task == 'add') 
		{			
			var form = document.adminForm ;
			if ($form->filter_category_id.value == 0) 
			{
				alert("<?php echo JText::_('EDOCMAN_CHOOSE_CAT_TO_ADD_DOCUMENTS'); ?>");
			}
		} 
	}
</script>


<form action="<?php echo JRoute::_('index.php?option=com_edocman&view=documents'); ?>" method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search pull-left">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
			<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->filter_search); ?>" title="<?php echo JText::_('Search'); ?>" class="search-query" />
			<button type="submit" class="btn"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" class="btn" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
		</div>
		<div class="filter-select pull-right">			
			<?php
				echo $this->lists['filter_category_id'];
                if ($this->config->activate_multilingual_feature) 
                {
                	echo $this->lists['filter_language'];    
                }
                echo $this->lists['filter_state'];
				if (version_compare(JVERSION, '3.0', 'ge'))
				{
					echo $this->pagination->getLimitBox();
				}
			?>			                                       
		</div>
	</fieldset>
	<div class="clearfix"></div>
	<table class="adminlist table table-striped">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this);" />
				</th>      
				<th style="text-align: left;">
					<?php echo JHtml::_('grid.sort',  'EDOCMAN_TITLE', 'tbl.title', $listDirn, $listOrder); ?>
				</th>						
				<th width="15%" style="text-align: left;">
					<?php echo JHtml::_('grid.sort',  'EDOCMAN_CATEGORY', 'cat.title', $listDirn, $listOrder); ?>
				</th>	
				<?php 
					if ($this->config->show_uploader_name_in_document_mamangement)
					{
					?>
						<th width="5%">
							<?php echo JHtml::_('grid.sort',  'EDOCMAN_UPLOADER', 'uc.username', $listDirn, $listOrder); ?>
						</th>
					<?php	
					}
				?>        
				<th width="5%">
					<?php echo JHtml::_('grid.sort',  'JPUBLISHED', 'tbl.published', $listDirn, $listOrder); ?>
				</th>                                
				<th width="10%">
					<?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ORDERING', 'tbl.ordering', $listDirn, $listOrder); ?>
					<?php if ($canOrder && $saveOrder) :?>
						<?php echo JHtml::_('grid.order',  $this->items, 'filesave.png', 'documents.saveorder'); ?>
					<?php endif; ?>
				</th>
				<th width="5%">
					<?php echo JHtml::_('grid.sort',  'EDOCMAN_HITS', 'hits', $listDirn, $listOrder); ?>
				</th>
				<th width="5%">
					<?php echo JHtml::_('grid.sort',  'EDOCMAN_DOWNLOADS', 'downloads', $listDirn, $listOrder); ?>
				</th>         
				<th width="10%" class="center">
					<?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ACCESS', 'access_level', $listDirn, $listOrder); ?>
				</th>                       
                <th width="1%" class="nowrap center">
                    <?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ID', 'tbl.id', $listDirn, $listOrder); ?>
                </th>                
			</tr>
		</thead>
		<tfoot>
			<tr>
				<?php
					if ($this->config->show_uploader_name_in_document_mamangement)
					{
						$cols = 10;
					}
					else
					{
						$cols = 9;
					}
				?>
				<td colspan="<?php echo $cols; ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->items as $i => $item) :
			$ordering	= ($listOrder == 'tbl.ordering');			
			$canEdit	= $user->authorise('core.edit',			'com_edocman.document.'.$item->id);
		    $canCheckin	= $user->authorise('core.admin', 'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
		    $canEditOwn	= $user->authorise('core.edit.own',		'com_edocman.document.'.$item->id) && $item->created_user_id == $userId;
		    $canChange	= $user->authorise('core.edit.state',	'com_edocman.document.'.$item->id) && $canCheckin;
			?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>    
				<td>
					<?php if ($item->checked_out) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'documents.', $canCheckin); ?>
    				<?php endif; ?>
    				<?php if ($canEdit || $canEditOwn) : ?>
    					<a href="<?php echo JRoute::_('index.php?option=com_edocman&task=document.edit&id='.$item->id);?>">
    						<?php echo $item->title; ?></a>
    				<?php else : ?>
    					<?php echo $this->escape($item->title); ?>
    				<?php endif; ?>
    				<p class="smallsub">
					<?php echo JText::sprintf('JGLOBAL_LIST_ALIAS', $this->escape($item->alias));?></p>
				</td> 					
				<td>
					<?php echo $item->category_title ; ?>
				</td>	
				<?php 
					if ($this->config->show_uploader_name_in_document_mamangement)
					{
					?>
						<td>
							<?php echo $item->username; ?>
						</td>
					<?php	
					}
				?>    		          
			    <td class="center">
				    <?php echo JHtml::_('jgrid.published', $item->published, $i, 'documents.', $canChange, 'cb'); ?>
			    </td>               
			    <td class="order">
				    <?php if ($canChange) : ?>
					    <?php if ($saveOrder) :?>
						    <?php if ($listDirn == 'asc') : ?>
							    <span><?php echo $this->pagination->orderUpIcon($i, true, 'documents.orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
							    <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, true, 'documents.orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
						    <?php elseif ($listDirn == 'desc') : ?>
							    <span><?php echo $this->pagination->orderUpIcon($i, true, 'documents.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
							    <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, true, 'documents.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
						    <?php endif; ?>
					    <?php endif; ?>
					    <?php $disabled = $saveOrder ?  '' : 'disabled="disabled"'; ?>
					    <input type="text" name="order[]" size="5" value="<?php echo $item->ordering;?>" <?php echo $disabled ?> class="input-mini" />
				    <?php else : ?>
					    <?php echo $item->ordering; ?>
				    <?php endif; ?>
			    </td>       
			    <td class="center">
			    	<?php echo (int)$item->hits ; ?>
			    </td>
			    <td class="center">
			    	<?php echo (int)$item->downloads ; ?>
			    </td>
			    <td class="center">
				    <?php echo $item->access_level ; ?>
				</td>                        
				<td class="center">
					<?php echo (int) $item->id; ?>
				</td>                
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>