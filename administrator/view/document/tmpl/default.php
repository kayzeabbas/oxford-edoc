<?php
/**
 * @version        1.7.5
 * @package        Joomla
 * @subpackage     EDocman
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2011 - 2016 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
EDocmanHelper::chosen('select.chosen');
$fields = $this->form->getGroup('params');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'cancel' || document.formvalidator.isValid(document.id('item-form'))) {					
			<?php echo $this->form->getField('description')->save(); ?>
			Joomla.submitform(task, document.getElementById('item-form'));
		} else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_edocman&view=document&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate" enctype="multipart/form-data">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
	<div class="row-fluid">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#general-page" data-toggle="tab"><?php echo JText::_('EDOCMAN_GENERAL');?></a></li>
			<li><a href="#permission-page" data-toggle="tab"><?php echo JText::_('Permissions');?></a></li>
			<li><a href="#publishing-details" data-toggle="tab"><?php echo JText::_('JGLOBAL_FIELDSET_PUBLISHING');?></a></li>
			<li><a href="#meta-options" data-toggle="tab"><?php echo JText::_('JGLOBAL_FIELDSET_METADATA_OPTIONS');?></a></li>
			<?php
			if($this->indexer == 1){
				?>
				<li><a href="#indexed-content" data-toggle="tab"><?php echo JText::_('EDOCMAN_INDEXED_CONTENT');?></a></li>
				<?php
			}
			?>
		</ul>
		<div class="tab-content">	
			<div class="tab-pane active row-fluid" id="general-page">
				<div class="span8">
					<fieldset class="adminform">
						<legend><?php echo JText::_('EDOCMAN_FIELDSET_DETAILS');?></legend>
						<table width="100%">
							<tr>
								<td class="key">
									<?php echo $this->form->getLabel('title'); ?>	
								</td>
								<td>
									<?php echo $this->form->getInput('title'); ?>
								</td>
							</tr>				
							<tr>
								<td class="key">
									<?php echo $this->form->getLabel('alias'); ?>	
								</td>
								<td>
									<?php echo $this->form->getInput('alias'); ?>
								</td>					
							</tr>	
							<tr>
								<td class="key">
									<?php echo $this->form->getLabel('document_version'); ?>	
								</td>
								<td>
									<?php echo $this->form->getInput('document_version'); ?>
								</td>					
							</tr>
							<tr>
								<td class="key">
									<?php echo $this->form->getLabel('category_id'); ?>	
								</td>
								<td  style="padding-bottom: 10px;">
									<?php echo $this->form->getInput('category_id'); ?>		
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo $this->form->getLabel('extra_category_ids'); ?>	
								</td>
								<td  style="padding-bottom: 5px;">
									<?php echo $this->form->getInput('extra_category_ids'); ?>		
								</td>
							</tr>
							<?php
			                    if ((int)$this->config->file_upload_method == 0) {
			                    ?>
			                    	<tr>
			        					<td>
			        						<?php echo $this->form->getLabel('filename'); ?>
			        					</td>
			        					<td>
			        						<?php echo $this->form->getInput('filename'); ?>        						
			        						<?php
			                                    if ($this->item->id) {
			                                        ?>
			                                        	<span style="padding-top: 3px; display: block;">
			                                        <?php
			                                        if ($this->item->original_filename) {
			                                            echo JText::_('Current File : ');                                    
			                                        ?>
			                                        	<a href="<?php echo 'index.php?option=com_edocman&task=document.download&id='.$this->item->id; ?>&Itemid=<?php echo EdocmanHelper::getItemid(); ?>"><?php echo $this->item->original_filename ; ?></a>
			                                        <?php        
			                                        }      
			                                        ?>
			                                        	</span>
			                                        <?php
			                                    }
			        						?>																							
			        					</td>
			        				</tr>
			                    <?php        
			                    } else {
			                    ?>
			                    	<tr>
			        					<td>
			        						<?php echo $this->form->getLabel('select_filename'); ?>
			        					</td>
			        					<td>
			        						<?php echo $this->form->getInput('select_filename'); ?>        																											
			        					</td>
			        				</tr>
			                    <?php    
			                    }
							?>							
							<tr>
								<td>
									<?php echo $this->form->getLabel('image'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('image'); ?>
									<?php 
										if ($this->item->image && file_exists(JPATH_ROOT.'/media/com_edocman/document/thumbs/'.$this->item->image)) {								
										?>																							
											<input type="checkbox" name="del_image" value="1" title="<?php echo JText::_('EDOCMAN_DEL_IMAGE_DESC'); ?>" />
											Delete <a href="<?php echo JURI::root().'media/com_edocman/document/thumbs/'.$this->item->image; ?>" target="_blank" class="modal"><strong>current document thumbnail</strong></a>?																							
										<?php	
										}
									?>
								</td>
							</tr>
                            <?php
                                if (!isset($this->config->access_level_inheritance) || $this->config->access_level_inheritance !== '1')
                                {
                                ?>
                                    <tr>
                                        <td>
                                            <?php echo $this->form->getLabel('access'); ?>
                                        </td>
                                        <td>
                                            <?php echo $this->form->getInput('access'); ?>
                                        </td>
                                    </tr>
                                <?php
                                }
                            ?>
							<tr>
								<td>
									<?php echo $this->form->getLabel('user_ids'); ?>
								</td>
								<td>						
									<?php echo EdocmanHelper::getUserInput($this->item->user_ids) ; ?>
								</td>
							</tr>			
							<tr>
								<td>
									<?php echo $this->form->getLabel('license_id'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('license_id'); ?>
								</td>
							</tr>				
							<tr>
								<td>
									<?php echo $this->form->getLabel('document_url'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('document_url'); ?>
								</td>
							</tr>
							<?php
							if($this->config->view_url){
							?>
							<tr>
								<td>
									<?php echo $this->form->getLabel('view_url'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('view_url'); ?>
								</td>
							</tr>
							<?php } ?>
							<tr>
								<td class="key">
									<?php echo $this->form->getLabel('indicators'); ?>	
								</td>
								<td>
									<?php echo $this->form->getInput('indicators'); ?>		
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo $this->form->getLabel('tags'); ?>	
								</td>
								<td>
									<?php echo $this->form->getInput('tags'); ?>		
								</td>
							</tr>
							<?php
			                    if ($this->config->activate_multilingual_feature) {
			                    ?>
			                    <tr>	
			                  		<td><?php echo $this->form->getLabel('language'); ?></td>
							        <td><?php echo $this->form->getInput('language'); ?></td>
							    </tr>      	
			                    <?php    
			                    }        
							?>
							<tr>
								<td>
									<?php echo $this->form->getLabel('published'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('published'); ?>
								</td>
							</tr>
						</table>							
						<div class="clr"></div>
						<?php echo $this->form->getLabel('short_description'); ?>
						<div class="clr"></div>
						<?php echo $this->form->getInput('short_description'); ?>
						<div class="clr"></div>
						<?php echo $this->form->getLabel('description'); ?>
						<div class="clr"></div>
						<?php echo $this->form->getInput('description'); ?>
						<div class="clr"></div>
						<?php echo $this->form->getLabel('document_history'); ?>
						<div class="clr"></div>
						<?php echo $this->form->getInput('document_history'); ?>
					</fieldset>
				</div>
				<?php
					if(count($fields))
					{
					?>
					<div class="span4">
						<fieldset class="adminform">
							<legend><?php echo JText::_('EDOCMAN_CUSTOM_FIELDS');?></legend>
							<table width="100%">
								<?php foreach($fields as $field): ?>
									<?php if ($field->hidden): ?>
										<tr>
											<td colspan="2">
												<?php echo $field->input; ?>
											</td>
										</tr>
									<?php else: ?>
										<tr>
											<td class="key">
												<?php echo $field->label; ?>
											</td>
											<td>
												<?php echo $field->input; ?>
											</td>
										</tr>
									<?php endif; ?>
								<?php endforeach; ?>
							</table>
						</fieldset>
					</div>
					<?php
					}
				?>
			</div>			
			<div class="tab-pane" id="permission-page">		
				<?php
					if ($this->canDo->get('core.admin'))
					{
						echo $this->form->getInput('rules');
					}						
				?>
			</div>
			<div class="tab-pane" id="publishing-details">			
				<?php echo $this->loadTemplate('options'); ?>			
			</div>
			<div class="tab-pane" id="meta-options">
				<?php echo $this->loadTemplate('metadata'); ?>	
			</div>
			<div class="tab-pane" id="indexed-content">
				<?php 
				if($this->item->indexed_content != ""){
					?>
					<div class="row-fluid">
						<div class="span12" style="border:1px solid #CCC;padding:10px;">
							<?php
							echo $this->item->indexed_content;
							?>
						</div>
					</div>
					<?php
				}else{
					echo '<div class="alert alert-no-items">'.JText::_('EDOCMAN_HASNOT_INDEX_CONTENT_OF_THIS_DOCUMENT_WILL_DO_IT_AFTER_SAVING').'</div>';
				}
				?>	
			</div>
		</div>					
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</div>
</form>