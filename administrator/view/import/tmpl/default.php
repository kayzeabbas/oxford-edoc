<?php
/**
 * @version		2.6.6
 * @package        Joomla
 * @subpackage	Documents Seller
 * @author         Tuan Pham Ngoc
 * @copyright	Copyright (C) 2018-2011 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die ;
	
JToolBarHelper::title(JText::_('EDOCMAN_IMPORT_DOCUMENTS'),'upload');
JToolBarHelper::save('save', JText::_('Import'));
JToolBarHelper::cancel('cancel');	 
?>
<form action="index.php?option=com_edocman&view=import" method="post" name="adminForm" id="adminForm">	
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
	<div style="float:left; width: 100%;">
		<fieldset class="adminform">
			<legend><?php echo JText::_('EDOCMAN_IMPORT_OPTIONS'); ?></legend>
				<table class="admintable" style="width: 100%;">				
					<tr>
						<td width="100" align="right" class="key">
							<?php echo  JText::_('Folder'); ?>
						</td>
						<td>
							<?php echo $this->lists['folder'] ; ?>
						</td>
						<td>
							<small>
								Choose the where your documents are stored and you want to import. If you want to scan the root folder (the <strong>Document path</strong> in the configuration), leave this field empty.
							</small>
						</td>
					</tr>
					<tr>
						<td width="100" align="right" class="key">
							File Extensions
						</td>
						<td>
							<input class="text_area" type="text" name="exts" id="exts" size="40" maxlength="250" value="<?php echo $this->config->allowed_file_types;?>" />
						</td>
						<td>
							<small>Choose the file types you want to scan and import into EDocman</small>.
						</td>
					</tr>							
					<tr>
						<td width="100" align="right" class="key">
							Category
						</td>
						<td>
							<?php echo $this->lists['category_id'] ; ?>
						</td>
						<td>
							<small>Choose the category to which the imported documents will be assigned</small>.
						</td>
					</tr>
					<tr>
						<td width="100" align="right" class="key">
							Access
						</td>
						<td>
							<?php echo $this->lists['access'] ; ?>
						</td>
						<td>
							<small>Choose the access level for the importing documents</small>.
						</td>
					</tr>
			</table>						
		</fieldset>				
	</div>
</div>
<div class="clr"></div>	
	<?php echo JHtml::_( 'form.token' ); ?>
	<input type="hidden" name="task" value="" />
</form>