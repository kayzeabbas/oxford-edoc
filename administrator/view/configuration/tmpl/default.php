<?php
/**
 * @version        1.7.5
 * @package        Joomla
 * @subpackage     EDocman
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2011 - 2016 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */
jimport('joomla.filesystem.file') ;
JHtml::_('behavior.tooltip');
// Set toolbar items for the page
JToolBarHelper::title(   JText::_( 'EDOCMAN_CONFIG' ),'cog');
JToolBarHelper::save();	
JToolBarHelper::cancel();
//$document = JFactory::getDocument();
//$document->addStyleSheet(JUri::root().'components/com_edocman/assets/css/tab.css');
//$document->addScript(JUri::root().'components/com_edocman/assets/js/bootstrap.min.js');
$editorPlugin = null;
if (JPluginHelper::isEnabled('editors', 'codemirror'))
{
	$editorPlugin = 'codemirror';
}
elseif(JPluginHelper::isEnabled('editor', 'none'))
{
	$editorPlugin = 'none';
}
if ($editorPlugin)
{
	$showCustomCss = 1;
}else{
	$showCustomCss = 0;
}
?>
<style>
	table.alignleft td {
		text-align: left;
	}
</style>
<form action="index.php?option=com_edocman&view=configuration" method="post" name="adminForm" id="adminForm">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<!--<div class="clearfix"></div>-->
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>
			<div class="row-fluid">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#general-page" data-toggle="tab"><?php echo JText::_('EDOCMAN_GENERAL');?></a></li>
					<li><a href="#message-page" data-toggle="tab"><?php echo JText::_('EDOCMAN_MESSAGES');?></a></li>			
					<li><a href="#theme-page" data-toggle="tab"><?php echo JText::_('EDOCMAN_THEMES');?></a></li>
					<li><a href="#sef-setting-page" data-toggle="tab"><?php echo JText::_('EDOCMAN_SEF_SETTINGS');?></a></li>
					<?php
					if($showCustomCss == 1){
					?>
					<li><a href="#custom-css-page" data-toggle="tab"><?php echo JText::_('EDOCMAN_CUSTOM_CSS');?></a></li>
					<?php
					}
					?>
					<li><a href="#download-id" data-toggle="tab"><?php echo JText::_('DOWNLOAD ID');?></a></li>
				</ul>	
				<div class="tab-content">	
					<div class="tab-pane active" id="general-page">									
						<table class="adminform" style="width:100%;">
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_DOCUMENT_PATH') ; ?>
								</td>
								<td width="40%">
									<input type="text" name="documents_path" class="inputbox" value="<?php echo $this->config->documents_path; ?>" size="50" />
									<?php 
										if (!is_writable($this->config->documents_path)) {
										?>
											<span style="color:red; font-weight: bold;"><?php echo JText::_('EDOCMAN_DOCUMENT_PATH_NOT_WRITABLE') ?></span>
										<?php	
										}
									?>
									<input type="button" class="button btn" onclick="resetPath();" value="<?php echo JText::_('EDOCMAN_RESET');?>" />
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_DOCUMENT_PATH_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_LOAD_JQUERY') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['load_jquery'];?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_LOAD_JQUERY_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_LOAD_TWITTER_BOOTSTRAP') ; ?>
								</td>
								<td width="40%">
								   <?php echo $this->lists['load_twitter_bootstrap'];?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_LOAD_TWITTER_BOOTSTRAP_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_TWITTER_BOOTSTRAP_VERSION') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['twitter_bootstrap_version'];?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_TWITTER_BOOTSTRAP_VERSION_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_ACCESS_LEVEL_INHERITANCE') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['access_level_inheritance'];?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_ACCESS_LEVEL_INHERITANCE_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_ACTIVATE_MULTILINGUAL_FEATURE') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['activate_multilingual_feature']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_ACTIVATE_MULTILINGUAL_FEATURE_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_ACTIVATE_HERACHICAL_FOLDER_STRUCTURE') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['activate_herachical_folder_structure']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_ACTIVATE_HERACHICAL_FOLDER_STRUCTURE_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_FILE_UPLOAD_METHOD'); ?>
								</td>
								<td>
									<?php echo $this->lists['file_upload_method']; ?>
								</td>
								<td>
									&nbsp; 
								</td>
							</tr>						
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_ALLOWED_FILE_TYPES') ; ?>
								</td>
								<td>
									<input type="text" name="allowed_file_types" class="inputbox" value="<?php echo $this->config->allowed_file_types; ?>" size="50" />
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_ALLOWED_FILE_TYPES_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_DELETE_FILE_WHEN_DELETE_DOCUMENT'); ?>
								</td>
								<td>
									<?php echo $this->lists['delete_file_when_document_deleted']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_DELETE_FILE_WHEN_DELETE_DOCUMENT_EXPLAIN'); ?>
								</td>				
							</tr>
							<tr>
								<td>
									<?php echo JText::_('EDOCMAN_HIDE_DOWNLOAD_BUTTON'); ?>
								</td>
								<td>
									<?php echo $this->lists['hide_download_button']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_HIDE_DOWNLOAD_BUTTON_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td>
									<?php echo JText::_('EDOCMAN_SHOW_VIEW_BUTTON'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_view_button']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_SHOW_VIEW_BUTTON_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td>
									<?php echo JText::_('Use Google Viewer'); ?>
								</td>
								<td>
									<?php echo $this->lists['use_googleviewer']; ?>
								</td>
								<td>
									<?php echo JText::_('Do you want to use Google Viewer function to view the document content'); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_FILE_EXTENSIONS_FOR_VIEWING') ; ?>
								</td>
								<td>
									<input type="text" name="exts_for_view" class="inputbox" value="<?php echo $this->config->exts_for_view; ?>" size="50" />
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_FILE_EXTENSIONS_FOR_VIEWING_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td>
									<?php echo JText::_('EDOCMAN_ENABLE_DOCUMENT_VIEW_URL'); ?>
								</td>
								<td>
									<?php echo $this->lists['view_url']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_ENABLE_DOCUMENT_VIEW_URL_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_MAX_UPLOAD_FILE_SIZE') ; ?>
								</td>
								<td>
									<input type="text" name="max_file_size" class="input-mini" value="<?php echo $this->config->max_file_size; ?>" size="5" />					
									<?php echo $this->lists['max_filesize_type'] ; ?>&nbsp;&nbsp;&nbsp;<?php echo JText::_('EDOCMAN_DEFAULT_INI_SETTING'); ?>: <strong><?php echo ini_get('upload_max_filesize'); ?></strong>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_MAX_UPLOAD_FILE_SIZE_EXPLAIN'); ?>
								</td>
							</tr>						
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_REQUIRE_APPROVE_DOCUMENT'); ?>
								</td>
								<td>
									<?php echo $this->lists['require_admin_approve']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_REQUIRE_APPROVE_DOCUMENT_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_CATEGORY_THUMB_WIDTH') ; ?>					
								</td>
								<td>
									<input type="text" name="category_thumb_width" class="input-mini" value="<?php echo $this->config->category_thumb_width; ?>" size="10" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_CATEGORY_THUMB_HEIGHT') ; ?>					
								</td>
								<td>
									<input type="text" name="category_thumb_height" class="input-mini" value="<?php echo $this->config->category_thumb_height; ?>" size="10" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_DOCUMENT_THUMB_WIDTH') ; ?>					
								</td>
								<td>
									<input type="text" name="document_thumb_width" class="input-mini" value="<?php echo $this->config->document_thumb_width; ?>" size="10" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_DOCUMENT_THUMB_HEIGHT') ; ?>					
								</td>
								<td>
									<input type="text" name="document_thumb_height" class="input-mini" value="<?php echo $this->config->document_thumb_height; ?>" size="10" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>																									
							<tr>
								<td class="key">
									<?php echo JText::_('Date Format') ; ?>					
								</td>
								<td>
									<input type="text" name="date_format" class="input-mini" value="<?php echo $this->config->date_format; ?>" size="10" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Days for new') ; ?>					
								</td>
								<td>
									<input type="text" name="day_for_new" class="input-mini" value="<?php echo $this->config->day_for_new; ?>" size="10" />
								</td>
								<td>
									<?php echo JText::_('Number of days which the document is marked as new'); ?>
								</td>
							</tr>														
							<tr>
								<td class="key">
									<?php echo JText::_('Downloads to hot') ; ?>					
								</td>
								<td>
									<input type="text" name="downloads_to_hot" class="input-mini" value="<?php echo $this->config->downloads_to_hot; ?>" size="10" />
								</td>
								<td>
									<?php echo JText::_('Number of downloads for the document so that It is marked as host'); ?>
								</td>
							</tr>
							<!--<tr>
								<td class="key">
									<?php echo JText::_('Activate Cart Feature'); ?>
								</td>
								<td>
									<?php echo $this->lists['enable_cart']; ?>
								</td>
								<td>
									if set to Yes, users can add serveral documents into "cart" before downloading
								</td>					
							</tr>
							-->
								
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_ENABLE_RSS') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['enable_rss']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_OVERWRITE_EXISTING_FILE') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['overwrite_existing_file']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_OVERWRITE_EXISTING_FILE_EXPLAIN') ; ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Log Downloads'); ?>
								</td>
								<td>
									<?php echo $this->lists['download_log']; ?>
								</td>
								<td>
									<?php echo JText::_('If set to Yes, whenever a user download a document, a record about the download will be created to log the download. You can see the log by accessing to Downloads Log function in the back-end'); ?>
								</td>
							</tr>
                            <tr>
                                <td class="key">
                                    <?php echo JText::_('EDOCMAN_RESET_DOWNLOAD_LOGS_AFTER_UPDATING'); ?>
                                </td>
                                <td>
                                    <?php echo $this->lists['reset_downloadlog']; ?>
                                </td>
                                <td>
                                    <?php echo JText::_('EDOCMAN_RESET_DOWNLOAD_LOGS_AFTER_UPDATING_EXPLAIN'); ?>
                                </td>
                            </tr>
							<tr>
								<td width="30%" class="key">
									<?php echo JText::_('EDOCMAN_COLLECT_DOWNLOADER_INFORMATION');?>
								</td>
								<td>
									<?php
									echo $this->lists['collect_downloader_information'] ;
									?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_COLLECT_DOWNLOADER_INFORMATION_EXPLAIN');?>
								</td>
							</tr>
							<tr>
								<td width="30%" class="key">
									<?php echo JText::_('EDOCMAN_ONETIME_COLLECTION');?>
								</td>
								<td>
									<?php
									echo $this->lists['onetime_collect'] ;
									?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_ONETIME_COLLECTION_EXPLAIN');?>
								</td>
							</tr>

							<tr>
								<td width="30%" class="key">
									<?php echo JText::_('EDOCMAN_DOWNLOAD_TYPE');?>
								</td>
								<td>
									<?php
									echo $this->lists['download_type'] ;
									?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_DOWNLOAD_TYPE_EXPLAIN');?>
								</td>
							</tr>

							<tr>
								<td class="key">
									<?php echo JText::_('Processing Content Plugin'); ?>
								</td>
								<td>
									<?php echo $this->lists['process_plugin']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>			
							
							<?php
								$comments = JPATH_ROOT.'/components/com_jcomments/jcomments.php';
								if (file_exists($comments))
								{
								?>
									<tr>
										<td class="key">
											<?php echo JText::_('JComment Integration'); ?>
										</td>
										<td>
											<?php echo $this->lists['jcomment_integration']; ?>
										</td>
										<td>
											&nbsp;
										</td>
									</tr>	
								<?php	
								}
								?>									
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('Show uploader name in documents management') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['show_uploader_name_in_document_mamangement']; ?>
								</td>
								<td>
									If you enable front-end document submission, you should set this to Yes so that you will know who is the owner of the document
								</td>
							</tr>
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_USING_DEFAULT_LICENSE') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['use_default_license']; ?>
								</td>
								<td>
									Select Yes if you want to use Default License for all documents uploaded at Front-end
								</td>
							</tr>
                            <tr>
								<td class="key" width="15%">
									<?php echo JText::_('EDOCMAN_SEARCH_WITH_ALL_SUB_CATEGORIES') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['search_with_sub_cats']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_SEARCH_WITH_ALL_SUB_CATEGORIES_EXPLAIN') ; ?>
								</td>
							</tr>
							<tr>
								<td class="key" width="15%">
									<?php echo JText::_('Login to Download') ; ?>
								</td>
								<td width="40%">
									<?php echo $this->lists['login_to_download']; ?>
								</td>
								<td>
									<?php echo JText::_('Do you want to show button Login to Download') ; ?>
								</td>
							</tr>
						</table>
					</div>	
					<div class="tab-pane" id="message-page">		
						<table class="adminform" width="100%">
							<tr>
								<td class="key" width="18%">
									<?php echo JText::_('Notification Emails') ; ?>
								</td>
								<td>
									<input type="text" name="notification_emails" class="inputbox" value="<?php echo $this->config->notification_emails; ?>" size="50" />
								</td>
								<td>
									Email of users who will receive notificaiton when someone upload documents from front-end.You can put multiple email here, command seperated (For example sales@joomdonation.com,accounting@joomdonation.com)
								</td>
							</tr>	
							<tr>
								<td class="key">
									<?php echo JText::_('Download notification') ; ?>
								</td>
								<td>
									<?php echo $this->lists['download_notification']; ?>
								</td>									
								<td>
									If set to Yes, administrators will received notification email when someone download free documents
								</td>					
							</tr>					
							<tr>
								<td class="key">
									<?php echo JText::_('Download notification email subject'); ?>
								</td>
								<td>
									<input type="text" name="download_email_subject" class="inputbox" value="<?php echo $this->config->download_email_subject; ?>" size="70" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>			
							<tr>
								<td class="key">
									<?php echo JText::_('Download email body'); ?>
								</td>
								<td>
									<textarea rows="10" cols="70" name="download_email_body" class="input-xxlarge"><?php echo $this->config->download_email_body;?></textarea>
								</td>
								<td>
									Available tags : [USERNAME], [DOCUMENT_TITLE], [USER_IP]
								</td>
							</tr>

							<tr>
								<td class="key">
									<?php echo JText::_('Download link email subject'); ?>
								</td>
								<td>
									<input type="text" name="download_link_email_subject" class="inputbox" value="<?php echo $this->config->download_link_email_subject; ?>" size="70" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Download link email body'); ?>
								</td>
								<td>
									<textarea rows="10" cols="70" name="download_link_email_body" class="input-xxlarge"><?php echo $this->config->download_link_email_body;?></textarea>
								</td>
								<td>
									Available tags : [NAME], [EMAIL], [DOCUMENT_TITLE], [DOWNLOAD_LINK]
								</td>
							</tr>

							<tr>
								<td class="key">
									<?php echo JText::_('Download Complete Message'); ?>
								</td>
								<td>
									<textarea rows="10" cols="70" name="download_complete_message" class="input-xxlarge"><?php echo $this->config->download_complete_message;?></textarea>
								</td>
								<td>
									Available tags : [DOCUMENT_TITLE], [DOWNLOAD_LINK]
								</td>
							</tr>

							<tr>
								<td class="key">
									<?php echo JText::_('Download Complete Message (In case Download Type set to Send Download Link)'); ?>
								</td>
								<td>
									<textarea rows="10" cols="70" name="download_complete_message_send_download_link" class="input-xxlarge"><?php echo $this->config->download_complete_message_send_download_link;?></textarea>
								</td>
								<td>
									Available tags : [DOCUMENT_TITLE]
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Upload Notification') ; ?>
								</td>
								<td>
									<?php echo $this->lists['upload_notification']; ?>
								</td>									
								<td>
									If set to Yes, administrators will received notification email when someone download free documents
								</td>					
							</tr>					
							<tr>
								<td class="key">
									<?php echo JText::_('Upload notification email subject'); ?>
								</td>
								<td>
									<input type="text" name="upload_email_subject" class="inputbox" value="<?php echo $this->config->upload_email_subject; ?>" size="70" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>			
							<tr>
								<td class="key">
									<?php echo JText::_('Upload email body'); ?>
								</td>
								<td>
									<textarea rows="10" cols="70" name="upload_email_body" class="input-xxlarge"><?php echo $this->config->upload_email_body;?></textarea>
								</td>
								<td>
									Available tags : [USERNAME], [DOCUMENT_TITLE], [USER_IP]
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Document assigned notification') ; ?>
								</td>
								<td>
									<?php echo $this->lists['document_assigned_notification']; ?>
								</td>									
								<td>
									If set to Yes, the system will sent notification email to user when there is document assigned to him
								</td>					
							</tr>	
							<tr>
								<td class="key">
									<?php echo JText::_('Document assigned email subject'); ?>
								</td>
								<td>
									<input type="text" name="document_assigned_email_subject" class="inputbox" value="<?php echo $this->config->document_assigned_email_subject; ?>" size="70" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>			
							<tr>
								<td class="key">
									<?php echo JText::_('Document assigned email body'); ?>
								</td>
								<td>
									<textarea rows="10" cols="70" name="document_assigned_email_body" class="input-xxlarge"><?php echo $this->config->document_assigned_email_body;?></textarea>
								</td>
								<td>
									Available tags :[NAME], [USERNAME], [OWNER_USERNAME], [OWNER_NAME], [DOCUMENT_TITLE]
								</td>
							</tr>

							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_EMAIL_SHARE_SUBJECT'); ?>
								</td>
								<td>
									<input type="text" name="document_share_email_subject" class="inputbox" value="<?php echo $this->config->document_share_email_subject; ?>" size="70" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>			
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_EMAIL_SHARE_CONTENT'); ?>
								</td>
								<td>
									<textarea rows="10" cols="70" name="document_share_email_content" class="input-xxlarge"><?php echo $this->config->document_share_email_content;?></textarea>
								</td>
								<td>
									Available tags :[NAME], [FRIEND_NAME], [DOCUMENT_TITLE], [LINK], [MESSAGE]
								</td>
							</tr>

							<tr>
								<td class="key">
									<?php echo JText::_('Document upload form header text'); ?>
								</td>
								<td>
									<textarea rows="10" cols="70" name="header_text" class="input-xxlarge"><?php echo $this->config->header_text;?></textarea>
								</td>
								<td>
									Available tags : [CATEGORY]
								</td>
							</tr>
						</table>
					</div>		
					<div class="tab-pane" id="theme-page">	
						<table class="adminform" width="100%">
							<tr>
								<td colspan="3" class="key">
									<strong><?php echo JText::_('Front-end upload'); ?></strong>
								</td>
							</tr>
							<tr>
								<td width="20%" class="key">
									<?php echo JText::_('Use simple front-end upload form'); ?>
								</td>
								<td width="15%">
									<?php echo $this->lists['use_simple_upload_form']; ?>
								</td>
								<td>
									If set to Yes, users will just have enter some basic information like Title, choose File, description of document when upload documents from front-end
								</td>		
							</tr>
							<tr>
								<td colspan="3" class="key">
									<strong><?php echo JText::_('EDOCMAN_GENERAL'); ?></strong>
								</td>
							</tr>
							<tr>
								<td width="20%" class="key">
									<?php echo JText::_('EDOCMAN_SHOW_DEFAULT_CATEGORY_THUMBNAIL'); ?>
								</td>
								<td width="15%">
									<?php echo $this->lists['show_default_category_thumbnail']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_SHOW_DEFAULT_CATEGORY_THUMBNAIL_DESC'); ?>
								</td>		
							</tr>
							<tr>
								<td width="20%" class="key">
									<?php echo JText::_('EDOCMAN_SHOW_DEFAULT_DOCUMENT_THUMBNAIL'); ?>
								</td>
								<td width="15%">
									<?php echo $this->lists['show_default_document_thumbnail']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_SHOW_DEFAULT_DOCUMENT_THUMBNAIL_DESC'); ?>
								</td>		
							</tr>
							<tr>
								<td width="20%" class="key">
									<?php echo JText::_('EDOCMAN_SHOW_ICON_BESIDE_TITLE'); ?>
								</td>
								<td width="15%">
									<?php echo $this->lists['show_icon_beside_title']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_SHOW_ICON_BESIDE_TITLE_DESC'); ?>
								</td>		
							</tr>
							<tr>
								<td width="20%" class="key">
									<?php echo JText::_('Use Download Link to Document title'); ?>
								</td>
								<td>
									<?php echo $this->lists['use_download_link_instead_of_detail_link']; ?>
								</td>
								<td>
									<?php echo JText::_('If set to Yes, click on Document Title will download the document instead of redirecting to Document Detail page'); ?>
								</td>
							</tr>																		
							<tr>
								<td colspan="3" class="key">
									<strong><?php echo JText::_('Categories View'); ?></strong>
								</td>
							</tr>			
							<tr>
								<td width="20%" class="key">
									<?php echo JText::_('Show empty Categories'); ?>
								</td>
								<td width="15%">
									<?php echo $this->lists['show_empty_cat']; ?>
								</td>
								<td>
									&nbsp;
								</td>				
							</tr>				
							<tr>
								<td class="key">
									<?php echo JText::_('Show Number of Documents'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_number_documents']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Categories per Row'); ?>
								</td>
								<td>
									<?php echo $this->lists['number_categories_per_row']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Categories per Page'); ?>
								</td>
								<td>
									<input type="text" name="number_categories" class="input-mini" value="<?php echo $this->config->number_categories; ?>" size="10" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							
							<tr>
								<td colspan="3" class="key">
									<strong><?php echo JText::_('Sub-categories'); ?></strong>
								</td>
							</tr>			
							<tr>
								<td class="key">
									<?php echo JText::_('Show sub-categories icon'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_subcategory_icon']; ?>
								</td>
								<td>
									&nbsp;
								</td>				
							</tr>				
							<tr>
								<td class="key">
									<?php echo JText::_('Show sub-categories description'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_subcategory_description']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Number sub-categories per row'); ?>
								</td>
								<td>
									<?php echo $this->lists['number_subcategories']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td colspan="3" class="key">
									<strong><?php echo JText::_('Category View'); ?></strong>
								</td>
							</tr>											
							<tr>
								<td class="key">
									<?php echo JText::_('Documents per Page'); ?>
								</td>
								<td>
									<input type="text" name="number_documents" class="input-mini" value="<?php echo $this->config->number_documents; ?>" size="10" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Number of Columns in Column Layout'); ?>
								</td>
								<td>
									<input type="text" name="number_columns" class="input-mini" value="<?php echo $this->config->number_columns; ?>" size="10" />
								</td>	
								<td>
									&nbsp;
								</td>			
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show Document Detail in Popup'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_detail_in_popup']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>			
							<tr>
								<td class="key">
									<?php echo JText::_('Show Publish Date'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_publish_date']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Hide Details button'); ?>
								</td>
								<td>
									<?php echo $this->lists['hide_details_button']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show sort options'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_sort_options']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Default sort option'); ?>
								</td>
								<td>
									<?php echo $this->lists['default_sort_option']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Default sort direction'); ?>
								</td>
								<td>
									<?php echo $this->lists['default_sort_direction']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td colspan="3" class="key">
									<strong><?php echo JText::_('Category Table Layout'); ?></strong>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show File Type'); ?>
								</td>
								<td>
									<?php echo $this->lists['category_table_show_filetype']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show File size'); ?>
								</td>
								<td>
									<?php echo $this->lists['category_table_show_filesize']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show header'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_tablelayoutheader']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td colspan="3" class="key">
									<strong><?php echo JText::_('Document Detail View'); ?></strong>
								</td>
							</tr>
		
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_SHOW_SHARING_BUTTON'); ?>
								</td>
								<td>
									<?php echo $this->lists['turn_on_sharing']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>

							<tr>
								<td>
									<?php echo JText::_('EDOCMAN_SHOW_VERSION'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_document_version']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_SHOW_VERSION_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('EDOCMAN_SHOW_SOCIAL_SHARING_BUTTONS'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_social_sharing_buttons']; ?>
								</td>
								<td>
									<?php echo JText::_('EDOCMAN_SHOW_SOCIAL_SHARING_BUTTONS_EXPLAIN'); ?>
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show Hits'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_hits']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show Number Downloaded'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_number_downloaded']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show File Name'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_filename']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>	
							<tr>
								<td class="key">
									<?php echo JText::_('Show File Size'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_filesize']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>				
							<tr>
								<td class="key">
									<?php echo JText::_('Show File Type'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_filetype']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show Creation User'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_creation_user']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show Creation Date'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_creation_date']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="key">
									<?php echo JText::_('Show Modify Date'); ?>
								</td>
								<td>
									<?php echo $this->lists['show_modified_date']; ?>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>							
						</table>	
					</div>
					<div class="tab-pane" id="sef-setting-page">
						<table class="adminform">
							<tr>
								<td width="30%" class="key">
									Insert Document ID
								</td>
								<td>
									<?php
										echo $this->lists['insert_document_id'] ;
									?>					
								</td>
								<td>
									If set to Yes, Document ID will be prepended to document title, which is needed in case you have documents with same name
								</td>
							</tr>

							<tr>
								<td width="30%" class="key">
									Insert Category ID
								</td>
								<td>
									<?php
										echo $this->lists['insert_category_id'] ;
									?>
								</td>
								<td>
									If set to Yes, Category ID will be prepended to Category title, which is needed in case you have categories with same name
								</td>
							</tr>
							<tr>
								<td width="30%" class="key">
									Insert Category
								</td>
								<td>
									<?php
										echo $this->lists['insert_category'] ;
									?>					
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
						</table>
					</div>
					<?php
					if($showCustomCss == 1){
					?>
					<div class="tab-pane" id="custom-css-page">
						<table  width="100%">
							<tr>
								<td>
									<?php
									$customCss = '';
									if (file_exists(JPATH_ROOT.'/components/com_edocman/assets/css/custom.css'))
									{
										$customCss = file_get_contents(JPATH_ROOT.'/components/com_edocman/assets/css/custom.css');
									}
									echo JEditor::getInstance($editorPlugin)->display('custom_css', $customCss, '100%', '550', '75', '8', false, null, null, null, array('syntax' => 'css'));
									?>
								</td>
							</tr>
						</table>
					</div>
					<?php } ?>
					<div class="tab-pane" id="download-id">
						<table class="adminform">
							<tr>
								<td width="30%" class="key">
									Download ID
								</td>
								<td>
									<input type="text" name="download_id" class="input-xlarge" value="<?php echo $this->config->download_id; ?>" size="50" />
									<BR />
									Enter your <strong>Download ID</strong> into this config option to be able to use Joomla Update to update your site to latest version of Edocman whenever there is new version available. To register Download ID, please go to: <a href="http://joomdonation.com" target="_blank">www.joomdonation.com</a> and click on menu <a href="http://joomdonation.com/download-ids.html" target="_blank">Download ID</a>. <BR /><strong>Notice:</strong> You should login before you access to this page.
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	<input type="hidden" name="option" value="com_edocman" />
	<input type="hidden" name="task" value="" />	
	<script type="text/javascript">
		function resetPath() {
			var form = document.adminForm ;
			var path = '<?php echo $this->path; ?>';
			form.documents_path.value = path ;
		}
	</script>
</form>
