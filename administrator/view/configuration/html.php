<?php
/**
 * @version        1.7.5
 * @package        Joomla
 * @subpackage     EDocman
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2011 - 2016 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die ;
/**
 * HTML View class for Edocman component
 *
 * @static
 * @package        Joomla
 * @subpackage     EDocman
 */
class EdocmanViewConfigurationHtml extends OSViewHtml
{
	public static function showCheckboxfield($fieldname,$fieldvalue,$option1='',$option2=''){
        if($option1 == ""){
            $option1 = JText::_('JYES');
        }
        if($option2 == ""){
            $option2 = JText::_('JNO');
        }
        if (version_compare(JVERSION, '3.0', 'lt')) {
            $optionArr = array();
            $optionArr[] = JHTML::_('select.option',1,$option1);
            $optionArr[] = JHTML::_('select.option',0,$option2);
            return JHTML::_('select.genericlist',$optionArr,$fieldname,'class="input-mini chosen"','value','text',$fieldvalue);
        }else{
            $name = $fieldname;
            if(intval($fieldvalue) == 0){
                $checked2 = 'checked="checked"';
                $checked1 = "";
            }else{
                $checked1 = 'checked="checked"';
                $checked2 = "";
            }
			ob_start();
            ?>
            <fieldset id="jform_params_<?php echo $name;?>" class="radio btn-group">
                <input type="radio" id="jform_params_<?php echo $name;?>0" name="<?php echo $name; ?>" value="1" <?php echO $checked1;?>/>
                <label for="jform_params_<?php echo $name;?>0"><?php echo $option1;?></label>
                <input type="radio" id="jform_params_<?php echo $name;?>1" name="<?php echo $name; ?>" value="0" <?php echO $checked2;?>/>
                <label for="jform_params_<?php echo $name;?>1"><?php echo $option2;?></label>
            </fieldset>
	        <?php
			$radio = ob_get_contents();
			ob_end_clean();
			return $radio;
        }
    }

	function display($tpl = null)
	{		
		$config = EdocmanHelper::getConfig();		
		$lists['overwrite_existing_file'] = self::showCheckboxfield('overwrite_existing_file',$config->overwrite_existing_file);   

		$lists['enable_rss'] = self::showCheckboxfield('enable_rss',$config->enable_rss); 

		$lists['process_plugin'] = self::showCheckboxfield('process_plugin',$config->process_plugin);  

		$lists['download_log'] = self::showCheckboxfield('download_log',$config->download_log);  

        $lists['load_twitter_bootstrap'] = self::showCheckboxfield('load_twitter_bootstrap',isset($config->load_twitter_bootstrap) ? $config->load_twitter_bootstrap : 1); 


		
		$lists['load_jquery'] = self::showCheckboxfield('load_jquery',isset($config->load_jquery) ? $config->load_jquery : 1);

		$lists['activate_herachical_folder_structure'] = self::showCheckboxfield('activate_herachical_folder_structure',$config->activate_herachical_folder_structure); 

		$lists['activate_multilingual_feature'] = self::showCheckboxfield('activate_multilingual_feature',$config->activate_multilingual_feature);

        $lists['access_level_inheritance'] = self::showCheckboxfield('access_level_inheritance',$config->access_level_inheritance); 

		$options = array() ;
		$options[] = JHtml::_('select.option', 0, JText::_('Upload new file for each document'));
		$options[] = JHtml::_('select.option', 1, JText::_('Choose file from server'));
		
		$lists['file_upload_method'] = JHtml::_('select.genericlist', $options, 'file_upload_method', 'class="inputbox chosen"', 'value', 'text', $config->file_upload_method);
		
		$options = array() ;
		$options[] =  JHtml::_('select.option',  0, JText::_('Select'));
		$options[] = JHtml::_('select.option', '1', JText::_('Byte'));
		$options[] = JHtml::_('select.option', '2', JText::_('Kb'));
		$options[] = JHtml::_('select.option', '3', JText::_('Mb'));
		$lists['max_filesize_type'] = JHtml::_('select.genericlist', $options, 'max_filesize_type', 'class="input-small chosen"', 'value', 'text', $config->max_filesize_type ? $config->max_filesize_type : 3);
		
		$lists['require_admin_approve'] = self::showCheckboxfield('require_admin_approve',$config->require_admin_approve);  
																								
		$lists['download_notification'] = self::showCheckboxfield('download_notification',$config->download_notification);  

		//User upload settings			
		$lists['upload_notification'] = self::showCheckboxfield('upload_notification',$config->upload_notification);  
		
		$lists['document_assigned_notification'] = self::showCheckboxfield('document_assigned_notification',$config->document_assigned_notification); 

		$lists['jcomment_integration'] = self::showCheckboxfield('jcomment_integration',@$config->jcomment_integration);  
		
		$lists['delete_file_when_document_deleted'] = self::showCheckboxfield('delete_file_when_document_deleted',$config->delete_file_when_document_deleted);  
		
		$lists['show_default_category_thumbnail'] = self::showCheckboxfield('show_default_category_thumbnail',$config->show_default_category_thumbnail);  

		$lists['show_default_document_thumbnail'] = self::showCheckboxfield('show_default_document_thumbnail',$config->show_default_document_thumbnail);
		
		$lists['show_icon_beside_title'] = self::showCheckboxfield('show_icon_beside_title',$config->show_icon_beside_title); 
		
		$lists['show_empty_cat'] = self::showCheckboxfield('show_empty_cat',$config->show_empty_cat);  

		$lists['show_number_documents'] = self::showCheckboxfield('show_number_documents',$config->show_number_documents);   

		$lists['number_categories'] = self::showCheckboxfield('number_categories',$config->number_categories);  
				
		$lists['number_documents'] = self::showCheckboxfield('number_documents',$config->number_documents);   

		//Sub-categories settings
		$lists['show_subcategory_icon'] = self::showCheckboxfield('show_subcategory_icon',$config->show_subcategory_icon);  

		$lists['show_subcategory_description'] = self::showCheckboxfield('show_subcategory_description',$config->show_subcategory_description);  
		
		$lists['use_download_link_instead_of_detail_link'] = self::showCheckboxfield('use_download_link_instead_of_detail_link',$config->use_download_link_instead_of_detail_link);  

		$lists['category_table_show_filesize'] = self::showCheckboxfield('category_table_show_filesize',$config->category_table_show_filesize);  

		$lists['category_table_show_filetype'] = self::showCheckboxfield('category_table_show_filetype',$config->category_table_show_filetype); 

		
		$lists['show_view_button'] = self::showCheckboxfield('show_view_button',$config->show_view_button);
		$lists['view_url'] = self::showCheckboxfield('view_url',$config->view_url);

		$lists['hide_download_button'] = self::showCheckboxfield('hide_download_button',$config->hide_download_button); 

		$lists['show_hits'] = self::showCheckboxfield('show_hits',$config->show_hits); 

		$lists['show_filename'] = self::showCheckboxfield('show_filename',$config->show_filename); 

		$lists['show_filesize'] = self::showCheckboxfield('show_filesize',$config->show_filesize); 

		$lists['show_filetype'] = self::showCheckboxfield('show_filetype',$config->show_filetype); 

		$lists['show_creation_date'] = self::showCheckboxfield('show_creation_date',$config->show_creation_date);

		$lists['show_creation_user'] = self::showCheckboxfield('show_creation_user',$config->show_creation_user);

		$lists['show_number_downloaded'] = self::showCheckboxfield('show_number_downloaded',$config->show_number_downloaded);

		$lists['show_modified_date'] = self::showCheckboxfield('show_modified_date',$config->show_modified_date); 

		$lists['show_detail_in_popup'] = self::showCheckboxfield('show_detail_in_popup',$config->show_detail_in_popup); 

		$lists['show_publish_date'] = self::showCheckboxfield('show_publish_date',$config->show_publish_date); 

		$lists['hide_details_button'] = self::showCheckboxfield('hide_details_button',$config->hide_details_button);

        $lists['show_sort_options'] = self::showCheckboxfield('show_sort_options',$config->show_sort_options);

		$lists['show_document_version'] = self::showCheckboxfield('show_document_version',$config->show_document_version);

        $lists['reset_downloadlog'] = self::showCheckboxfield('reset_downloadlog',$config->reset_downloadlog);

		$lists['use_googleviewer'] = self::showCheckboxfield('use_googleviewer',$config->use_googleviewer);

		$lists['show_tablelayoutheader'] = self::showCheckboxfield('show_tablelayoutheader',$config->show_tablelayoutheader);

        $lists['login_to_download'] = self::showCheckboxfield('login_to_download',$config->login_to_download);

		$options = array();
		$options[] = JHtml::_('select.option', 'tbl.title', JText::_('EDOCMAN_TITLE'));
		$options[] = JHtml::_('select.option', 'tbl.created_time', JText::_('JGLOBAL_CREATED_DATE'));
        $options[] = JHtml::_('select.option', 'tbl.modified_time', JText::_('JGLOBAL_FIELD_MODIFIED_LABEL'));
		$options[] = JHtml::_('select.option', 'tbl.ordering', JText::_('EDOCMAN_ORDERING'));
        $options[] = JHtml::_('select.option', 'tbl.file_size', JText::_('EDOCMAN_FILESIZE'));
		$lists['default_sort_option'] = JHtml::_('select.genericlist', $options, 'default_sort_option', 'class="chosen"', 'value', 'text', $config->default_sort_option ? $config->default_sort_option : 'tbl.ordering');

		$options = array();
		$options[] = JHtml::_('select.option', 2, JText::_('EDOCMAN_VERSION_2'));
		$options[] = JHtml::_('select.option', 3, JText::_('EDOCMAN_VERSION_3'));
		$lists['twitter_bootstrap_version'] = JHtml::_('select.genericlist', $options, 'twitter_bootstrap_version', 'class="chosen"', 'value', 'text', $config->twitter_bootstrap_version ? $config->twitter_bootstrap_version : 2);

		$options = array();
		$options[] = JHtml::_('select.option', 'asc', JText::_('EDOCMAN_ASC'));
		$options[] = JHtml::_('select.option', 'desc', JText::_('EDOCMAN_DESC'));
		$lists['default_sort_direction'] = JHtml::_('select.genericlist', $options, 'default_sort_direction', 'class="chosen"', 'value', 'text', $config->default_sort_direction);
		$lists['show_uploader_name_in_document_mamangement'] = self::showCheckboxfield('show_uploader_name_in_document_mamangement',$config->show_uploader_name_in_document_mamangement); 

        $lists['show_social_sharing_buttons'] = self::showCheckboxfield('show_social_sharing_buttons',$config->show_social_sharing_buttons); 

		//Fields configuration		
        $lists['use_simple_upload_form'] = self::showCheckboxfield('use_simple_upload_form',$config->use_simple_upload_form);

		$lists['insert_document_id'] = self::showCheckboxfield('insert_document_id',$config->insert_document_id);

        $lists['insert_category_id'] = self::showCheckboxfield('insert_category_id',$config->insert_category_id);

		$lists['use_default_license'] = self::showCheckboxfield('use_default_license',$config->use_default_license);

        $lists['search_with_sub_cats'] = self::showCheckboxfield('search_with_sub_cats',$config->search_with_sub_cats);

		$lists['turn_on_sharing'] = self::showCheckboxfield('turn_on_sharing',$config->turn_on_sharing);

		$options = array() ;
		$options[] = JHtml::_('select.option', 0, JText::_('All nested categories'));
		$options[] = JHtml::_('select.option', 1, JText::_('Only the last one'));
		$options[] = JHtml::_('select.option', 2, JText::_('No insert'));
		$lists['insert_category'] = JHtml::_('select.genericlist', $options, 'insert_category', ' class="inputbox chosen"', 'value', 'text', $config->insert_category);

		$options = array();
		$options[] = JHtml::_('select.option', 1, 1);
		$options[] = JHtml::_('select.option', 2, 2);
		$options[] = JHtml::_('select.option', 3, 3);
		$options[] = JHtml::_('select.option', 6, 6);
		$lists['number_categories_per_row'] = JHtml::_('select.genericlist', $options, 'number_categories_per_row', ' class="input-mini chosen"', 'value', 'text', isset($config->number_categories_per_row) ? $config->number_categories_per_row : 2);

		$options = array();
		$options[] = JHtml::_('select.option', 1, 1);
		$options[] = JHtml::_('select.option', 2, 2);
		$options[] = JHtml::_('select.option', 3, 3);
		$options[] = JHtml::_('select.option', 6, 6);
		$lists['number_subcategories'] = JHtml::_('select.genericlist', $options, 'number_subcategories', ' class="input-mini chosen"', 'value', 'text', isset($config->number_subcategories) ? $config->number_subcategories : 2);

		// Collect downloader information
		$lists['collect_downloader_information'] = self::showCheckboxfield('collect_downloader_information',$config->collect_downloader_information); 
		$lists['onetime_collect'] = self::showCheckboxfield('onetime_collect',$config->onetime_collect); 

		$options = array() ;
		$options[] = JHtml::_('select.option', 0, JText::_('EDOCMAN_DIRECT_DOWNLOAD'));
		$options[] = JHtml::_('select.option', 1, JText::_('EDOCMAN_SEND_DOWNLOAD_LINK'));
		$lists['download_type'] = JHtml::_('select.genericlist', $options, 'download_type', ' class="inputbox chosen"', 'value', 'text', isset($config->download_type) ? $config->download_type : 1);

		$path = JPATH_ROOT.'/edocman' ;
		$path = str_replace("\\", "/", $path) ;
		//Get tab object					
		$this->lists = $lists;
		$this->config =	$config;				
		$this->path = $path;	
		// We don't need toolbar in the modal window.
		if (version_compare(JVERSION, '3.0', 'ge')) {
			if ($this->getLayout() !== 'modal')
			{
				EdocmanHelper::addSideBarmenus('configuration');
				$this->sidebar = JHtmlSidebar::render();
			}
		}
		parent::display();
	}
}