<?php
/**
 * @version        1.7.5
 * @package        Joomla
 * @subpackage     EDocman
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2011 - 2016 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
$config = EDocmanHelper::getConfig();
EDocmanHelper::chosen('select.chosen');
if (version_compare(JVERSION, '3.0', 'ge')) {
	EdocmanHelper::addSideBarmenus('category');
	$sidebar = JHtmlSidebar::render();
}
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'cancel' || document.formvalidator.isValid(document.id('item-form'))) 
		{
			<?php echo $this->form->getField('description')->save(); ?>
			Joomla.submitform(task, document.getElementById('item-form'));
		} 
		else 
		{
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_edocman&view=category&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate" enctype="multipart/form-data">
<?php if (!empty( $sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
	<div class="row-fluid">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#general-page" data-toggle="tab"><?php echo JText::_('EDOCMAN_GENERAL');?></a></li>
			<li><a href="#permission-page" data-toggle="tab"><?php echo JText::_('Permissions');?></a></li>
			<li><a href="#publishing-details" data-toggle="tab"><?php echo JText::_('JGLOBAL_FIELDSET_PUBLISHING');?></a></li>
			<li><a href="#meta-options" data-toggle="tab"><?php echo JText::_('JGLOBAL_FIELDSET_METADATA_OPTIONS');?></a></li>					
		</ul>		
		<div class="tab-content">	
			<div class="tab-pane active" id="general-page">											
				<table width="100%" class="adminform">
					<tr>
						<td class="key">
							<?php echo $this->form->getLabel('title'); ?>
						</td>
						<td>
							<?php echo $this->form->getInput('title'); ?>
						</td>
					</tr>
					<tr>
						<td class="key">
							<?php echo $this->form->getLabel('alias'); ?>
						</td>
						<td>
							<?php echo $this->form->getInput('alias'); ?>
						</td>
					</tr>
					<tr>
						<td class="key">
							<?php echo $this->form->getLabel('parent_id'); ?>
						</td>
						<td style="padding-bottom: 10px;">
							<?php echo $this->form->getInput('parent_id'); ?>
						</td>
					</tr>
					<?php 
						if ($config->activate_herachical_folder_structure) {
						?>
							<tr>
								<td class="key">
									<?php echo $this->form->getLabel('path'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('path'); ?>
								</td>
							</tr>		                    	
						<?php                                    
						}
					?>
					<tr>
						<td class="key">
							<?php echo $this->form->getLabel('image'); ?>
						</td>
						<td>
							<?php echo $this->form->getInput('image'); ?>
							<?php
							if ($this->item->image && file_exists(JPATH_ROOT.'/media/com_edocman/category/thumbs/'.$this->item->image)) {
							?>
								<input type="checkbox" name="del_image" value="1" title="<?php echo JText::_('EDOCMAN_DEL_IMAGE_DESC'); ?>" />
								Delete <a href="<?php echo JURI::root().'media/com_edocman/category/thumbs/'.$this->item->image; ?>" target="_blank" class="modal"><strong>current category thumbnail</strong></a>?
							<?php
							}
							?>
						</td>
					</tr>
					<tr>
						<td class="key">
							<?php echo $this->form->getLabel('category_layout'); ?>
						</td>
						<td>
							<?php echo $this->form->getInput('category_layout'); ?>
						</td>
					</tr>
					<tr>
						<td class="key">
							<?php echo $this->form->getLabel('access'); ?>
						</td>
						<td>
							<?php echo $this->form->getInput('access'); ?>
						</td>
					</tr>
					<tr>
						<td class="key">
							<?php echo $this->form->getLabel('published'); ?>
						</td>
						<td>
							<?php echo $this->form->getInput('published'); ?>
						</td>
					</tr>
					<?php
						if ($config->activate_multilingual_feature) {
						?>
							<tr>
								<td class="key">
									<?php echo $this->form->getLabel('language'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('language'); ?>
								</td>
							</tr>		                  		  
						<?php    
						}        
					?>
					<tr>
						<td class="key">
							<?php echo $this->form->getLabel('user_ids'); ?>
						</td>
						<td>
							<?php echo EdocmanHelper::getUserInput($this->item->user_ids); ?>	
						</td>
					</tr>
					<tr>
						<td class="key">
							<?php echo $this->form->getLabel('notification_emails'); ?>
						</td>
						<td>
							<?php echo $this->form->getInput('notification_emails'); ?>
						</td>
					</tr>

					<?php
						if (JPluginHelper::isEnabled('edocman', 'notification'))
						{
						?>
							<tr>
								<td class="key">
									<?php echo $this->form->getLabel('notify_group_ids'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('notify_group_ids'); ?>
								</td>
							</tr>
						<?php
						}
					?>

					<tr>
						<td class="key">
							<?php echo $this->form->getLabel('id'); ?>
						</td>
						<td>
							<?php echo $this->form->getInput('id'); ?>	
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<?php echo $this->form->getLabel('description'); ?>
							<div class="clr"></div>
							<?php echo $this->form->getInput('description'); ?>
						</td>
					</tr>
				</table>																
			</div>
			<div class="tab-pane" id="permission-page">		
				<?php
					if ($this->canDo->get('core.admin'))
					{
						echo $this->form->getInput('rules');
					}	
				?>
			</div>
			<div class="tab-pane" id="publishing-details">			
				<?php echo $this->loadTemplate('options'); ?>			
			</div>
			<div class="tab-pane" id="meta-options">
				<?php echo $this->loadTemplate('metadata'); ?>	
			</div>
		</div>	
		<input type="hidden" name="task" value="" />	
		<?php echo JHtml::_('form.token'); ?>
		</div>
	</div>
</form>