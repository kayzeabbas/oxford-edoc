<?php
/**
 * @version        1.7.6
 * @package        Joomla
 * @subpackage     Edocman
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2011 - 2013 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */

// No direct access.
defined('_JEXEC') or die;

class EdocmanModelUpload extends OSModel
{
	public function __construct(array $config = array())
	{
		parent::__construct();
		$this->state->insert('category_id', 'int', 0)
			->insert('published', 'int', 0)
			->insert('access', 'int', 0);
	}

	/**
	 * Store list of uploaded documents
	 *
	 * @param array $data
	 *
	 * @return boolean
	 */
	public function store($data)
	{
		$db         = $this->getDbo();
		$query      = $db->getQuery(true);
		$categoryId = (int) $data['category_id'];
		$files         = $data['file'];
		$originalFiles = $data['original_file'];
		$titles        = $data['title'];
		$descriptions  = $data['description'];
        $filesize      = $data['filesize'];
		if (count($files))
		{
			for ($i = 0, $n = count($files); $i < $n; $i++)
			{

				$documentData                      = array();
				$documentData['access']            = $data['access'];
				$documentData['published']         = $data['published'];
				$documentData['title']             = $titles[$i];
				$documentData['original_filename'] = $originalFiles[$i];
				$documentData['filename']          = $files[$i];
				$documentData['alias']             = '';
				$documentData['short_description'] = $documentData['description'] = $descriptions[$i];
                $documentData['file_size']         = $filesize[$i];
				// Check if the document is existed or not
				$sql = 'SELECT COUNT(id) FROM #__edocman_documents WHERE filename="' . $documentData['filename'] . '"';
				$db->setQuery($sql);
				$total = (int) $db->loadResult();
				if ($total > 0)
				{
					continue;
				}
				$row        = JTable::getInstance('Document', 'EdocmanTable');
				$row->bind($documentData);
				$row->setMainCategory($categoryId);
				EDocmanHelper::prepareDocument($row, $categoryId);
				$row->check();
				$row->store();
				$query->insert('#__edocman_document_category')
					->columns('category_id, document_id, is_main_category')
					->values("$categoryId, $row->id, 1");
				$db->setQuery($query);
				$db->execute();
				$query->clear();
			}
		}

		return true;
	}
}