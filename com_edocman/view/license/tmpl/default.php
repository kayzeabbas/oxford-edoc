<?php
/**
 * @version        1.7.5
 * @package        Joomla
 * @subpackage     EDocman
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2011 - 2016 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
if (version_compare(JVERSION, '3.0', 'ge')) {
	EdocmanHelper::addSideBarmenus('license');
	$sidebar = JHtmlSidebar::render();
}
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'cancel' || document.formvalidator.isValid(document.id('license-form'))) 
		{
			Joomla.submitform(task, document.getElementById('license-form'));
		}
		else 
		{
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_edocman&view=license&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="license-form" class="form-validate">
<?php if (!empty( $sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
	<div class="row-fluid">	
		<fieldset class="adminform">
			<legend><?php echo JText::_('EDOCMAN_FIELDSET_DETAILS'); ?></legend>
			<table width="100%">
				<tr>
					<td class="key">
						<?php echo $this->form->getLabel('title'); ?>
					</td>
					<td>
						<?php echo $this->form->getInput('title'); ?>
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo $this->form->getLabel('id'); ?>
					</td>
					<td>
						<?php echo $this->form->getInput('id'); ?>
					</td>
				</tr>
				<tr>
					<td class="key">
						<?php echo $this->form->getLabel('published'); ?>
					</td>
					<td>
						<?php echo $this->form->getInput('published'); ?>
					</td>
				</tr>
    		</table>              		   		
    			<div class="clearfix"></div>
    			<?php echo $this->form->getLabel('description'); ?>
    			<div class="clearfix"></div>
    			<?php echo $this->form->getInput('description'); ?>            
		</fieldset>
	</div>
</div>
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
<div class="clr"></div>
</form>