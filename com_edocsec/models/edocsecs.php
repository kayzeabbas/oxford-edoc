<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * HelloWorldList Model
 */
class EdocsecModelEdocsecs extends JModelList
{

    /**
     * Method to build an SQL query to load the list data.
     *
     * @return	string	An SQL query
     */
    public function getItems()
    {
        // Get a storage key.
        $store = $this->getStoreId();

        // Try to load the data from internal storage.
        if (isset($this->cache[$store]))
        {
            return $this->cache[$store];
        }
        // Create a new query object.
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        // Select some fields
        $query->select('#__users.id as user_id, #__users.name, #__edocsec.edoc_cat');
        // From the users table
        $query->from('#__users');
        $query->join('LEFT', '#__edocsec ON (#__edocsec.id = #__users.id)');

        $mainframe = JFactory::getApplication();
        $option = 'com_edocsec';
        $filter_order = $mainframe->getUserStateFromRequest($option . 'category_filter_order', 'filter_order', 'name',
                'cmd');
        $filter_order_Dir = $mainframe->getUserStateFromRequest($option . 'category_filter_order_Dir',
                'filter_order_Dir', '', 'word');
        $query .= ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir;
        $db->setQuery($query);
        $items = $db->loadObjectList();

        foreach ($items as $item)
        {
            $query = $db->getQuery(true);
            $query->select('id')
                    ->from('#__edocman_categories')
                    ->where("user_ids like '" . $item->user_id . "%' or user_ids like '%," . $item->user_id . ",%' or user_ids like '%," . $item->user_id . "'");
            $db->setQuery($query);
            if ($cats = $db->loadObjectList())
            {
                if ($item->edoc_cat != null && !empty($item->edoc_cat))
                {
                    $item->edoc_cat = explode(',', $item->edoc_cat);
                    array_splice($item->edoc_cat, array_search('0', $item->edoc_cat), 1);
                }
                else
                {
                    $item->edoc_cat = array();
                }
                foreach ($cats as $cat)
                {
                    if (!in_array($cat->id, $item->edoc_cat))
                    {
                        array_push($item->edoc_cat, $cat->id);
                    }
                }
                $item->edoc_cat = implode(',', $item->edoc_cat);
            }
        }

        // Check for a database error.
        if ($this->_db->getErrorNum())
        {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        // Add the items to the internal cache.
        $this->cache[$store] = $items;

        return $this->cache[$store];
    }

}
