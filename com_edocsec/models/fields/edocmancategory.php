<?php

/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * Supports an HTML select list of categories
 *
 * @package     Joomla.Platform
 * @subpackage  Form
 * @since       11.1
 */
class JFormFieldEdocmanCategory extends JFormField
{

    /**
     * @var    string	The form field type.
     * @since  11.1
     */
    public $type = 'Category';

    /**
     * Method to get the field options.
     *
     * @return  array    The field option objects.
     * @since   11.1
     */
    protected function getInput()
    {
        // Initialise variables.
        $options = array();
        $published = (string) $this->element['published'];

        $db = & JFactory::getDbo();
        $sql = 'SELECT id, title, parent_id FROM #__edocman_categories WHERE published = 1';
        $db->setQuery($sql);
        $rows = $db->loadObjectList();

        $children = array();
        // first pass - collect children
        if (count($rows))
        {
            foreach ($rows as $v)
            {
                $pt = $v->parent_id;
                $list = @$children[$pt] ? $children[$pt] : array();
                array_push($list, $v);
                $children[$pt] = $list;
            }
        }
        $list = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999);
        $options = array();
        // $options[] = JHtml::_('select.option', 0, JText::_('Select Category'));
        if (count($list))
        {
            foreach ($list as $row)
            {
                $options[] = JHtml::_('select.option', $row->id, $row->treename);
            }
        }

        if ($this->element['value'])
        {
            $selectedValue = $this->element['value'];
        }
        else
        {
            $selectedValue = $this->value;
        }
        if ($this->element['readonly'])
        {
            $disabled = ' disabled="true" ';
        }
        else
        {
            $disabled = '';
        }

        if ($disabled)
        {
            return JHtml::_('select.genericlist', $options, $this->name,
                            array(
                        'option.text.toHtml' => false,
                        'list.attr'          => 'class="inputbox"  ' . $disabled,
                        'option.text'        => 'text',
                        'option.key'         => 'value',
                        'list.select'        => $selectedValue
                    )) . '<input type="hidden" name="' . $this->name . '" value="' . $selectedValue . '" />';
        }
        else
        {
            // return JHtml::_('select.genericlist', $options, $this->name, array(
            // 'option.text.toHtml' => false ,
            // 'list.attr' => 'class="inputbox" multiple="multiple" size="15"',
            // 'option.text' => 'text' ,
            // 'option.key' => 'value',
            // 'list.select' => $selectedValue
            // )) ;
            $html = '';
            foreach ($options as $option)
            {
                if (in_array($option->value, $selectedValue))
                {
                    $html.='<label class="catlabel" for="cat' . $option->value . '"><input class="catbox" type="checkbox" id="cat' . $option->value . '" value="' . $option->value . '" name="' . $this->name . '" checked="checked">' . $option->text . '</label>';
                }
                else
                {
                    $html.='<label class="catlabel" for="cat' . $option->value . '"><input class="catbox" type="checkbox" id="cat' . $option->value . '" value="' . $option->value . '" name="' . $this->name . '">' . $option->text . '</label>';
                }
            }
            return $html;
        }
    }

}
