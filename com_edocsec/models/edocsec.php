<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

/**
 * Edocsec Model
 */
class EdocsecModelEdocsec extends JModelAdmin
{

    /**
     * Returns a reference to the a Table object, always creating it.
     *
     * @param	type	The table type to instantiate
     * @param	string	A prefix for the table class name. Optional.
     * @param	array	Configuration array for model. Optional.
     * @return	JTable	A database object
     * @since	2.5
     */
    public function getTable($type = 'Edocsec', $prefix = 'EdocsecTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getItem($pk = null)
    {
        $id = (int) $this->getState($this->getName() . '.id');
        $item = new stdClass();
        $item->id = $id;
        $table = $this->getTable();
        $db = $table->getDBO();
        $query = $db->getQuery(true);
        $query->select('name')->from('#__users')->where('id = ' . $db->quote($id));
        $db->setQuery($query);
        $item->name = $db->loadResult();
        //check exists and make initial record, bad form, do not copy
        if (!$table->get_edoc_cat($id))
        {
            $item->edoc_cat = "";
            $db->insertObject("#__edocsec", $item);
        }
        //get all categories
        if ($cat = $table->get_cat($id))
        {
            $item->edoc_cat = $cat;
        }
        return $item;
    }

    /**
     * Method to get the record form.
     *
     * @param	array	$data		Data for the form.
     * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
     * @return	mixed	A JForm object on success, false on failure
     * @since	2.5
     */
    public function getForm($data = array(), $loadData = true)
    {
        // Get the form.
        $form = $this->loadForm('com_edocsec.edocsec', 'edocsec', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form))
        {
            return false;
        }
        if (empty($data))
            $data = $this->loadFormData();

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return	mixed	The data for the form.
     * @since	2.5
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_edocsec.edit.edocsec.data', array());
        if (empty($data))
        {
            $data = $this->getItem();
        }
        return $data;
    }

    public function save($data)
    {
        $application = JFactory::getApplication();
        $table = $this->getTable();

        $table->id = (int) $data['id'];
        $table->edoc_cat = $data['edoc_cat'];

        if (!$table->check())
        {
            $this->setError($table->getError());
            return false;
        }

        if (!$table->store())
        {
            $this->setError($table->getError());
            return false;
        }

        $this->setState($this->getName() . '.id', $table->id);

        $this->cleanCache();

        return true;
    }

}
