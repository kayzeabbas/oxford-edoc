<?php

// No direct access
defined('_JEXEC') or die;

// import Joomla table library
jimport('joomla.database.table');

class EdocsecTableEdocsec extends JTable
{

    /**
     * Constructor
     *
     * @param JDatabase A database connector object
     */
    public function __construct(&$db)
    {
        parent::__construct('#__edocsec', 'id', $db);
    }

    public function get_edoc_cat($id)
    {
        $db = $this->getDBO();

        $result = array();
        $query = $db->getQuery(true);
        $query->select('edoc_cat')
                ->from('#__edocsec')
                ->where('id = ' . (int) $id);
        $db->setQuery($query);
        if ($results = $db->loadResult())
        {
            $result = explode(',', $results);
        }

        return $result;
    }

    public function get_cat($id)
    {
        $db = $this->getDBO();
        $result = array();

        $result = $this->get_edoc_cat($id);

        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__edocman_categories')
                ->where("user_ids like '" . (int) $id . "%' or user_ids like '%," . (int) $id . ",%' or user_ids like '%," . (int) $id . "'");
        $db->setQuery($query);
        if ($results = $db->loadObjectList())
        {
            foreach ($results as $row)
            {
                if (!in_array($row->id, $result))
                {
                    array_push($result, $row->id);
                }
            }
        }
        return $result;
    }

    public function store($updateNulls = false)
    {
        $db = $this->getDBO();
        $this->id = (int) $this->id;

        JArrayHelper::toInteger($this->edoc_cat);
        // remove user from all categories
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__edocman_categories')
                ->where("user_ids like '" . $db->escape((int) $this->id, true) . "%' or user_ids like '%," . $db->escape((int) $this->id,
                                true) . ",%' or user_ids like '%," . $db->escape((int) $this->id, true) . "'");
        $db->setQuery($query);
        if ($results = $db->loadObjectList())
        {
            foreach ($results as $r)
            {
                $ids = explode(',', $r->user_ids);
                array_splice($ids, array_search($this->id, $ids), 1);
                $r->user_ids = implode(',', $ids);
                $db->updateObject('#__edocman_categories', $r, 'id');
            }
        }
        // add user to specified categories
        foreach ($this->edoc_cat as $cat)
        {
            $query = $db->getQuery(true);
            $query->select('*')
                    ->from('#__edocman_categories')
                    ->where('id = ' . (int) $cat);
            $db->setQuery($query);
            if ($result = $db->loadObject())
            {
                $uids = explode(',', $result->user_ids);
                if (!in_array($this->id, $uids))
                {
                    array_push($uids, $this->id);
                    $result->user_ids = implode(',', $uids);
                    $db->updateObject('#__edocman_categories', $result, 'id');
                }
            }
        }
        $this->edoc_cat = implode(',', $this->edoc_cat);


        $db->updateObject('#__edocsec', $this, 'id');
        return true;
    }

}
