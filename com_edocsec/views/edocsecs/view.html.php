<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

class EdocsecViewEdocsecs extends JViewLegacy
{

    function display($tpl = null)
    {
        // Get data from the model
        $items = $this->get('Items');

        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            JError::raiseError(500, implode('<br />', $errors));
            return false;
        }
        // Assign data to the view
        $this->items = $items;

        $mainframe = JFactory::getApplication('admin');
        $option = 'com_edocsec';
        $filter_order = $mainframe->getUserStateFromRequest($option . 'category_filter_order', 'filter_order', 'name',
                'cmd');
        $filter_order_Dir = $mainframe->getUserStateFromRequest($option . 'category_filter_order_Dir',
                'filter_order_Dir', '', 'word');

        $lists['order_Dir'] = $filter_order_Dir;
        $lists['order'] = $filter_order;
        $this->lists = $lists;

        $this->addToolBar();

        // Display the template
        parent::display($tpl);
    }

    protected function addToolBar()
    {
        JToolBarHelper::title('Jobsite Permissions');
        JToolBarHelper::editList('edocsec.edit');
    }

}
