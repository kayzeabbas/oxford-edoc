<?php
/**
 * @version		1.5.2
 * @package        Joomla
 * @subpackage     EDocman
 * @author         Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */ 
defined('_JEXEC') or die ; 

/**
 * This field was written base on the category layout of docman extension 
 * @category	EDOCman
 * @package		EDOCman 1.7.5
 * @copyright	Copyright (C) 2008 - 2016 Osolution team. All rights reserved.
 * @license	    This file can not be redistributed without the written consent of the 
 				original copyright holder. This file is not licensed under the GPL. 
 * @link     	http://www.joomdonation.com
 */
?>
<?php	
	if ($this->config->show_detail_in_popup) {
		JHTML::_('behavior.modal');
		$popup = 'class="modal" rel="{handler: \'iframe\', size: {x: 800, y: 500}}"';	
	} else {
		$popup = '';
	}
	if (isset($this->config->number_columns)) {
		$numberColumns = $this->config->number_columns ;
	} else {
		$numberColumns = 2 ;
	}	
	$user = JFactory::getUser() ;
	$userId = $user->get('id');
?>
<script type="text/javascript">
	var cookieVar = 'columns_layout';
	var defaultCookieLayout = 'grid';
</script>
<form method="post" name="adminForm" id="adminForm" action="index.php">	
	<h1 class="dm_title"><?php echo JText::_('EDOCMAN_SEARCH_RESULT'); ?></h1>	
	<!-- Documents List -->
	<?php if(count($this->items)) { ?>
	    <div id="dm_docs">	    
    	<?php    		
	        for ($i = 0 , $n = count($this->items) ;  $i < $n ; $i++) {
	            $item = $this->items[$i] ;
	            $canDownload = $user->authorise('edocman.download', 'com_edocman.document.'.$item->id) ;
	            $canEdit	= $user->authorise('core.edit',			'com_edocman.document.'.$item->id);	            	            
	            $canEditOwn	= $user->authorise('core.edit.own',		'com_edocman.document.'.$item->id) && $item->created_user_id == $userId;	            
	            $canDownload = $canDownload || $canEdit || ($item->created_user_id == $userId) ;
	        	if ($item->image)
	        		$imgSrc = JUri::base().'media/com_edocman/document/thumbs/'.$item->image ;
	        	else {
	        		if (!isset($this->config->show_default_document_thumbnail) || $this->config->show_default_document_thumbnail) {
	        			$ext = JString::strtolower(JFile::getExt($item->filename)) ;
	        			if (JFile::exists(JPATH_ROOT.'/components/com_edocman/assets/images/icons/32x32/'.$ext.'.png')) {
	        				$imgSrc = JUri::base().'components/com_edocman/assets/images/icons/32x32/'.$ext.'.png' ;
	        			} else {
	        				$imgSrc = JUri::base().'components/com_edocman/assets/images/icons/32x32/zip.png';
	        			}	
	        		} else {
	        			$imgSrc = '';
	        		}	        		
	        	}	
	        	if ($this->config->show_detail_in_popup) {
	        		$url = JRoute::_('index.php?option=com_edocman&view=document&id='.$item->id.'&tmpl=component&Itemid='.$this->Itemid);
	        	} else {	        		
	        		$url = JRoute::_('index.php?option=com_edocman&view=document&id='.$item->id.'&Itemid='.$this->Itemid);
	        	}	  	       
	        	$downloadUrl = JRoute::_('index.php?option=com_edocman&task=document.download&id='.$item->id.'&Itemid='.$this->Itemid) ;
	        	$width = (int)(100/$numberColumns) - 6 ;
	        	if ($i % $numberColumns == 0) {
	        		echo '<div class="col_container" style="float:left;width : 100%;">';
	        	}      		        	
	        ?>
	        	<div class="dm_row" style="width : <?php echo $width; ?>%; float: left; border : 1px solid #E5E5E5; margin: 1px;">
					<h3 class="dm_title">
						<?php 
							if ($imgSrc) {
							?>
								<img src="<?php echo $imgSrc; ?>" alt="<?php echo $item->title; ?>" />
							<?php	
							}
							if ($this->config->use_download_link_instead_of_detail_link && $canDownload) {
							?>
								<a href="<?php echo $downloadUrl; ?>" title="<?php echo JText::_('EDOCMAN_DOWNLOAD') ; ?>">
		    						<?php echo $item->title; ?>
		    					</a>
							<?php	
							} else {
							?>
								<a href="<?php echo $url; ?>" title="<?php echo $item->title; ?>">
    								<?php echo $item->title; ?>
    							</a>
							<?php	
							}
						?>																																
						<div class="clr"></div>
					</h3>				
				<?php
				
				//output document description
				if ($item->short_description) :
					?>
					<div class="dm_description">
						<?php echo $item->short_description ; ?>
					</div>
					<?php
				else :
					?>
						<div class="dm_description">
							<?php echo $item->description ; ?>
						</div>
					<?php	
				endif;
				?>
				<div class="clr"></div>								
				<div class="dm_taskbar">
				    <ul>					    	
						<?php
						    if ($canDownload && $this->config->hide_download_button !== '1') {
						    ?>
						    	<li>
        							<a href="<?php echo $downloadUrl; ?>">
        								<?php echo JText::_('EDOCMAN_DOWNLOAD'); ?>
        							</a>
        						</li>
						    <?php
						    }      
					        if ($canDownload && $this->config->show_view_button && $item->canView) {
					            $viewUrl = JRoute::_('index.php?option=com_edocman&task=document.viewdoc&id='.$item->id.'&Itemid='.$this->Itemid) ;
					        ?>
						        <li>
        							<a href="<?php echo $viewUrl; ?>" target="_blank">
        								<?php echo JText::_('EDOCMAN_VIEW'); ?>
        							</a>
        						</li>
						        <?php    
					        }      						    						    						    						   
						?>													    							
						<li>
							<a href="<?php echo $url; ?>" <?php echo $popup; ?>>
								<?php echo JText::_('EDOCMAN_DETAILS'); ?>
							</a>
						</li>				 					
				    </ul>
				    <div class="clr"></div>
				</div>				
				</div>
	        <?php	
	        	if (($i + 1) % $numberColumns == 0)
	        		echo "</div>" ;
	        }
	        if ($i % $numberColumns != 0)
	        	echo "</div>" ;
	    ?>	    
	    </div>	    
    	<?php
    		if ($this->pagination->total > $this->pagination->limit) {
    		?>
    			<div align="center" class="pagination">
    				<?php echo $this->pagination->getListFooter(); ?>
    			</div>
    		<?php	
    		}
    	?>	    		   
	<?php } else { ?>
	    <br />
	    <div id="dm_docs">
	        <i><?php echo JText::_('EDOCMAN_NO_DOCS_FOUND'); ?></i>
	    </div>
	<?php } ?>
	<input type="hidden" name="id" value="<?php echo $this->category->id; ?>" />
	<input type="hidden" name="view" value="category" />	
	<input type="hidden" name="Itemid" value="<?php echo $this->Itemid ; ?>" />
	<input type="hidden" name="option" value="com_edocman" />		
	<input type="hidden" name="direction" value="<?php echo $this->direction; ?>" />
	
	<script language="javascript">
		function changeDirection(newDirection) {
			var form = document.adminForm ;
			form.direction.value = newDirection ;
			form.submit();
		}	
	</script>
		
</form>