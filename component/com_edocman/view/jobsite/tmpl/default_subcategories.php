<?php
	if (count($this->categories)) {			
			if ($this->category)
				$text = JText::_('EDOCMAN_SUB_CATEGORIES');
			else
				$text = JText::_('EDOCMAN_CATEGORIES') ;
			?>
			<div id="dm_cats">
			    <!--<h2 class="dm_title"><?php echo $text ;?></h2>		-->		    
			    <?php		
			    	$numberColumns = $this->config->number_subcategories ? $this->config->number_subcategories : 3 ;	  


			    ?>
			    
			    <?php

					if ($this->category->title == 'Jobsite Invoices'){
						?>
						<table id="jobsites" width="100%">
							<col style="width:10%">
	                        <col style="width:45%">
	                        <col style="width:45%">
					    	<thead>
					    		<tr>
					    			<th>JOB SITE #</th>
					    			<th>JOB LOCATION</th>
					    			<th>JOB NAME</th>

					    		</tr>
					    	</thead>
					    	<tbody>
					 <?php
						for ($i = 0 , $n = count($this->categories) ; $i < $n ; $i++) {
				    		$item = $this->categories[$i] ;	

				    		if (!$this->config->show_empty_cat && $item->total_documents == 0)
				    			continue ;

				    		if ($item->category_layout == 'default')		    		
				    			$link = 'component/edocman/?view=category&id='.$item->id.'&Itemid='.$this->Itemid;
				    		else
				    			$link = 'component/edocman/?view=category&layout='.$item->category_layout.'&id='.$item->id.'&Itemid='.$this->Itemid;



					    	?>
					    	<tr>
                                <td>
                                    <a href="<?php echo $link; ?>">
                                        <?php echo $item->job_id;?>
                                    </a>
                                </td>

                                <td>
                                    <a href="<?php echo $link; ?>">
                                        <?php echo $item->job_location; ?>
                                    </a>
                                </td>
					    		<td>
					    			<a href="<?php echo $link; ?>">
                                        <?php echo $item->title; ?>
									</a>
								</td>
					    	</tr>
					    		
					    	<?php

					    	if (($i + 1) % $numberColumns == 0)
					    		echo "</div>" ;
					    }
					    	
		    	?>

		    		</tbody>
	    		</table>

		    	<?php

			    	} else {

			    		echo '<div class="dm_row dm_container">';

				    	for ($i = 0 , $n = count($this->categories) ; $i < $n ; $i++) {
				    		$item = $this->categories[$i] ;	
				    		if (!$this->config->show_empty_cat && $item->total_documents == 0)
				    			continue ;
				    		if ($item->category_layout == 'default')		    		
				    			$link = 'component/edocman/?view=category&id='.$item->id.'&Itemid='.$this->Itemid;
				    		else
				    			$link = 'component/edocman/?view=category&layout='.$item->category_layout.'&id='.$item->id.'&Itemid='.$this->Itemid;

				    		
				    		if ($item->image) {
				    			$imgUrl = JURI::base().'media/com_edocman/category/thumbs/'.$item->image ;
				    		} else {
				    			$imgUrl = JURI::base().'components/com_edocman/assets/images/icons/32x32/folder.png' ;
				    		}
				    		// if ($i % $numberColumns == 0) {
				    		// 	echo '<div class="dm_row dm_container" style="width : 100%;">';
				    		// }	    		
				    	?>
				    		<div class="dm_row_category" style="margin: 20px;">
                                <h3 class="dm_title subcategory_link">
                                <?php
									if (!isset($this->config->show_subcategory_icon) || $this->config->show_subcategory_icon) {
									?>
										<a href="<?php echo $link; ?>">
											<img src="<?php echo $imgUrl; ?>" alt="<?php echo $item->title; ?>" />
										</a>
									<?php	
									}
				    			?>				    																												

									<a href="<?php echo $link; ?>">
																		 
											<?php 
												echo $item->title; 											
												if ($this->config->show_number_documents) {
												?>
													<!--<small>( <?php echo $item->total_documents ;?> <?php  echo $item->total_documents > 1 ? JText::_('EDOCMAN_DOCUMENTS') : JText::_('EDOCMAN_DOCUMENT') ; ?> )</small>-->	
												<?php	
												}
											?>	
																																																			
									</a>
								</h3>
							    <?php
							    if($item->description && (!isset($this->config->show_subcategory_description) || $this->config->show_subcategory_description)) :
							        ?><div class="dm_description"><?php echo $item->description;?></div><?php
							    endif;

								if($item->job_id){
								?>
									<p>Jobsite ID: <?php echo $item->job_id;?></p>
								<?php
								}
							    ?>
							    <div class="clr"></div>							
							</div>	
				    	<?php

				    	// if (($i + 1) % $numberColumns == 0)
				    	// 	echo "</div>" ;
				    	
				    	}

			    	echo '</div>';
			    	}

			    	// if ($i % $numberColumns != 0)
			    	// 	echo "</div>" ;

			    	echo '</div>';
			    ?>		
	<?php	    
	}
?>
<div class="clr"></div>