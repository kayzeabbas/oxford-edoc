<?php
/**
* @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */ 
defined('_JEXEC') or die ; 

/**
 * This field was written base on the category layout of docman extension 
 * @category	DOCman
 * @package		DOCman15
 * @copyright	Copyright (C) 2003 - 2009 Johan Janssens and Mathias Verraes. All rights reserved.
 * @license	    This file can not be redistributed without the written consent of the 
 				original copyright holder. This file is not licensed under the GPL. 
 * @link     	http://www.joomladocman.org
 */
?>
<?php	
	if ($this->config->show_detail_in_popup) {
		JHTML::_('behavior.modal');
		$popup = 'class="modal" rel="{handler: \'iframe\', size: {x: 800, y: 500}}"';	
	} else {
		$popup = '';
	}
	if (isset($this->config->number_columns)) {
		$numberColumns = $this->config->number_columns ;
	} else {
		$numberColumns = 2 ;
	}	
	$user = & JFactory::getUser() ;
	$userId = $user->get('id');
?>
<form method="post" name="adminForm" id="adminForm" action="index.php">	
	<div class="dm_cat" style="margin-left: 20px; margin-top: 20px;">
		<?php
			if ($this->category->image) {
				$imgUrl = JURI::base().'media/com_edocman/thumbs/'.$this->category->image ;
			} else {
				if (!isset($this->config->show_default_category_thumbnail) || $this->config->show_default_category_thumbnail)
					$imgUrl = JURI::base().'components/com_edocman/assets/images/icons/32x32/folder.png' ;
				else
					$imgUrl = '' ;							
			}
			if ($imgUrl) {
			?>
				<img class="dm_thumb-left" src="<?php echo $imgUrl; ?>" alt="<?php echo $this->category->title; ?>" />		
			<?php	
			}										       		    		     
			if($this->category->title != '') :
		        ?><h1 class="dm_title" style="display:inline;"><?php echo $this->category->title;?></h1>
		        <?php
		    endif;	
		    if ($user->authorise('core.create', 'com_edocman.category.'.$this->category->id)) {
		    ?>
        		<!--<span style="float: right; margin:20px;"><a href="index.php?option=com_edocman&task=document.add&category_id=<?php echo $this->category->id; ?>&Itemid=<?php echo $this->Itemid; ?>" class="edocman_upload_link"><?php echo JText::_('EDOCMAN_UPLOAD'); ?></a></span>
        	      -->
        	<?php
        	}		
			if($this->category->description != '') :
				?><div class="dm_description"><?php echo $this->category->description;?></div><?php
			endif;
		?>
		<div class="clr"></div>
	</div>
	
	<!-- Sub categories -->	
	<?php echo $this->loadTemplate('subcategories'); ?>
	
	<!-- Documents List -->
	<?php if(count($this->items)) { ?>
	    <div id="dm_docs" style="margin-left: 20px; margin-top: 20px;">
    	<?php
    		if ($this->config->show_sort_options) {
    		?>
    			<table class="table_header" width="100%">
		    		<tr>
		    			<td class="title_column">
		    				<h2 class="dm_title"><?php echo JText::_('EDOCMAN_DOCUMENTS'); ?></h2>	
		    			</td>
		    			<td style="text-align: right;" class="sort_column">
		    				<span class="sort_by_title"><?php echo JText::_('EDOCMAN_SORT_BY'); ?></span>
				    		<span class="sort_by_option">
				    			<?php echo $this->lists['ordering'] ; ?>
				    		</span>
				    		<span class="sort_by_dir">
				    			<?php
				    				if ($this->direction == 'asc') {
				    					$newDirection = 'desc' ;
				    					$directionIcon = 'i_asc_arrow.gif' ;
				    				} else {
				    					$newDirection = 'asc' ;
				    					$directionIcon = 'i_desc_arrow.gif' ;
				    				}
				    			?>
				    			<a href="javascript:changeDirection('<?php echo $newDirection; ?>');"><img src="<?php echo JURI::base(true).'/components/com_edocman/assets/images/'.$directionIcon;?>" /></a>
				    		</span>
		    			</td>
		    		</tr>
		    	</table>	
    		<?php	
    		} else {
    		?>
    			<h2 class="dm_title" ><?php echo JText::_('EDOCMAN_DOCUMENTS'); ?></h2>
    		<?php	
    		}	 
	        for ($i = 0 , $n = count($this->items) ;  $i < $n ; $i++) {
	            $item = $this->items[$i] ;
	            $canDownload = $user->authorise('edocman.download', 'com_edocman.document.'.$item->id) ;
	            $canEdit	= $user->authorise('core.edit',			'com_edocman.document.'.$item->id);
	            $canDelete	= $user->authorise('core.delete',			'com_edocman.document.'.$item->id);
	            $canCheckin	= $user->authorise('core.admin', 'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
	            $canEditOwn	= $user->authorise('core.edit.own',		'com_edocman.document.'.$item->id) && $item->created_user_id == $userId;
	            $canChange	= $user->authorise('core.edit.state',	'com_edocman.document.'.$item->id) && $canCheckin;
	            $canDownload = $canDownload || $canEdit || ($item->created_user_id == $userId) ;
	        	if ($item->image)
	        		$imgSrc = JURI::base().'media/com_edocman/document/thumbs/'.$item->image ;
	        	else {
	        		if (!isset($this->config->show_default_document_thumbnail) || $this->config->show_default_document_thumbnail) {
	        			$ext = JString::strtolower(JFile::getExt($item->filename)) ;
	        			if (JFile::exists(JPATH_ROOT.DS.'components/com_edocman/assets/images/icons/32x32/'.$ext.'.png')) {
	        				$imgSrc = JURI::base().'components/com_edocman/assets/images/icons/32x32/'.$ext.'.png' ;
	        			} else {
	        				$imgSrc = JURI::base().'components/com_edocman/assets/images/icons/32x32/zip.png';
	        			}	
	        		} else {
	        			$imgSrc = '';
	        		}	        		
	        	}	
	        	if ($this->config->show_detail_in_popup) {
	        		$url = JRoute::_('index.php?option=com_edocman&view=document&id='.$item->id.'&tmpl=component&Itemid='.$this->Itemid);
	        	} else {	        		
	        		$url = JRoute::_('index.php?option=com_edocman&view=document&id='.$item->id.'&Itemid='.$this->Itemid);
	        	}	  	       
	        	$downloadUrl = JRoute::_('index.php?option=com_edocman&task=document.download&id='.$item->id.'&Itemid='.$this->Itemid) ;
	        	$width = (int)(100/$numberColumns) - 6 ;
	        	if ($i % $numberColumns == 0) {
	        		echo '<div class="col_container" style="float:left;>';
	        	}      		        	
	        ?>
	        	<div class="dm_row" style="width : <?php echo $width; ?>%; float: left; border : 1px solid #E5E5E5; margin: 1px;">
					<h3 class="dm_title">
						<?php 
							if ($imgSrc) {
							?>
								<img src="<?php echo $imgSrc; ?>" alt="<?php echo $item->title; ?>" />
							<?php	
							}
							if ($this->config->use_download_link_instead_of_detail_link && $canDownload) {
							?>
								<a href="<?php echo $downloadUrl; ?>" title="<?php echo JText::_('EDOCMAN_DOWNLOAD') ; ?>">
		    						<?php echo $item->title; ?>
		    					</a>
							<?php	
							} else {
							?>
								<a href="<?php echo $url; ?>" title="<?php echo $item->title; ?>">
    								<?php echo $item->title; ?>
    							</a>
							<?php	
							}
						?>																																
						<div class="clr"></div>
					</h3>				
				<?php
				
				//output document description
				if ($item->short_description) :
					?>
					<div class="dm_description" style="margin-left: 20px; margin-top: 20px;">
						<?php echo $item->short_description ; ?>
					</div>
					<?php
				else :
					?>
						<div class="dm_description" style="margin-left: 20px; margin-top: 20px;">
							<?php echo $item->description ; ?>
						</div>
					<?php	
				endif;
				?>
				<div class="clr"></div>								
				<div class="dm_taskbar">
				    <ul>
						<?php
						    if ($canDownload) {
						    ?>
						    	<li>
        							<a href="<?php echo $downloadUrl; ?>">
        								<?php echo JText::_('EDOCMAN_DOWNLOAD'); ?>
        							</a>
        						</li>
						    <?php      
						        if ($this->config->show_view_button && $item->canView) {
						            $viewUrl = JRoute::_('index.php?option=com_edocman&task=document.viewdoc&id='.$item->id.'&Itemid='.$this->Itemid) ;
						        ?>
						        <li>
        							<a href="<?php echo $viewUrl; ?>" target="_blank">
        								<?php echo JText::_('EDOCMAN_VIEW'); ?>
        							</a>
        						</li>
						        <?php    
						        }      						    
						    }						    						    
						?>													    							
						<li>
							<a href="<?php echo $url; ?>" <?php echo $popup; ?>>
								<?php echo JText::_('EDOCMAN_DETAILS'); ?>
							</a>
						</li>				 					
				    </ul>
				    <div class="clr"></div>
				</div>				
				</div>
	        <?php	
	        	if (($i + 1) % $numberColumns == 0)
	        		echo "</div>" ;
	        }
	        if ($i % $numberColumns != 0)
	        	echo "</div>" ;
	    ?>	    
	    </div>	    
    	<?php
    		if ($this->pagination->total > $this->pagination->limit) {
    		?>
    			<div align="center" class="pagination">
    				<?php echo $this->pagination->getListFooter(); ?>
    			</div>
    		<?php	
    		}
    	?>	    		   
	<?php } else { ?>
	    <br />
	    <div id="dm_docs" style="margin-left: 20px;">
	        <i><?php echo JText::_('EDOCMAN_NO_DOCS'); ?></i>
	    </div>
    <?php } ?>
	<input type="hidden" name="id" value="<?php echo $this->category->id; ?>" />
	<input type="hidden" name="view" value="category" />	
	<input type="hidden" name="Itemid" value="<?php echo $this->Itemid ; ?>" />
	<input type="hidden" name="option" value="com_edocman" />		
	<input type="hidden" name="direction" value="<?php echo $this->direction; ?>" />

	<script language="javascript">
		function changeDirection(newDirection) {
			var form = document.adminForm ;
			form.direction.value = newDirection ;
			form.submit();
		}	
	</script>

    <div style="text-align:right; width: 950px; margin-top: 20px;">
        <?php if (!isset($this->jobsiteID))
        {
            ?>
            <a class='btn-cancel' href="<?php echo JRoute::_('index.php?option=com_edocman&view=jobsites'); ?>">Back</a>
            <?php
        }
        else
        {
            ?>
            <a class='btn-cancel' href="index.php/memberarea">Back</a>
        <?php } ?>
    </div>
</form>