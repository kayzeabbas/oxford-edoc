<?php 
/**
* @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die ; 

/**
 * This field was written base on the category layout of docman extension 
 * @category	DOCman
 * @package		DOCman15
 * @copyright	Copyright (C) 2003 - 2009 Johan Janssens and Mathias Verraes. All rights reserved.
 * @license	    This file can not be redistributed without the written consent of the 
 				original copyright holder. This file is not licensed under the GPL. 
 * @link     	http://www.joomladocman.org
 */
?>
<!--div class = "edocman_search">
<?php  
/*	jimport( 'joomla.application.module.helper' );
	$module = JModuleHelper::getModule( 'mod_edocman_search' );
	echo JModuleHelper::renderModule( $module );*/
?>
</div-->

<?php	
	if ($this->config->show_detail_in_popup) {
		JHTML::_('behavior.modal');
		$popup = 'class="modal" rel="{handler: \'iframe\', size: {x: 800, y: 500}}"';	
	} else {
		$popup = '';
	}
	$user = & JFactory::getUser() ;	
	$userId = $user->id;

	 $see = $this->category;
?>
<form method="post" name="edocman_form" id="edocman_form" action="<?php echo 'component/edocman/?view=category&id='.$this->category->id.'&Itemid='.$this->Itemid; ?>">
	<?php
		if ($this->category) {
		?>
			<div class="dm_cat" >
				<?php
					if ($this->category->image) {
						$imgUrl = JURI::base().'media/com_edocman/category/thumbs/'.$this->category->image ;
					} else {
						if (!isset($this->config->show_default_category_thumbnail) || $this->config->show_default_category_thumbnail) 
							$imgUrl = JURI::base().'components/com_edocman/assets/images/icons/32x32/folder.png' ;
						else
							$imgUrl = '' ;
					}
					if ($imgUrl) {
					?>
						<img class="dm_thumb-left" src="<?php echo $imgUrl; ?>" alt="<?php echo $this->category->image; ?>" />	
					<?php	
					}									       		    		     
					if($this->category->title != '') :
				    ?>
				    	<h1 class="dm_title" style="display: inline;"><?php echo $this->category->title;?></h1>
				    <?php
				    endif;

				    
				    //Custom Code: Add job ID search
				    
				    if($this->category->title == 'Jobsite Invoices'){
				    	?><br>
				    	<div class = "jobIDsearch">
					    	<form action="" method="POST">
					    		<input type="text" name="input_value" id="jobID" placeholder="Job Site Number">
					    		<input class="btn-default" type="submit" name="submit" value="Search">
				    	</div>
				    	<?php
				    }
			    	if (isset($_POST['submit']))
			    	{
			    		$db = JFactory::getDBO();
			    		$jobID = $_POST['input_value'];
			    		$query = $db->getQuery(true);
						$query
						 ->select($db->quoteName('id'))
						 ->from($db->quoteName('qyrw7_edocman_categories'))
						 ->where('job_id = '.$jobID);
						$db->setQuery($query);
			    		$urlID = $db->loadResult();
			    		if ($urlID==null) $urlID = 19;
							// Execute this code if the submit button is pressed.
			    		header("Location: ?view=category&id=$urlID&Itemid=0");
			    		exit;

			    	}
					//Custom Code: Add job ID search END


				    if (($user->authorise('core.create', 'com_edocman.category.'.$this->category->id)) && count($this->items)) {
				    ?>
    		            <!--<span style="float: right;"><a href="index.php?option=com_edocman&task=document.add&category_id=<?php echo $this->category->id; ?>&Itemid=<?php echo $this->Itemid; ?>" class="edocman_upload_link"><?php echo JText::_('EDOCMAN_UPLOAD'); ?></a></span>-->
    		            <!-- <span style="float: right;"><a href="index.php?option=com_edocman&task=document.add&category_id=<?php echo $this->category->id; ?>&Itemid=<?php echo $this->Itemid; ?>" class="edocman_upload_link">Upload Documents</a></span>-->
    		        <?php                        
    		        }				    				        
					if($this->category->description != '') :
						?><div class="dm_description"><?php echo $this->category->description;?></div><?php
					endif;
					if($this->category->job_id){
					?>
						<!-- Remove Job Site ID on the document repository -->
						<!-- <p>JobsiteID: <?php echo $this->category->job_id;?></p> -->
					<?php
					}
				?>
			</div>		
		<?php	
		}
	?>
	<!-- Sub categories -->	
	<?php echo $this->loadTemplate('subcategories'); ?>	
	<!-- Documents List -->
	<?php if(count($this->items)) { ?>
	    <div id="dm_docs">
    	<?php
    		if ($this->config->show_sort_options) {
    		?>
    			<table class="table_header" width="100%">
		    		<tr>
		    			<td class="title_column">
		    				<!--<h2 class="dm_title"><?php echo JText::_('EDOCMAN_DOCUMENTS'); ?></h2>	-->
		    			</td>
		    			<td style="text-align: right;" class="sort_column">
		    				<span class="sort_by_title"><?php echo JText::_('EDOCMAN_SORT_BY'); ?></span>
				    		<span class="sort_by_option">
				    			<?php echo $this->lists['ordering'] ; ?>
				    		</span>
				    		<span class="sort_by_dir">
				    			<?php
				    				if ($this->direction == 'asc') {
				    					$newDirection = 'desc' ;
				    					$directionIcon = 'i_asc_arrow.gif' ;
				    				} else {
				    					$newDirection = 'asc' ;
				    					$directionIcon = 'i_desc_arrow.gif' ;
				    				}
				    			?>
				    			<a href="javascript:changeDirection('<?php echo $newDirection; ?>');"><img src="<?php echo JURI::base(true).'/components/com_edocman/assets/images/'.$directionIcon;?>" /></a>
				    		</span>
		    			</td>
		    		</tr>
		    	</table>	
    		<?php	
    		} else {
    		?>
    			<!--<h2 class="dm_title"><?php //echo JText::_('EDOCMAN_DOCUMENTS'); ?></h2>-->
    		<?php	
    		}	    
	        for ($i = 0 , $n = count($this->items) ;  $i < $n ; $i++){
	        	$item = $this->items[$i] ;
	        	if ($item->image)
	        		$imgSrc = JURI::base().'media/com_edocman/document/thumbs/'.$item->image ;
	        	else {
	        		if (!isset($this->config->show_default_document_thumbnail) || $this->config->show_default_document_thumbnail) {
	        			$ext = JString::strtolower(JFile::getExt($item->filename)) ;
	        			if (JFile::exists(JPATH_ROOT.DS.'components/com_edocman/assets/images/icons/32x32/'.$ext.'.png')) {
	        				$imgSrc = JURI::base().'components/com_edocman/assets/images/icons/32x32/'.$ext.'.png' ;
	        			} else {
	        				$imgSrc = JURI::base().'components/com_edocman/assets/images/icons/32x32/zip.png';
	        			}
	        		} else {
	        			$imgSrc = '' ;
	        		}	        		
	        	}		        	
	        	if ($this->config->show_detail_in_popup) {
	        		$url = JRoute::_('index.php?option=com_edocman&view=document&id='.$item->id.'&tmpl=component&Itemid='.$this->Itemid);
	        	} else {
	        		$url = JRoute::_('index.php?option=com_edocman&view=document&id='.$item->id.'&Itemid='.$this->Itemid);
	        	}	
	        	$downloadUrl = JRoute::_('index.php?option=com_edocman&task=document.download&id='.$item->id.'&Itemid='.$this->Itemid) ;
	        	$canDownload = $user->authorise('edocman.download', 'com_edocman.document.'.$item->id) ;
	        	$canEdit	= $user->authorise('core.edit',			'com_edocman.document.'.$item->id);
	        	$canDelete	= $user->authorise('core.delete',			'com_edocman.document.'.$item->id);
    		    $canCheckin	= $user->authorise('core.admin', 'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
    		    $canEditOwn	= $user->authorise('core.edit.own',		'com_edocman.document.'.$item->id) && $item->created_user_id == $userId;    		    
    		    $canChange	= $user->authorise('core.edit.state',	'com_edocman.document.'.$item->id) && $canCheckin;
    		    $canDownload = ($item->user_ids == "" && ($canDownload || $canEdit || ($item->created_user_id == $userId))) || ($item->user_ids && in_array($userId, explode(',', $item->user_ids))) ;	
    		    $invoice = $item->user_ids;  

	        ?>

	        	<div class="dm_row">
					<h3 class="dm_title">				
						<?php 
							if ($imgSrc) {
							?>
								<img src="<?php echo $imgSrc; ?>" alt="<?php echo $item->title; ?>" />
							<?php		
							}
							if ($this->config->use_download_link_instead_of_detail_link && $canDownload) {
							?>
								<a href="<?php echo $downloadUrl; ?>" title="<?php echo JText::_('EDOCMAN_DOWNLOAD'); ?>">
						    		<?php echo $item->title; ?>											
								</a>
							<?php	
							} else {
							?>
								<a href="<?php echo $url; ?>" title="<?php echo $item->title; ?>" <?php echo $popup; ?>>
						    		<?php echo $item->title; ?>											
								</a>	
							<?php	
							}
						?>																																														
					</h3>				
				<?php
				//output document description
				if ($item->short_description) :
					?>
					<div class="dm_description">
						<?php echo $item->short_description ; ?>
					</div>
					<?php
				else :
					?>
						<div class="dm_description">
							<?php echo $item->description ; ?>
						</div>
					<?php	
				endif;
				?>
				<div class="dm_taskbar">
				    <ul>					    	
						<?php
						    if ($canDownload) {
						    ?>
						    	<li>
        							<a href="<?php echo $downloadUrl; ?>" class="btn_taskbar_default">
        								<?php echo JText::_('EDOCMAN_DOWNLOAD'); ?>
        							</a>
        						</li>
						    <?php  
						        if ($this->config->show_view_button && $item->canView) {
						            $viewUrl = JRoute::_('index.php?option=com_edocman&task=document.viewdoc&id='.$item->id.'&Itemid='.$this->Itemid) ;
						        ?>
						        <!--li>
        							<a href="<?php //echo $viewUrl; ?>" target="_blank">
        								<?php //echo JText::_('EDOCMAN_VIEW'); ?>
        							</a>
        						</li-->
						        <?php    
						        }      
						    }						    						    
						?>													    							
						<!--li>
							<a href="<?php //echo $url; ?>" <?php //echo $popup; ?>>
								<?php //echo JText::_('EDOCMAN_DETAILS'); ?>
							</a>
						</li-->						
						<?php	
						    if ($canEdit) {
						        $url = JRoute::_('index.php?option=com_edocman&task=document.edit&id='.$item->id.'&Itemid='.$this->Itemid) ;    
						    ?>
						    	<!--<li>
        							<a href="<?php echo $url; ?>">
        								<?php echo JText::_('EDOCMAN_EDIT'); ?>
        							</a>
        						</li>-->
						    <?php    
						    }	
						    if ($canDelete && $invoice) {
						        $url = JRoute::_('index.php?option=com_edocman&task=document.mail&id='.$item->id.'&Itemid='.$this->category->id) ;    
						    ?>
						    	<li>
        							<a href="<?php echo $url; ?>" class="btn_taskbar_default">
        								<?php echo 'Mail'; ?>
        							</a>
        						</li>
						    <?php    
						    }				    			                            
	                        if ($canDelete) {	                            
						    ?>
						    	<li>
        							<a href="javascript:deleteConfirm(<?php echo $item->id; ?>);" class='btn_taskbar_cancel'>
        								<?php echo JText::_('EDOCMAN_DELETE'); ?>
        							</a>
        						</li>
						    <?php    
						    }						    						    
						    if ($canChange) {
						        if ($item->published) {
						            $text = JText::_('EDOCMAN_UNPUBLISH');
						            $url = "javascript:publishConfirm($item->id, 0)";
						        } else {
						            $url = $url = "javascript:publishConfirm($item->id, 1)"; 
						            $text = JText::_('EDOCMAN_PUBLISH');
						        }   						        
						    ?>
						    	<!--<li>
        							<a href="<?php echo $url; ?>">
        								<?php echo $text; ?>
        							</a>
        						</li>-->
						    <?php    
						    }
						  
						?>						
				    </ul>
				</div>
			</div>	
        <?php
        }
	    ?>	 
	    </div>	    
    	<?php
    		if ($this->pagination->total > $this->pagination->limit) {
    		?>
    			<div class="pagination" align="center">
    				<?php echo $this->pagination->getListFooter(); ?>
    			</div>
    		<?php	
    		}
    	?>	    		   
	<?php } else if ($this->category && count($this->categories) == 0) { ?>
		
	    <br />
	    <div id="dm_docs">
	        <i><?php echo JText::_('EDOCMAN_NO_DOCS'); ?></i>
	    </div>
	<?php } ?>	
	<input type="hidden" name="direction" value="<?php echo $this->direction; ?>" />
	<input type="hidden" name="cid[]" value="0" id="document_id" />
	<input type="hidden" name="category_id" value="<?php echo $this->category->id ; ?>" />
	<input type="hidden" name="Itemid" value="<?php echo $this->Itemid ; ?>" />


	<div style="text-align:right">
	<?php 
		if($this->category->title == 'Product Files'){
			?>
			<a class='cancel-button btn-cancel' href="index.php/products">Cancel</a> 
			<?php
		}
		else if($this->category->title == 'Jobsite Invoices'){
			?>
			<a class='cancel-button btn-cancel' href="index.php/memberarea">Cancel</a> 
			<?php
		}
		else{
			?>

			<a class='cancel-button btn-cancel' href="component/edocman/?view=category&id=<?php $this->category->parent_id; ?>">Cancel</a>
			<?php
		} 
	?>
	</div>
			
	<script language="javascript">
		function changeDirection(newDirection) {
			var form = document.edocman_form ;
			form.direction.value = newDirection ;
			form.submit();
		}	
		function deleteConfirm(id) {
			var msg = "<?php echo JText::_('EDOCMAN_DELETE_CONFIRM'); ?>";
			if (confirm(msg)) {
				var form = document.edocman_form ;
				form.task.value = 'documents.delete';
				document.getElementById('document_id').value = id;
				form.submit();
			}						
		}

		function publishConfirm(id, published) {
			var msg, task ;
			if (published) {
				msg = "<?php echo JText::_("EDOCMAN_PUBLISH_CONFIRM"); ?>";
				task = "documents.publish" ;
			} else {
				msg = "<?php echo JText::_("EDOCMAN_UNPUBLISH_CONFIRM"); ?>";
				task = "documents.unpublish" ;
			}
			if (confirm(msg)) {
				var form = document.edocman_form ;
				form.task.value = task ;
				document.getElementById('document_id').value = id;
				form.submit();
			}			
		}						
	</script>	
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="option" value="com_edocman" />		
	<?php echo JHtml::_('form.token'); ?>
</form>
</div>