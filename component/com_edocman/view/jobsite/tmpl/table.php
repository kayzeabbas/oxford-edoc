<?php
/**
* @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */ 
defined('_JEXEC') or die ; 

if ($this->config->show_detail_in_popup) {
	JHTML::_('behavior.modal');
	$popup = 'class="modal" rel="{handler: \'iframe\', size: {x: 800, y: 500}}"';	
} else {
	$popup = '';
}
$colspan = 4 ;
if ($this->config->category_table_show_filetype)
	$colspan++ ;
if ($this->config->category_table_show_filesize)
	$colspan++ ;
$user  = & JFactory::getUser() ;
?>
<form method="post" name="adminForm" id="adminForm" action="<?php echo JRoute::_('index.php?option=com_edocman&view=category&id='.$this->category->id.'&layout=table&Itemid='.$this->Itemid); ?>">
	<?php
		if ($this->category) {
		?>
			<div class="dm_cat" style="margin-left: 20px; margin-top: 20px;">
				<?php
					if ($this->category->image) {
						$imgUrl = JURI::base().'media/com_edocman/category/thumbs/'.$this->category->image ;
					} else {
						if (!isset($this->config->show_default_category_thumbnail) || $this->config->show_default_category_thumbnail) 
							$imgUrl = JURI::base().'components/com_edocman/assets/images/icons/32x32/folder.png' ;
						else
							$imgUrl = '' ;
					}
					if ($imgUrl) {
					?>
						<img class="dm_thumb-left" src="<?php echo $imgUrl; ?>" alt="<?php echo $this->category->image; ?>" />	
					<?php	
					}									       		    		     
					if($this->category->title != '') :
				    ?>
				    	<h1 class="dm_title" style="display: inline;"><?php echo $this->category->title;?></h1>
				    <?php
				    endif;				    
				    if ($user->authorise('core.create', 'com_edocman.category.'.$this->category->id)) {
				    ?>
    		            <!--<span style="float: right;"><a href="index.php?option=com_edocman&task=document.add&category_id=<?php echo $this->category->id; ?>&Itemid=<?php echo $this->Itemid; ?>" class="edocman_upload_link"><?php echo JText::_('EDOCMAN_UPLOAD'); ?></a></span>
    		                     -->
    		        <?php
    		        }				    				        
					if($this->category->description != '') :
						?><div class="dm_description"><?php echo $this->category->description;?></div><?php
					endif;
				?>
				<div class="clr"></div>
			</div>		
		<?php	
		}
		if ($this->config->show_sort_options) {
		?>
			<table class="table_header" width="100%">
	    		<tr>
	    			<td class="title_column">
	    				<h2 class="dm_title"><?php echo JText::_('EDOCMAN_DOCUMENTS'); ?></h2>	
	    			</td>
	    			<td style="text-align: right;" class="sort_column">
	    				<span class="sort_by_title"><?php echo JText::_('EDOCMAN_SORT_BY'); ?></span>
			    		<span class="sort_by_option">
			    			<?php echo $this->lists['ordering'] ; ?>
			    		</span>
			    		<span class="sort_by_dir">
			    			<?php
			    				if ($this->direction == 'asc') {
			    					$newDirection = 'desc' ;
			    					$directionIcon = 'i_asc_arrow.gif' ;
			    				} else {
			    					$newDirection = 'asc' ;
			    					$directionIcon = 'i_desc_arrow.gif' ;
			    				}
			    			?>
			    			<a href="javascript:changeDirection('<?php echo $newDirection; ?>');"><img src="<?php echo JURI::base(true).'/components/com_edocman/assets/images/'.$directionIcon;?>" /></a>
			    		</span>
	    			</td>
	    		</tr>
	    	</table>	
		<?php		
		} else {
		?>
			<h2 style="padding-bottom: 5px;"><?php echo JText::_('EDOCMAN_DOCUMENTS'); ?></h2>
		<?php	
		}
	?>	
	<table width="100%" class="bt_table bt_table-striped bt_table-bordered bt_table-condensed">
		<thead>
			<tr>
				<th class="sectiontableheader col_no" style="text-align: left;">
					<?php echo JText::_('EDOCMAN_NO'); ?>
				</th>
				<th class="sectiontableheader">
					<?php echo JText::_('EDOCMAN_TITLE'); ?>
				</th>			
				<?php
					if ($this->config->category_table_show_filetype) {
					?>
						<th class="sectiontableheader table_filetype_col">
							<?php echo JText::_('EDOCMAN_FILE_TYPE'); ?>
						</th>	
					<?php	
					}
					if ($this->config->category_table_show_filesize) {
						?>
						<th class="sectiontableheader table_filesize_col">
							<?php echo JText::_('EDOCMAN_FILESIZE'); ?>
						</th>	
					<?php	
					}					
				?>				
				<th class="sectiontableheader created_date_col">
					<?php echo JText::_('EDOCMAN_CREATED_DATE'); ?>
				</th>
				<th class="sectiontableheader download_col">
					<?php echo JText::_('EDOCMAN_DOWNLOAD'); ?>
				</th>					
			</tr>
		</thead>
		<tbody>
		<?php
			$tabs = array('sectiontableentry1', 'sectiontableentry1');
			$total = 0 ;
			$k = 0 ;			
			for ($i = 0 , $n = count($this->items) ; $i < $n; $i++) {
				$item = $this->items[$i] ;
				$tab = $tabs[$k] ;											
				if ($this->config->show_detail_in_popup) {
	        		$url = JRoute::_('index.php?option=com_edocman&view=document&id='.$item->id.'&tmpl=component&Itemid='.$this->Itemid);
	        	} else {
	        		$url = JRoute::_('index.php?option=com_edocman&view=document&id='.$item->id.'&Itemid='.$this->Itemid);
	        	}	  	        	
	        	$canDownload = $user->authorise('edocman.download', 'com_edocman.document.'.$item->id) ;
	        	$downloadUrl = JRoute::_('index.php?option=com_edocman&task=document.download&id='.$item->id.'&Itemid='.$this->Itemid) ;

	        	if ($this->config->category_table_show_filetype || $this->config->category_table_show_filesize) {
	        		require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_edocman'.DS.'helpers'.DS.'file.class.php' ;
	        		$item->data = new Edocman_File($item->filename, $this->config->documents_path) ;
	        	}	        		
			?>
				<tr class="<?php echo $tab; ?>">
					<td>
						<?php echo $i + 1 ; ?>
					</td>					
					<td>
						<?php 
							if ($this->config->use_download_link_instead_of_detail_link && $canDownload) {
							?>
								<a href="<?php echo $downloadUrl; ?>" <?php echo $popup; ?>><?php echo $item->title; ?></a>	
							<?php	
							} else {
							?>
								<a href="<?php echo $url; ?>" <?php echo $popup; ?>><?php echo $item->title; ?></a>
							<?php	
							}	
						?>						
					</td>		
					<?php 
						if ($this->config->category_table_show_filetype) {
						?>
							<td>
								<?php echo $item->data->mime; ?>
							</td>
						<?php	
						}

						if ($this->config->category_table_show_filesize) {
						?>
							<td>
								<?php echo $item->data->size; ?>
							</td>
						<?php	
						}
					?>
					<td align="center">
						<?php echo JHtml::_('date', $item->created_time, $this->config->date_format, null); ?>
					</td>
					<td align="center">
						<?php                            
                            if ($canDownload) {
                            ?>                            	
    							<a href="<?php echo $downloadUrl; ?>" class="edocman_download_link">
    								<img src="<?php echo JURI::base().'/components/com_edocman/assets/images/icons/download.png'; ?>" />
    							</a>        						
                            <?php    
                            }     
						?>						
					</td>			
				</tr>
			<?php				
				$k = 1 - $k ;				
			}			
			?>
				<tbody>
			<?php		
			if ($this->pagination->total > $this->pagination->limit) {
			?>	<tfoot>
					<tr>
						<td colspan="<?php echo $colspan; ?>" style="text-align: center;"><div class="pagination"><?php echo $this->pagination->getListFooter(); ?></div></td>
					</tr>	
				</tfoot>			
			<?php		
			}
		?>				
	</table>					
	<input type="hidden" name="direction" value="<?php echo $this->direction; ?>" />		
	<script type="text/javascript">
		function changeDirection(newDirection) {
			var form = document.adminForm ;
			form.direction.value = newDirection ;
			form.submit();
		}	
	</script>
			
</form>