<?php
/**
* @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die ;
jimport( 'joomla.application.component.view');
/**
 * HTML View class for Edocman component
 *
 * @static
 * @package		Joomla
 * @subpackage	Edocman
 * @since 1.5
 */
class EdocmanViewJobsiteHtml extends OSViewHtml
{	
	function display($tpl = null)
	{
        $jInput = JFactory::getApplication()->input;
        $model  = $this->getModel();

		jimport('joomla.filesystem.file') ;
        if (JFactory::getUser()->id == 0){
            JFactory::getApplication()->redirect("/login","Session Timeout Or Not Logged Ifn");
        }
		$mainframe = JFactory::getApplication('site') ;		
		$document = & JFactory::getDocument();							
		$pathway = & $mainframe->getPathway();							
		$db = & JFactory::getDBO();						
		$user = & JFactory::getUser();
		$config = EdocmanHelper::getConfig() ;
		$userId = $user->id;
		$Itemid = $jInput->getInt('Itemid', 0) ;
		$categoryId = $jInput->getInt('id', 0) ;
        $jobsiteID = $jInput->get("jobsite_search_id",-1);
        $this->isAdmin = $user->authorise("core.admin");
		
		if (!$categoryId) {
			$menus = JSite::getMenu();
			$menu = $menus->getActive();
			if (is_object($menu)) {
			    $params = new JRegistry() ;

			    $params->loadString($menu->params) ;

				$categoryId = $params->get('id', 0);
				if ($categoryId) {
                    $jInput->get('id', $categoryId);
					$this->getModel()->setCategory($categoryId);
				}
			}
		}		
		//Check category access
		
		if ($categoryId && !EdocmanHelper::canAccessCategory($categoryId)) {
		    JFactory::getApplication()->redirect('/login', JText::_('EDOCMAN_INVALID_CATEGORY'));
		    return ;
		}
        $invoices = array();
        $noninvoices = array();
		if ($categoryId) {
            $items = &$model->getData();


            foreach ($items as $item){
                $match = array();
                $array = explode("-",$item->title);
                if (count($array)>2){
                    $noninvoices[] = $item;
                    continue;
                }
                if (preg_match("/[0-9]+/",$array[1],$match)) {

                    if (preg_match("/\(.*\)/", $array[1])) {
                        $invoices[$match[0]]['detailed'] = $item;
                    } else {
                        $invoices[$match[0]]['summary'] = $item;
                    }
                }else{
                    $noninvoices[] = $item;
                }
            }

        }

        $this->invoices = $invoices;
        $this->noninvoices = $noninvoices;

        if ($jobsiteID > 0){
            $this->jobsiteID = $jobsiteID;
            $model = $this->getModel();
            $categories = $model->getJobSites($jobsiteID);
        }else{
            $categories = & $model->getCategories();
        }

		$pagination = & $model->getPagination();
		$category = & $model->getCategory();
		if ($category) {
			$metaKey = $category->metakey ;
			$metaDescription = $category->metadesc;
			if ($metaKey) {
				$document->setMetaData('keywords', $metaKey);
			}
			
			if ($metaDescription) {
				$document->setMetaData('description', $metaDescription);
			}
						
			$document->setTitle($category->title) ;
		}	
		if ($config->fix_breadcrumb_issue !== 1) {
			//Pathway
			$parents = EdocmanHelper::getParentCategories($categoryId) ;
			for ($i = count($parents)-1 ; $i > 0 ; $i--) {
				$parent = $parents[$i] ;
				if ($parent->total_children)
					$pathUrl = JRoute::_('index.php?option=com_edocman&view=categories&id='.$parent->id.'&Itemid='.$Itemid);
				else  {
					if ($parent->category_layout == 'default')
						$pathUrl = JRoute::_('index.php?option=com_edocman&view=category&id='.$parent->id.'&Itemid='.$Itemid);
					else
						$pathUrl = JRoute::_('index.php?option=com_edocman&view=category&layout='.$parent->category_layout.'&id='.$parent->id.'&Itemid='.$Itemid);
				}
				$pathway->addItem($parent->title, $pathUrl);
			}
		}													
		$pathway->addItem($category->title);														
		//Sort		
		$lists = array();
		if ($config->show_sort_options) {
			$ordering = $jInput->get('ordering',  $config->default_sort_option);
			$direction = $jInput->get('direction', $config->default_sort_direction);
			$options = array() ;
			$options[] = JHTML::_('select.option', 'a.title', JText::_('EDOCMAN_TITLE'));
			$options[] = JHTML::_('select.option', 'a,a.created_time', JText::_('EDOCMAN_DATE'));
			$options[] = JHTML::_('select.option', 'a.ordering', JText::_('EDOCMAN_ORDERING'));
            $options[] = JHTML::_('select.option', 'a.modified_time', JText::_('EDOCMAN_MODIFIED'));
			$lists['ordering'] = JHTML::_('select.genericlist', $options, 'ordering', ' class="inputbox" onchange="submit();" ', 'value', 'text', $ordering);			
			$this->lists = $lists;
			$this->ordering = $ordering;
			$this->direction = $direction;
		}				
		
		if ($config->process_plugin && count($items)) {
		    foreach ($items as $item) {
		        $item->short_description = JHtml::_('content.prepare', $item->short_description);
		    }
		}
				
		if ($config->show_view_button && count($items)) {		  
		   $extsForView = explode(',', $config->exts_for_view) ;
		   for ($i = 0 , $n = count($extsForView) ; $i < $n ; $i++) {
		       $extsForView[$i] = strtolower(trim($extsForView[$i])) ;
		   }		   
		   foreach ($items as $item) {
		       $fileName = $item->filename ;
		       $fileExt = strtolower(JFile::getExt($fileName)) ;
		       if (in_array($fileExt, $extsForView)) {
		           $item->canView = 1 ;
		       } else {
		           $item->canView = 0 ;
		       }    
		   }     
		}			
		if ($category->category_layout) {			
			if (!isset($_GET['layout']))
				$this->setLayout($category->category_layout) ;			
		}			
		$layout = $this->getLayout() ;
		if ($layout == 'table') {
			//Add bootstrap css for table layout
			$document->addStyleSheet(JURI::base(true).'/components/com_edocman/assets/css/bootstrap.css') ;
		}					
		$this->userId = $userId;
		$this->items = $items;
		$this->pagination = $pagination;
		$this->Itemid = $Itemid;
		$this->config = $config;
		$this->category = $category;
		$this->categories = $categories;

		parent::display();
	}
}