<?php
/**
 * @version         1.7.6
 * @package        Joomla
 * @subpackage     EDocman
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2011-2016 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die ;
require_once JPATH_ROOT.'/components/com_edocman/helper/file.class.php' ;
$uploadItemId = EDocmanHelperRoute::findUploadMenuId($this->Itemid);
$bootstrapHelper = $this->bootstrapHelper;
?>
<div id="edocman-category-page-default" class="edocman-container <?php echo $bootstrapHelper->getClassMapping('row-fluid'); ?>">
	<form method="post" name="edocman_form" id="edocman_form" action="<?php echo JRoute::_('index.php?option=com_edocman&view=userdocuments&Itemid='.$this->Itemid); ?>">
		<h1 class="edocman-page-heading" style="display: inline; margin-bottom: 10px;">
			<?php echo JText::_('EDOCMAN_MANAGE_DOCUMENTS'); ?>
			<span style="float: right;"><a href="<?php echo JRoute::_('index.php?option=com_edocman&view=document&layout=edit&Itemid=' . $uploadItemId); ?>" class="edocman_upload_link btn btn-success"><i class="edocman-icon-upload"></i><?php echo JText::_('EDOCMAN_UPLOAD'); ?></a></span>
		</h1>
		<?php
			if (count($this->items))
			{
			?>
				<table class="table table-striped table-bordered table-condensed" style="margin-top: 10px;">
					<thead>
					<tr>
						<th>
							<?php echo JText::_('EDOCMAN_TITLE'); ?>
						</th>
						<th class="edocman-created-date-col center">
							<?php echo JText::_('EDOCMAN_CREATED_DATE'); ?>
						</th>
						<th>
							<?php echo JText::_('EDOCMAN_FILE_TYPE'); ?>
						</th>
						<th>
							<?php echo JText::_('EDOCMAN_FILESIZE'); ?>
						</th>
						<th class="center edocman-edit-delete"></th>
					</tr>
					</thead>
					<tbody>
					<?php
					$total = 0 ;
					$activeItemid = $this->defaultItemid;
					$catId = 0;
					for ($i = 0 , $n = count($this->items) ; $i < $n; $i++)
					{
						$catId = $categoryId;
						$item = $this->items[$i] ;
						$item->data = new EDocman_File($item->id,$item->filename, $this->path);
						$Itemid = EDocmanHelperRoute::getDocumentMenuId($item->id, $catId, $activeItemid);
						$url = JRoute::_('index.php?option=com_edocman&view=document&id='.$item->id.'&catid='.$catId.'&Itemid='.$Itemid);
						?>
						<tr>
							<td>
								<a href="<?php echo $url; ?>" target="_blank"><?php echo $item->title; ?></a>
							</td>
							<td class="center">
								<?php echo JHtml::_('date', $item->created_time, $config->date_format, null); ?>
							</td>
							<td>
								<?php echo $item->data->mime; ?>
							</td>
							<td>
								<?php echo $item->data->size; ?>
							</td>
							<th class="center">
								<a class="btn" href="<?php echo JRoute::_('index.php?option=com_edocman&task=document.edit&id='.$item->id.'&Itemid='.$Itemid) ; ?>">
									<i class="edocman-icon-pencil"></i>
								</a>
								<a class="btn" href="javascript:deleteConfirm(<?php echo $item->id; ?>);">
									<i class="edocman-icon-trash"></i>
								</a>
							</th>
						</tr>
					<?php
					}
					?>
					</tbody>
				</table>
			<?php
			}
			else
			{
				echo JText::_('EDOCMAN_NO_DOCUMENT_UPLOADED_YET');
			}
			if ($this->pagination->total > $this->pagination->limit)
			{
				?>
				<div class="pagination">
					<?php echo $this->pagination->getPagesLinks(); ?>
				</div>
			<?php
			}

		?>
		<input type="hidden" name="cid[]" value="0" id="document_id" />
		<input type="hidden" name="category_id" value="0" />
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token'); ?>

		<script type="text/javascript">
			function deleteConfirm(id)
			{
				var msg = "<?php echo JText::_('EDOCMAN_DELETE_CONFIRM'); ?>";
				if (confirm(msg))
				{
					var form = document.edocman_form ;
					form.task.value = 'documents.delete';
					document.getElementById('document_id').value = id;
					form.submit();
				}
			}
		</script>
	</form>
</div>