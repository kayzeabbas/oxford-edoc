<?php
/**
 * @version         1.7.6
 * @package        Joomla
 * @subpackage     EDocman
 * @author         Tuan Pham Ngoc
 * @copyright      Copyright (C) 2011-2016 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die ;
JHtml::_('behavior.modal', 'a.edocman-modal');
$item = $this->item ;
$config = $this->config;
$url = JRoute::_(EDocmanHelperRoute::getDocumentRoute($item->id, $this->categoryId, $this->Itemid), false);
$siteUrl = JUri::base();
$socialUrl = JUri::getInstance()->toString();
$user = JFactory::getUser() ;
$userId = $user->get('id');
$canDownload = $user->authorise('edocman.download', 'com_edocman.document.'.$item->id) ;
$canEdit	 = $user->authorise('core.edit',			'com_edocman.document.'.$item->id);
$canDelete	 = $user->authorise('core.delete',		'com_edocman.document.'.$item->id);
if(!$canDelete){
	$canDelete	= $user->authorise('edocman.deleteown',			'com_edocman.document.'.$item->id) && ($item->created_user_id == $userId);
}
$canCheckin	= $user->authorise('core.admin', 'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
$canEditOwn	= $user->authorise('core.edit.own',		'com_edocman.document.'.$item->id) && $item->created_user_id == $userId;
$canChange	= $user->authorise('core.edit.state',	'com_edocman.document.'.$item->id) && $canCheckin;
$canDownload = ($item->created_user_id == $userId) || ($item->user_ids =="" && ($canDownload || $canEdit) || ($item->user_ids != '' && in_array($userId, explode(',', $item->user_ids)))) ;
$bootstrapHelper = $this->bootstrapHelper;
$btnClass = $bootstrapHelper->getClassMapping('btn');
require_once JPATH_ROOT.'/components/com_edocman/helper/file.class.php' ;
$item->data = new EDocman_File($item->id,$item->filename, $config->documents_path) ;
if ($this->config->collect_downloader_information && !$userId)
{
	$showDownloadForm = true;
}
else
{
	$showDownloadForm = false;
}
?>
<div id="edocman-document-page" class="edocman-container edocman-document">
	<div class="edocman-box-heading clearfix">
		<h1 class="edocman-page-heading pull-left">
			<?php
			if($config->show_icon_beside_title){
			?>
            	<i class="<?php echo $item->data->fileicon; ?>"></i>
			<?php
			}
			?>
			<?php echo $item->title; ?>
			<?php
			if(!empty($item->indicators) || !empty($item->new_indicator))
			{
				$indicators = $item->indicators;
			?>
			<span class="indicators">
				<?php
				if (!empty($item->new_indicator))
				{
				?>
					<span class="edocman_new">
							<?php echo JText::_('EDOCMAN_NEW');?>
					</span>
				<?php
				}
				if(in_array('featured', $indicators))
				{
				?>
					<span class="edocman_featured">
							<?php echo JText::_('EDOCMAN_FEATURED');?>
					</span>
				<?php
				}
				if(in_array('hot', $indicators))
				{
				?>
					<span  class="edocman_hot">
						<?php echo JText::_('EDOCMAN_HOT');?>
					</span>
				<?php
				}
				?>
			</span>
			<?php
			}
			?>
		</h1>
		<div class="clearfix"></div>
		<p class="edocman_document_details_information">
			<?php
			if(($item->created_time > 0) && ($this->config->show_creation_date == 1)){
			?>
			<span class="created-on-label">
				<time datetime="<?php echo $item->created_time;?>" itemprop="datePublished"> <?php echo JText::_('EDOCMAN_PUBLISHED_ON');?> <?php echo date("j F Y",strtotime($item->created_time));?> </time>
			</span>
			<?php
			}
			if(($item->created_user_id > 0) && ($this->config->show_creation_user == 1)){
			?>
				<span class="owner-label">
					<?php
					echo JText::_('EDOCMAN_BY');
					?>
					<span itemprop="author"><?php echo JFactory::getUser($item->created_user_id)->name;?></span>
				</span>
			<?php
			}
			if ($this->config->show_number_downloaded){
			?>
				<meta content="UserDownloads:<?php echo $item->downloads;?>" itemprop="interactionCount">
				<span class="hits-label"> <?php echo $item->downloads;?> <?php echo JText::_('EDOCMAN_DOWNLOADS');?> </span>
			<?php
			}
			?>
		</p>
	</div>
	<div id="edocman-document-details" class="edocman-description">
	<?php
		if ($this->config->show_social_sharing_buttons !== '0')
		{
			?>
			<div class="sharing clearfix" >
				<!-- FB -->
				<div style="float:left;" id="rsep_fb_like">
					<div id="fb-root"></div>
					<script src="http://connect.facebook.net/en_US/all.js" type="text/javascript"></script>
					<script type="text/javascript">
						FB.init({appId: '340486642645761', status: true, cookie: true, xfbml: true});
					</script>
					<fb:like href="<?php echo $socialUrl; ?>" send="true" layout="button_count" width="150" show_faces="false"></fb:like>
				</div>

				<!-- Twitter -->
				<div style="float:left;" id="rsep_twitter">
					<a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $this->item->title." ".$socialUrl; ?>">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>

				<!-- GPlus -->
				<div style="float:left;" id="rsep_gplus">
					<!-- Place this tag where you want the +1 button to render -->
					<g:plusone size="medium"></g:plusone>

					<!-- Place this render call where appropriate -->
					<script type="text/javascript">
						(function() {
							var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
							po.src = 'https://apis.google.com/js/plusone.js';
							var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
						})();
					</script>
				</div>
			</div>
		<?php
		}
	?>
	<div class="edocman-description-details clearfix">
		<?php
			if ($item->image && JFile::exists(JPATH_ROOT.'/media/com_edocman/document/thumbs/'.$item->image))
			{
				$modal_url = $imgSrc = JUri::base().'media/com_edocman/document/'.$item->image ;
				$imgSrc = JUri::base().'media/com_edocman/document/thumbs/'.$item->image ;
			}
			else
			{
				$modal_url = "";
				if (!isset($this->config->show_default_document_thumbnail) || $this->config->show_default_document_thumbnail)
				{
					$ext = JString::strtolower(EDocmanHelper::getFileExtension($item)) ;
					if (JFile::exists(JPATH_ROOT.'/components/com_edocman/assets/images/icons/32x32/'.$ext.'.png'))
					{
						$imgSrc = JUri::base().'components/com_edocman/assets/images/icons/32x32/'.$ext.'.png' ;
					}
					else
					{
						$imgSrc = JUri::base().'components/com_edocman/assets/images/icons/32x32/generic.png';
					}
				}
				else
				{
					$imgSrc = '' ;
				}
			}
			if ($imgSrc)
			{
				if($modal_url != ""){
					?>
					<a href="<?php echo $modal_url?>" class="edocman-modal edocman_thumbnail thumbnail">
					<?php
				}
				?>
					<img src="<?php echo $imgSrc; ?>" alt="<?php echo $item->title; ?>" class="edocman-thumb-left" />
				<?php
				if($modal_url != ""){
					?>
					</a>
					<?php
				}
			}
			//echo $item->short_description;
			$item->description = JHtml::_('content.prepare', $item->description);
			echo $item->description ;
		?>
	</div>
	<div id="edocman-document-info" class="clearfix">
		<table class="table table-bordered table-striped">
			<tbody>
			<?php
				if (($this->config->show_filename) && ($this->item->original_filename))
				{
				?>
				<tr>
					<td class="edocman-document-property-label">
						<?php echo JText::_('EDOCMAN_FILENAME') ?>:
					</td>
					<td class="edocman-document-property-value">
						<?php echo $this->item->original_filename; ?>
					</td>
				</tr>
				<?php
				}
				if (($this->config->show_document_version) and ($this->item->document_version != ""))
				{
				?>
					<tr>
						<td class="edocman-document-property-label"><?php echo JText::_('EDOCMAN_VERSION') ?>:</td>
						<td class="edocman-document-property-value"><?php echo $this->item->document_version; ?></td>
					</tr>
				<?php
				}
				if (($this->config->show_filesize) && ($this->item->document_url == ""))
				{
				?>
					<tr>
						<td class="edocman-document-property-label"><?php echo JText::_('EDOCMAN_FILESIZE') ?>:</td>
						<td class="edocman-document-property-value"><?php echo $this->item->data->size; ?></td>
					</tr>
				<?php
				}
				if ($this->config->show_filetype)
				{
				?>
					<tr>
						<td class="edocman-document-property-label"><?php echo JText::_('EDOCMAN_FILE_TYPE'); ?>:</td>
						<td class="edocman-document-property-value"><?php echo $this->item->data->mime; ?></td>
					</tr>
				<?php
				}
				if ($this->config->show_hits)
				{
				?>
					<tr>
						<td class="edocman-document-property-label"><?php echo JText::_('EDOCMAN_HITS'); ?>:</td>
						<td class="edocman-document-property-value"><?php echo $item->hits; ?>&nbsp;<?php echo JText::_('EDOCMAN_HITS'); ?></td>
					</tr>
				<?php
				}
				if ($this->config->show_number_downloaded)
				{
				?>
					<tr>
						<td class="edocman-document-property-label"><strong><?php echo JText::_('EDOCMAN_DOWNLOAD'); ?>:</strong></td>
						<td class="edocman-document-property-value">
							<?php echo $item->downloads; ?> <?php echo JText::_('EDOCMAN_TIMES');?>
						</td>
					</tr>
				<?php
				}
				$created_user_id = $item->created_user_id;
				if (($this->config->show_creation_user) and ((int)$created_user_id > 0))
				{
				?>
					<tr>
						<td class="edocman-document-property-label"><strong><?php echo JText::_('EDOCMAN_CREATED_USER'); ?>:</strong></td>
						<td class="edocman-document-property-value">
							<?php 
							$created_user = JFactory::getUser($created_user_id);
							echo $created_user->name;
							?>
						</td>
					</tr>
				<?php
				}
				if ($this->config->show_creation_date)
				{
				?>
					<tr>
						<td class="edocman-document-property-label"><strong><?php echo JText::_('EDOCMAN_CREATED_DATE'); ?>:</strong></td>
						<td class="edocman-document-property-value">
							<?php echo JHtml::_('date', $item->created_time, $this->config->date_format); ?>
						</td>
					</tr>
				<?php
				}
				if ($this->config->show_modified_date)
				{
				?>
					<tr>
						<td class="edocman-document-property-label"><?php echo JText::_('EDOCMAN_LAST_UPDATED'); ?>:</td>
						<td class="edocman-document-property-value">
							<?php echo JHtml::_('date', $item->modified_time ? $item->modified_time : $item->created_time, $this->config->date_format); ?>
						</td>
					</tr>
				<?php
				}
				if (count($this->item->params))
				{
					foreach($this->fields as $field)
					{
						if (!empty($this->item->params[$field->fieldname]))
						{
						?>
							<tr>
								<td class="edocman-document-property-label"><?php echo $field->title; ?>:</td>
								<td class="edocman-document-property-value">
									<?php echo $this->item->params[$field->fieldname]; ?>
								</td>
							</tr>
						<?php
						}
					}
				}
				if ($this->item->license_id)
				{
				?>
					<tr>
						<td class="edocman-document-property-label"><?php echo JText::_('EDOCMAN_LICENSE'); ?>:</td>
						<td class="edocman-document-property-value">
							<a href="<?php echo JRoute::_('index.php?option=com_edocman&view=license&id='.$this->item->license_id.'&Itemid='.$this->Itemid.'&tmpl=component'); ?>" class="edocman-modal" rel="{handler: \'iframe\', size: {x: 800, y: 500}}"><?php echo $this->item->license_title ; ?></a>
						</td>
					</tr>
				<?php
				}
				if($this->item->document_history != ""){
				?>
				<tr>
					<td class="edocman-document-property-label">
						<?php echo JText::_('EDOCMAN_HISTORY') ?>:
					</td>
					<td class="edocman-document-property-value">
						<?php echo $this->item->document_history; ?>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
   </div>
	<?php
	if ($item->tags)
	{
		$tags = explode(',', $item->tags);
	?>
		<ul class="edocman_tag_container clearfix">
			<span class="taged_label btn primary large">
				<?php echo JText::_('EDOCMAN_TAGED_UNDER');?>
			</span>
			<?php
			foreach ($tags as $tag)
			{
			?>
				<li class="edocman_tag_item">
					<a href="<?php echo JRoute::_('index.php?option=com_edocman&view=search&filter_tag='.$tag.'&Itemid='.$this->Itemid); ?>"><?php echo $tag; ?></a>
				</li>
			<?php
			}
			?>
		</ul>
	<?php
	}
if ($this->showTaskBar)
{
?>
	<div class="edocman-taskbar clearfix">
		<ul>
			<?php
			if ($canDownload && $this->config->hide_download_button !== '1')
			{
				$downloadUrl = JRoute::_('index.php?option=com_edocman&task=document.download&id='.$item->id.'&Itemid='.$this->Itemid) ;
				if ($showDownloadForm)
				{
				?>
					<li>
						<a data-toggle="modal" data-document-title="<?php echo $this->item->title; ?>" title="<?php echo JText::_('EDOCMAN_DOWNLOAD'); ?>"  id="<?php echo $this->item->id; ?>" class="email-popup edocman-document-title-link <?php echo $btnClass; ?>" href="#form-content">
							<?php if($this->item->document_url != ""){?>
								<i class="edicon edicon-link"></i>
								<?php echo JText::_('EDOCMAN_OPEN_DOCUMENT'); ?>
							<?php }else{ ?>
								<i class="edocman-icon-download"></i>
								<?php echo JText::_('EDOCMAN_DOWNLOAD'); ?>
							<?php } ?>
						</a>
					</li>
				<?php
				}
				else
				{
				?>
					<li>
						<a href="<?php echo $downloadUrl; ?>" class="<?php echo $btnClass; ?>">
							<?php if($this->item->document_url != ""){?>
								<i class="edicon edicon-link"></i>
								<?php echo JText::_('EDOCMAN_OPEN_DOCUMENT'); ?>
							<?php }else{ ?>
								<i class="edicon edicon-download"></i>
								<?php echo JText::_('EDOCMAN_DOWNLOAD'); ?>
							<?php } ?>
						</a>
					</li>
				<?php
				}
			}elseif(!$canDownload && $this->config->hide_download_button !== '1' && $this->config->login_to_download && (int)$userId == 0){
				?>
				<li>
					<a data-toggle="modal" class="email-popup edocman-download-link btn edocman-download-btn" href="#login-form">
						<span class="edocman_download_label">
							<?php
							echo JText::_('EDOCMAN_LOGIN_TO_DOWNLOAD'); ?>
						</span>
					</a>
				</li>
				<?php
			}

			if ($canDownload && $this->config->show_view_button && $item->canView)
			{
				$viewUrl = JRoute::_('index.php?option=com_edocman&task=document.viewdoc&id='.$item->id.'&Itemid='.$this->Itemid) ;
			?>
				<li>
					<a href="<?php echo $viewUrl; ?>" target="_blank" class="<?php echo $btnClass; ?>">
						<i class="edocman-icon-eye-open"></i>
						<?php echo JText::_('EDOCMAN_VIEW'); ?>
					</a>
				</li>
			<?php
			}

			//share document
			if($this->config->turn_on_sharing){
			?>
				<li>
					<a data-toggle="modal" class="email-popup edocman-download-link btn" href="#sharing-form" data-document-title="<?php echo $this->item->title; ?>" title="<?php echo JText::_('EDOCMAN_SHARE_DOCUMENT'); ?>"  id="<?php echo $this->item->id; ?>" >
						<i class="edicon edicon-share"></i>
						<?php
						echo JText::_('EDOCMAN_SHARE_DOCUMENT'); 
						?>
					</a>
				</li>
			<?php
			}
			//end share document

			if ($canEdit || $canEditOwn)
			{
				$url = JRoute::_('index.php?option=com_edocman&task=document.edit&id='.$item->id.'&Itemid='.$this->Itemid) ;
			?>
				<li>
					<a href="<?php echo $url; ?>" class="<?php echo $btnClass; ?>">
						<i class="edocman-icon-pencil"></i>
						<?php echo JText::_('EDOCMAN_EDIT'); ?>
					</a>
				</li>
			<?php
			}
			if ($canDelete)
			{
			?>
				<li>
					<a href="javascript:deleteConfirm();" class="<?php echo $btnClass; ?>">
						<i class="edocman-icon-trash"></i>
						<?php echo JText::_('EDOCMAN_DELETE'); ?>
					</a>
				</li>
			<?php
			}
			if ($canChange) {
				if ($item->published)
				{
					$text = JText::_('EDOCMAN_UNPUBLISH');
					$url = "javascript:publishConfirm('documents.unpublish')";
					$class = 'edocman-icon-remove';
				}
				else
				{
					$url = "javascript:publishConfirm('documents.publish')";
					$text = JText::_('EDOCMAN_PUBLISH');
					$class = 'edocman-icon-ok';
				}
				?>
				<li>
					<a href="<?php echo $url; ?>" class="<?php echo $btnClass; ?>">
						<i class="<?php echo $class; ?>"></i>
						<?php echo $text; ?>
					</a>
				</li>
			<?php
			}
			?>
		</ul>
	</div>
<?php
}
if (@$this->config->jcomment_integration && file_exists(JPATH_ROOT.'/components/com_jcomments/jcomments.php'))
{
	require_once JPATH_ROOT.'/components/com_jcomments/jcomments.php';
?>
	<div class="edocman-comments clearfix">
		<?php  echo JComments::showComments($item->id, 'com_edocman', $item->title); ?>
	</div>
<?php
}
?>
</div>
</div>

<form method="post" name="edocman_form" id="edocman_form" action="index.php">
	<input type="hidden" name="cid[]" value="<?php echo $item->id; ?>" id="document_id" />
	<input type="hidden" name="category_id" value="<?php echo $this->categoryId ; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
	<input type="hidden" name="option" value="com_edocman" />
	<?php echo JHtml::_('form.token'); ?>

	<script type="text/javascript">
		function deleteConfirm()
		{
			var msg = "<?php echo JText::_('EDOCMAN_DELETE_CONFIRM'); ?>";
			if (confirm(msg))
			{
				var form = document.edocman_form ;
				form.task.value = 'documents.delete';
				form.submit();
			}
		}
		function publishConfirm(task) {
			var msg;
			if (task == 'documents.publish')
			{
				msg = "<?php echo JText::_('EDOCMAN_PUBLISH_CONFIRM'); ?>";
			}
			else
			{
				msg = "<?php echo JText::_('EDOCMAN_UNPUBLISH_CONFIRM'); ?>";
			}
			if (confirm(msg))
			{
				var form = document.edocman_form ;
				form.task.value = task;
				form.submit();
			}
		}
	</script>
</form>
<?php
echo EDocmanHelperHtml::loadCommonLayout('common/sharing.php', array('bootstrapHelper' => $bootstrapHelper));

if ($showDownloadForm)
{
	echo EDocmanHelperHtml::loadCommonLayout('common/modal.php', array('bootstrapHelper' => $bootstrapHelper));
}
if(!$canDownload && $this->config->hide_download_button !== '1' && $this->config->login_to_download && (int)$userId == 0){
	echo EDocmanHelperHtml::loadCommonLayout('common/login.php', array('bootstrapHelper' => $bootstrapHelper));
}
?>