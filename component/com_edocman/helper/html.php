<?php
/**
 * @version         1.7.6
 * @package        Joomla
 * @subpackage     EDocman
 * @author         Tuan Pham Ngoc
 * @copyright	Copyright (C) 2010 - 2013 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */
abstract class EDocmanHelperHtml
{
    /**
     * Function to render a common layout which is used in different views
     * @param string $layout	Relative path to the layout file
     * @param array $data	An array contains the data passed to layout for rendering
     */
    public static function loadCommonLayout($layout, $data = array())
    {
        $app = JFactory::getApplication();
        $themeFile = str_replace('/tmpl', '', $layout);
        if (JFile::exists(JPATH_THEMES . '/' . $app->getTemplate() . '/html/com_edocman/' . $themeFile))
        {
            $path = JPATH_THEMES . '/' . $app->getTemplate() . '/html/com_edocman/' . $themeFile;
        }
        elseif (JFile::exists(JPATH_ROOT . '/components/com_edocman/view/' . $layout))
        {
            $path = JPATH_ROOT . '/components/com_edocman/view/' . $layout;
        }
        else
        {
            throw new RuntimeException(JText::_('The given shared template path is not exist'));
        }
        // Start an output buffer.
        ob_start();
        extract($data);
        // Load the layout.
        include $path;
        // Get the layout contents.
        $output = ob_get_clean();

        return $output;
    }
}