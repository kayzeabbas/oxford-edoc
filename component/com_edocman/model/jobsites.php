
<?php

/**
 * @version		1.1.0
 * @package		Joomla
 * @subpackage	Edocman
 * @author  Tuan Pham Ngoc
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;
jimport('joomla.application.component.model');

/**
 * Edocman Component Category Model
 *
 * @package		Joomla
 * @subpackage	Edocman
 * @since 1.5
 */
class EdocmanModelJobsites extends OSModelList
{

    /**
     * The category is being displayed
     * @var int
     */
    var $categoryId = 0;

    /**
     * Documents data array
     *
     * @var array
     */
    var $_data = null;

    /**
     * Pagination object
     *
     * @var object
     */
    var $_pagination = null;

    /**
     * Constructor
     *
     * @since 1.5
     */
    function __construct()
    {
        $config['table'] = '#__edocman_categories';
        parent::__construct($config);

        $mainframe = JFactory::getApplication('site');
        // Get the pagination request variables
        $listLength = EdocmanHelper::getConfigValue('number_documents');

        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit',
                $listLength ? $listLength : JFactory::getConfig()->get('list_limit'), 'int');

        $jInput = JFactory::getApplication()->input;
        $limitstart = $jInput->get('limitstart', 0);
        // In case limit has been changed, adjust limitstart accordingly
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        $this->state->setDefault('limit', $limit);
        $this->state->setDefault('limitstart', $limitstart);

        $this->categoryId = $this->getCategoryID();
    }

    /**
     * Get list of sub categories of the current category
     * @return array
     */
    function getCategories($jobsiteID = null)
    {
        $app = & JFactory::getApplication();
        $db = & JFactory::getDBO();
        $config = EdocmanHelper::getConfig();
        $user = & JFactory::getUser();
        $userId = $user->id;
        $where = [];

        //$where[] = ' tbl.parent_id = '.$this->categoryId ;
        $where[] = ' tbl.published = 1 ';
        //$where[] = "((tbl.user_ids = '' AND tbl.access IN (".implode(',', $user->getAuthorisedViewLevels()).")) OR tbl.user_ids='$userId' OR tbl.user_ids LIKE '$userId,%' OR tbl.user_ids LIKE '%,$userId,%' OR tbl.user_ids LIKE '%,$userId')";
        $where[] = "(tbl.user_ids='$userId' OR tbl.user_ids LIKE '%$userId%')";

        if ($config->activate_multilingual_feature && $app->getLanguageFilter())
        {
            $where[] = 'tbl.language IN (' . $db->Quote(JFactory::getLanguage()->getTag()) . ',' . $db->Quote('*') . ', "")';
        }

        if ($jobsiteID != null)
        {
            $where[] = 'tbl.job_id LIKE ' . $db->quote("%" . $jobsiteID . "%");
        }
        else
        {
            $where[] = 'tbl.job_id != 0';
        }

        $query = 'SELECT tbl.*, b.address '
                . ' FROM #__edocman_categories AS tbl '
                . ' LEFT JOIN #__jobsite_info as b on tbl.job_id = b.jobsite_id '
                . ' WHERE ' . implode(' AND ', $where)
                . ' ORDER BY tbl.job_id ';

        $db->setQuery($query);
        $rows = $db->loadObjectList();

        for ($i = 0, $n = count($rows); $i < $n; $i++)
        {
            $row = $rows[$i];
            $sql = 'SELECT COUNT(*) FROM #__edocman_categories WHERE parent_id = ' . $row->id;
            $this->getDbo()->setQuery($sql);
            $row->total_categories = $this->getDbo()->loadResult();
            if ($config->show_number_documents || !$config->show_empty_cat)
            {
                $row->total_documents = EdocmanHelper::countDocuments($row->id);
            }
        }
        return $rows;
    }

    /**
     * Get total Categories 
     *
     * @return int
     */
    function getTotal()
    {
        // Lets load the content if it doesn't already exist
        if (empty($this->_total))
        {
            $where = $this->_buildContentWhere();
            $sql = 'SELECT COUNT(*) FROM #__edocman_documents AS tbl ' . $where;
            $this->getDbo()->setQuery($sql);
            $this->_total = $this->getDbo()->loadResult();
        }
        return $this->_total;
    }

    /**
     * Method to get a pagination object for the donors
     *
     * @access public
     * @return integer
     */
    function getPagination()
    {
        // Lets load the content if it doesn't already exist
        if (empty($this->_pagination))
        {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'),
                    $this->state->getDefault('limit'));
        }
        return $this->_pagination;
    }

    /**
     * Build the select clause
     *
     * @return string
     */
    function _buildQuery()
    {
        // Get the WHERE and ORDER BY clauses for the query
        $where = $this->_buildContentWhere();
        $orderby = $this->_buildContentOrderBy();
        $query = 'SELECT tbl.* FROM  #__edocman_documents AS tbl '
                . $where
                . $orderby
        ;

        return $query;
    }

    /**
     * Build order by clause for the select command
     *
     * @return string order by clause
     */
    function _buildContentOrderBy()
    {
        $config = EdocmanHelper::getConfig();
        if ($config->show_sort_options)
        {
            $mainframe = JFactory::getApplication('site');
            $ordering = $mainframe->getUserStateFromRequest('com_edocman.category.ordering', 'ordering',
                    $config->default_sort_option, 'cmd');
            if (strlen($ordering) == 0)
                $ordering = $config->default_sort_option;
            $direction = $mainframe->getUserStateFromRequest('com_edocman.category.direction', 'direction',
                    $config->default_sort_direction, 'cmd');
            if (strlen($direction) == 0)
                $direction = $config->default_sort_direction;
            $orderby = ' ORDER BY ' . $ordering . ' ' . strtoupper($direction);
        } else
        {
            $ordering = $config->default_sort_option;
            if (strlen($ordering) == 0)
                $ordering = ' tbl.ordering ';
            $direction = $config->default_sort_direction;
            if (strlen($direction) == 0)
                $direction = ' asc ';
            $orderby = ' ORDER BY ' . $ordering . ' ' . strtoupper($direction);
        }

        return $orderby;
    }

    /**
     * Build the where clause
     *
     * @return string
     */
    function _buildContentWhere()
    {
        static $where = null;
        if ($where == null)
        {
            $app = & JFactory::getApplication();
            $config = EdocmanHelper::getConfig();
            $db = & JFactory::getDbo();
            $user = & JFactory::getUser();
            $userId = $user->id;
            $where = array();
            $categoryId = $this->categoryId;
            if (!$user->authorise('core.edit', 'com_edocman.category.' . $categoryId))
            {
                $where[] = 'tbl.published = 1';
            }
            $where[] = "((tbl.user_ids = '' AND tbl.access IN (" . implode(',', $user->getAuthorisedViewLevels()) . ")) OR tbl.user_ids='$userId' OR tbl.user_ids LIKE '$userId,%' OR tbl.user_ids LIKE '%,$userId,%' OR tbl.user_ids LIKE '%,$userId')";
            if ($categoryId)
            {
                $where[] = ' tbl.id IN (SELECT document_id FROM #__edocman_document_category WHERE category_id = ' . $categoryId . ' ) ';
            }
            if ($config->activate_multilingual_feature && $app->getLanguageFilter())
            {
                $where[] = 'tbl.language IN (' . $db->Quote(JFactory::getLanguage()->getTag()) . ',' . $db->Quote('*') . ', "")';
            }
            $where = ( count($where) ? ' WHERE ' . implode(' AND ', $where) : '' );
        }

        return $where;
    }

    /**
     * 
     * Get current category
     * @return object the current category
     */
    function getCategory()
    {
        if (is_null($this->categoryId))
            return null;
        $sql = 'SELECT * FROM #__edocman_categories WHERE id=' . $this->categoryId;
        $this->getDbo()->setQuery($sql);
        return $this->getDbo()->loadObject();
    }

    function getCategoryID()
    {
        $sql = 'SELECT id FROM #__edocman_categories WHERE title LIKE \'Jobsite Invoices\' ';
        $this->getDbo()->setQuery($sql);
        return $this->getDbo()->loadResult();
    }

}
